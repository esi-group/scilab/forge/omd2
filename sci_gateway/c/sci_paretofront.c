/*
 * PARETOFRONT returns the logical Pareto Front of a set of points.
 *      synopsis:   front = paretofront(M)
 * INPUT ARGUMENT
 *      - M         n x m array, of which (i,j) element is the j-th objective
 *                  value of the i-th point;
 *  OUTPUT ARGUMENT
 *      - front     n x 1 logical vector to indicate if the corresponding
 *                  points are belong to the front (true) or not (false).
 * Matlab version: By Yi Cao at Cranfield University, 31 October 2007
 */

/*
 * Scilab version based on Matlab version and developed in the context of:
 * - OMD2 project (French National Agency for Research)
 * - CSDL project (Systematic competitiveness cluster)
 */

#include "paretofront.h"

#include "api_scilab.h"
#if  API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "sciprint.h"
#include "Scierror.h"
#include "localization.h"

int sci_paretofront(char* fname)
{
    int* piBool = NULL;
    double* pdblReal = NULL;
    int iRows = 0, iCols = 0, i = 0;

    SciErr sciErr;
    int* piAddr = NULL;

    CheckRhs(1, 1);
    CheckLhs(1, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if(isDoubleType(pvApiCtx, piAddr))
    {
        if(!isVarComplex(pvApiCtx, piAddr))
        {
            sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &iRows, &iCols, &pdblReal);
            if(sciErr.iErr)
            {
                printError(&sciErr, 0);
                return 0;
            }

            sciErr = allocMatrixOfBoolean(pvApiCtx, Rhs + 1, iRows, 1, &piBool);
            if(sciErr.iErr)
            {
                printError(&sciErr, 0);
                return 0;
            }

            memset(piBool, 0x00, iRows*sizeof(int));
            paretofront(piBool, pdblReal, iRows, iCols);

            LhsVar(1) = Rhs + 1;
            PutLhsVar();
            return 0;
        }
        else
        {
            Scierror(999, _("%s: Wrong type for input argument #%d: A real matrix expected.\n"), fname, 1);
            return 0;
        }
    }
    else
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A real matrix expected.\n"), fname, 1);
        return 0;
    }

    return 0;
}

