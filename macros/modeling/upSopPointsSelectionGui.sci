// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function upSopPointsSelectionGui()

    // Get data
    sopSelGui = findobj("Tag", "soppointsselectiongui");
    sopproject = sopGetCurrentProject(sopSelGui);

    // Get current response
    responsesMenu = findobj("Tag", "responses_menu");
    responseIndex = 1; // Default
    responsesMenuChildren = responsesMenu.children;
    for kResp=1:size(responsesMenuChildren, "*")
        if responsesMenuChildren(kResp).checked=="on" then
            responseIndex = size(responsesMenuChildren, "*") - kResp + 1;
        end
    end
    
    // Update window title
    projectResponses = sopproject.responses;
    sopSelGui.figure_name = "Points selection GUI - " + projectResponses(responseIndex).name;

    pointsListbox = findobj("Tag", "points_listbox");
    if isempty(sopproject.doenumerotation) then
        strPoints = "P" + string(1:size(projectResponses(responseIndex).values, 1))';
    else
        strPoints = sopproject.doenumerotation.values;
    end
    if or(pointsListbox.string'<>strPoints) then
        pointsListbox.string = strPoints;
        pointsListbox.value = 1;
    end
    
    selectedPoint = pointsListbox.value;
    pointType = projectResponses(responseIndex).valuestype;
    pointType = pointType(selectedPoint);
    
    badpointCheckbox = findobj("Tag", "badpoint_checkbox");
    validationpointCheckbox = findobj("Tag", "validationpoint_checkbox");
    learningpointCheckbox = findobj("Tag", "learningpoint_checkbox");

    doecenterCheckbox = findobj("Tag", "doecenter_checkbox")
    select pointType
    case 0 // Bad point
        badpointCheckbox.value = 1;
        validationpointCheckbox.value = 0;
        learningpointCheckbox.value = 0;
        doecenterCheckbox.enable = "off";
    case 1 // Learning point
        badpointCheckbox.value = 0;
        validationpointCheckbox.value = 0;
        learningpointCheckbox.value = 1;
        doecenterCheckbox.enable = "on";
    case 2 // Validation point
        badpointCheckbox.value = 0;
        validationpointCheckbox.value = 1;
        learningpointCheckbox.value = 0;
        doecenterCheckbox.enable = "off";
    end
    
    if selectedPoint==sopproject.doecenterpointindex then
        doecenterCheckbox.value = 1;
    else
        doecenterCheckbox.value = 0;
    end
    
    // DOE numerotation
    doeStr = _("Point index");
    doeEnable = "off";
    doeValue = 1;
    projectVariables = sopproject.variables;
    if ~isempty(projectVariables) then
        doeEnable = "on";
    end
    for kVar = 1:size(projectVariables)
        doeStr($+1) = projectVariables(kVar).name;
    end
    doeNumVar = sopproject.doenumerotation;
    if ~isempty(doeNumVar) then
        doeStr($+1) = doeNumVar.name;
        doeValue = size(doeStr, "*");
    end
    doenumPopup = findobj("Tag", "doenumerotation_popup");
    doenumPopup.string = doeStr;
    doenumPopup.enable = doeEnable;
    doenumPopup.value = doeValue;

    selPointsAxes = findobj("Userdata", "selpoints_axes");
    axesChildren = selPointsAxes.children;
    if ~isempty(axesChildren) then
        delete(axesChildren)
    end
    
    sca(selPointsAxes)
    drawlater();

    allValues = projectResponses(responseIndex).values;
    
    respValues = projectResponses(responseIndex).values;
    respValuesType = projectResponses(responseIndex).valuestype;
    badPoints = respValues(respValuesType==0);
    badPointsIndices = find(respValuesType==0);
    learningPoints = respValues(respValuesType==1);
    learningPointsIndices = find(respValuesType==1);
    validationPoints = respValues(respValuesType==2);
    validationPointsIndices = find(respValuesType==2);
    
    if ~isempty(badPointsIndices) then
        plot(badPointsIndices, badPoints, ".r");
    end
    if ~isempty(learningPointsIndices) then
        plot(learningPointsIndices, learningPoints, ".g");
    end
    if ~isempty(validationPointsIndices) then
        plot(validationPointsIndices, validationPoints, ".b");
    end
    
    strPoints = pointsListbox.string;
    xstring(selectedPoint*1.01, ..
            respValues(selectedPoint), ..
            strPoints(selectedPoint));
    plot(selectedPoint, respValues(selectedPoint), "*k", "MarkerSize", 7);

    drawnow();
    selPointsAxes.data_bounds = [0 min(allValues);size(allValues, "*") max(allValues)];
    selPointsAxes.tight_limits = "on";
endfunction
