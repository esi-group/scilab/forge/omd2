// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGraphModel2D(responseIndex)

    h = findobj("Tag", "sopmodelinggui");
    sopProj = sopGetCurrentProject(h);

    if size(sopProj.responses(responseIndex).models)==0 then
        messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has no model.", "Error", "error");
        return
    end
    for kModel = 1:size(sopProj.responses(responseIndex).models)
        if isempty(sopProj.responses(responseIndex).models(kModel).coeffs) then
            messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has not been modeled (Model #" + string(kModel) + ").", "Error", "error");
            return
        end
    end

    f = sopFigure(_("2D Model Visualization - ") + sopProj.responses(responseIndex).name, ..
                    [100+20*responseIndex 100+20*responseIndex 700 500]);

    // Reference point (center point if not user defined)
    fullDoe = sopGetDoe(sopProj, responseIndex, "goodpoints");
    refpoint = [];
    for currentFactorIndex=1:size(sopProj.factors)
        mini = min(fullDoe(:, currentFactorIndex), "r");
        maxi = max(fullDoe(:, currentFactorIndex), "r");
        refpoint(1, currentFactorIndex) = mini + (maxi-mini)/2;
    end
    
    minYAxes = %nan;
    maxYAxes = %nan;
    allAxes = [];
    
    drawlater(); // drawnow will be called in sopPageManager
    
    for currentFactorIndex=1:size(sopProj.factors)
        scf(f)
        a = sopSubplot(1, size(sopProj.factors), currentFactorIndex);
        allAxes($+1) = a;

        nbpointspardim = 20;
        plan = repmat(refpoint,nbpointspardim,1);
        minX = min(fullDoe(:, currentFactorIndex), "r");
        maxX = max(fullDoe(:, currentFactorIndex), "r");
        pas = (maxX - minX) / (nbpointspardim - 1);
        if (pas==0) then
            minX = 0.9 * minX;
            maxX = 1.1 * maxX;
            pas = (maxX - minX) / (nbpointspardim - 1);
        end
        plan(:,currentFactorIndex) = (minX:pas:maxX)';

        sca(a);
        a.auto_clear = "off";
        a.grid = [1 1];
        a.box= "on";
        a.tight_limits="on";

        legString = [];
        legHandles = [];
        colors = ["cyan" "green" "blue" "red" "magenta" "yellow"];
        for kModel = 1:size(sopProj.responses(responseIndex).models)
            allResponses = sopProj.responses#;
            allModels = allResponses(responseIndex).models#;
            
            mdl = sopGetValue(allModels(kModel));
            mdl.points = plan;
            allModels(kModel) = mdl;
            
            sopPrediction(allModels(kModel));
            predictedResponse = allModels(kModel).predictions
            [ierr, legString($+1)] = sopCallWrapper(sopProj.responses(responseIndex).models(kModel).funname, "description", sopProj.responses#(responseIndex).models#(kModel))
            plot(plan(:, currentFactorIndex), predictedResponse, "Foreground", colors(modulo(kModel, size(colors, "*"))+1));
            legHandles($+1) = get(gce(), "children");
            
            mdl = sopGetValue(allModels(kModel));
            mdl.points = refpoint;
            allModels(kModel) = mdl;
            
            sopPrediction(allModels(kModel));
            predictedResponse = allModels(kModel).predictions
            plot(refpoint(currentFactorIndex), predictedResponse, "s", "Markersize", 4, "Foreground", colors(modulo(kModel, size(colors, "*"))+1));
        end
        
        factValues = sopProj.factors(currentFactorIndex).values(sopProj.responses(responseIndex).valuestype == 1);
        respValues = sopProj.responses(responseIndex).values(sopProj.responses(responseIndex).valuestype == 1);
        plot(factValues, respValues, "b.", "Markersize", 4);
        legHandles($+1) = get(gce(), "children");
        legString($+1) = _("Learning points")
        
        factValues = sopProj.factors(currentFactorIndex).values(sopProj.responses(responseIndex).valuestype == 2);
        respValues = sopProj.responses(responseIndex).values(sopProj.responses(responseIndex).valuestype == 2);
        plot(factValues, respValues, "r.", "Markersize", 4);
        legHandles($+1) = get(gce(), "children");
        legString($+1) = _("Validation points")

        minYAxes = min([minYAxes, a.data_bounds(1,2)]);
        maxYAxes = max([maxYAxes, a.data_bounds(2,2)]);

        hLeg = legend(legHandles, legString, -5);
        hLeg.background = -2;
        sopSetAxesLabels(a, sopProj.factors(currentFactorIndex), sopProj.responses(responseIndex))

    end
    
    // Align all axes
    for kAxes = 1:size(allAxes, "*")
        allAxes(kAxes).data_bounds(:,2) = [minYAxes; maxYAxes];
    end
        
    sopPageManager(f, allAxes)

endfunction

