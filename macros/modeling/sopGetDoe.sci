// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [doePoints] = sopGetDoe(sopProj, responseIndex, pointsType)

    // TODO: input check

    doePoints = [];
    
    select pointsType
    case "learningpoints"
        for kFactor = 1:size(sopProj.factors)
            values = sopProj.factors(kFactor).values;
            valuesType = sopProj.responses(responseIndex).valuestype;
            doePoints = [doePoints values(valuesType == 1)];
        end
    case "validationpoints"
        for kFactor = 1:size(sopProj.factors)
            values = sopProj.factors(kFactor).values;
            valuesType = sopProj.responses(responseIndex).valuestype;
            doePoints = [doePoints values(valuesType == 2)];
        end
    case "goodpoints"
        for kFactor = 1:size(sopProj.factors)
            values = sopProj.factors(kFactor).values;
            valuesType = sopProj.responses(responseIndex).valuestype;
            factValues = [];
            for kValue = 1:size(values, "*")
                if or(valuesType(kValue) == [1 2]) then
                    factValues($+1) = values(kValue);
                end
            end
            doePoints = [doePoints factValues];
        end
    else
        error("Uknown option:" + pointsType); // TODO better error message
    end
endfunction
