// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function normedDoe = sopDoeNormalization(origDoe, minBound, maxBound)

// TODO test inputs

if argn(2) == 1 then
    minBound = min(origDoe, "r");
    maxBound = max(origDoe, "r");
end

// Manage fixed value factors
fixedParams = find(minBound==maxBound);
minBound(fixedParams) = 0;
maxBound(fixedParams) = 1;
origDoe(:,fixedParams) = 1;

normedDoe = (2*origDoe - ..
            (ones(size(origDoe,1),1) .*. maxBound) - ..
            ones(size(origDoe,1),1) .*. minBound) ./ ..
            (ones(size(origDoe,1),1) .*. maxBound - ones(size(origDoe,1),1) .*. minBound);

endfunction

