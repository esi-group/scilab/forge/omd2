// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function updateSopModelingGui()

    sopModGui = findobj("Tag", "sopmodelinggui");
    
    // When coming from point selection GUI
    tmpObj = findobj("Tag", "waitingforpointsselgui");
    if ~isempty(tmpObj) then
        if ~isempty(tmpObj.userdata) then // The user did not cancel ==> get updated project
            ud = tmpObj.userdata;
            delete(tmpObj);
            sopSetCurrentProject(sopModGui, ud); // Also calls updateSopModelingGui
            return
        else
            delete(tmpObj);
        end
    end
    
    sopproject = sopGetCurrentProject(sopModGui);
    
    if isempty(sopproject) then // At application start
        return
    end

    // Menus
    fileMenu = findobj("Tag", "file_menu");
    fileMenu.enable = "on";
    modelingMenu = findobj("Tag", "modeling_menu");
    if size(sopproject.responses)==0 then
        modelingMenu.enable = "off";
    else
        modelingMenu.enable = "on";
    end

    // Window management buttons
    hdls = sopGuiHandles(sopModGui);
    hdls.reset_btn.enable = "on";
    hdls.save_btn.enable = "on";
    hdls.saveclose_btn.enable = "on";
    hdls.close_btn.enable = "on";

    // Responses frame
    responsesListbox = findobj("Tag", "responses_listbox");
    projectResponses = sopproject.responses#;
    if sopGetNbResponses(sopproject)<>0 then
        respNames = [];
        for kResp = 1:sopGetNbResponses(sopproject)
            respNames($+1) = projectResponses(kResp).name;
        end
        if or(respNames<>responsesListbox.string') then
            responsesListbox.string = respNames;
        end
        responsesListbox.enable = "on";

        if ~isempty(responsesListbox.value) then
            resp2varbtn.enable = "on";
        else
            resp2varbtn.enable = "off";
        end
    else
        responsesListbox.enable = "off";
        responsesListbox.string = "";
        resp2varbtn.enable = "off";
    end

    // Configuration frame
    modelsListbox = findobj("Tag", "models_listbox");
    responsesListbox = findobj("Tag", "responses_listbox");
    selectedResponse = responsesListbox.value;
    addmodelButton = findobj("Tag", "addmodel_btn");
    delmodelButton = findobj("Tag", "delmodel_btn");
    editmodelButton = findobj("Tag", "editmodel_btn");
    selectpointsButton = findobj("Tag", "selectpoints_btn");
    startModelingMenu = findobj("Tag", "startmodeling_menu");
    viewMenu = findobj("Tag", "view_menu");
    
    // TODO Better management of menus enable status
    
    if ~isempty(selectedResponse) & selectedResponse<>0 then // Test on 0 added for Scilab 5.4.0 (bug?)
        viewMenu.enable = "on";
        for kChild = 1:size(viewMenu.children, "*")
            viewMenu.children(kChild).enable = "on";
        end
        startModelingMenu.enable = "on"
        addmodelButton.enable = "on";
        if length(selectedResponse)==1 then
            selectpointsButton.enable = "on";
        else
            selectpointsButton.enable = "off";
        end

        models = projectResponses(selectedResponse).models;
        if size(models)<>0 then
            modelsName = [];
            tic()
            for kModel = 1:size(models)
                modelsPtr = sopproject.responses#(selectedResponse).models#;
                [ierr, modelsName($+1)] = sopCallWrapper(models(kModel).funname, "description", modelsPtr(kModel));
            end
            if or(modelsListbox.string<>modelsName) then
                modelsListbox.callback_type = -1;
                modelsListbox.string = modelsName;
                modelsListbox.callback_type = 0;
            end
            modelsListbox.enable = "on";
        else
            modelsListbox.enable = "off";
            modelsListbox.string = _("No model.");
        end

        selectedModel = modelsListbox.value;
        // Can edit model only if one response and one model selected
        if size(selectedModel, "*")==1 & size(selectedResponse, "*")==1 then
            delmodelButton.enable = "on";
            editmodelButton.enable = "on";
        else
            delmodelButton.enable = "off";
            editmodelButton.enable = "off";
        end
    else
        viewMenu.enable = "off";
        modelsListbox.enable = "off";
        modelsListbox.string = _("No response selected.");
        addmodelButton.enable = "off";
        delmodelButton.enable = "off";
        editmodelButton.enable = "off";
        selectpointsButton.enable = "off";
    end
endfunction

