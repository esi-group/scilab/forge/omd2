// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopDoeGeneration(sopProj)

    rhs = argn(2);
    if rhs<>1 then
        error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"), "sopDoeGeneration", 1))
    end
    if sopObjectType(sopProj)<>"Project" then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A SOP object of type ''%s'' expected.\n"), "sopDoeGeneration", 1, "Project"));
    end

    sopLog("DOE generation...")

    sopProj.dataexecstatus = sopStatus("Running");

    sopLog(_("Reset all factors values."));
    allFactors = sopProj.factors#;
    for kFact = 1:size(allFactors)
        allFactors(kFact).values = [];
        allFactors(kFact).valuestype = [];
    end
    sopLog(_("Reset all responses values."));
    allResponses = sopProj.responses#;
    for kResp = 1:size(allResponses)
        allResponses(kResp).values = [];
        allResponses(kResp).valuestype = [];
    end

    // Call all generators
    allGenerators = sopProj.doegenerators#;
    for kGenerator = 1:size(allGenerators)

        // Call generator
        generatorFunction = allGenerators(kGenerator).funname;
        ierr = sopCallWrapper(generatorFunction, "generate", allGenerators(kGenerator));
        if ierr<>0 then
            sopProj.dataexecstatus = sopStatus("Failed");
            sopError("DOE generation: FAILED")
            return
        end

    end

    // Copy generated values into factors
    allFactors = sopProj.factors#;
    currentDoeColumn = 1;
    for kFact = 1:size(allFactors)
        if allFactors(kFact).isfixed then
            nominalValue = allFactors(kFact).nominalvalue;
            for kGenerator = 1:size(allGenerators)
                allFactors(kFact).values = [allFactors(kFact).values; ones(size(allGenerators(kGenerator).values,1),1) * nominalValue];
            end
        else
            ub = allFactors(kFact).maxbound;
            lb = allFactors(kFact).minbound;

            for kGenerator = 1:size(allGenerators)
                values = allGenerators(kGenerator).values;
                factValues = lb + (ub-lb) * values(:, currentDoeColumn);
                currentDoeColumn = currentDoeColumn + 1;
                allFactors(kFact).values = [allFactors(kFact).values; factValues];
            end
        end
        allFactors(kFact).valuestype = ones(size(allFactors(kFact).values, 1), 1);
    end

    sopProj.dataexecstatus = sopStatus("Done");

    sopLog("DOE generation: DONE")


endfunction
