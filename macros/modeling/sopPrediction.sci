// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopPrediction(modelObj)

    // Call the predictor
    wrapperFunction = modelObj.funname;
    ierr = sopCallWrapper(wrapperFunction, "estimate", modelObj);
    if ierr<>0 then
        sopError("Prediction failed."); // TODO
        return
    end

    // TODO manage transfo on error and prediction

endfunction
