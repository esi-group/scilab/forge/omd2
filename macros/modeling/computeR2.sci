// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [R2] = computeR2(observations,prediction)

Y_mean = mean(observations);

if sum((observations - Y_mean).^2) == 0
    R2 = 1;
    return
end
R2 = 1 - sum((observations - prediction).^2)/sum((observations - Y_mean).^2);
endfunction
