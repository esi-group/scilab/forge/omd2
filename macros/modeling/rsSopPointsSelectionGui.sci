// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function rsSopPointsSelectionGui()

    fig = findobj("Tag", "soppointsselectiongui");
    hdls = sopGuiHandles(fig);

    [margin, btnh] = sopGuiParameters("Margin", ..
                                      "ButtonHeight");

    figw = fig.position(3);
    figh = fig.position(4);

    // Add validation button
    btnw = 60;
    hdls.ok_btn.position = [figw-margin-btnw margin btnw btnh];
    hdls.cancel_btn.position = [figw-2*margin-2*btnw margin btnw btnh];

    // Points list frame
    framex = margin;
    framey = 2*margin + btnh;
    framew = (figw-5*margin-40)/4;
    frameh = figh-3*margin-btnh;
    hdls.points_frame.position = [framex, framey, framew, frameh];
    hdls.points_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.points_listbox.position = [framex+margin framey+margin framew-2*margin frameh-2*margin];

    // Points properties frame
    framex = 2*margin + framew;
    framey = 2*margin + btnh;
    framew = (figw-5*margin-40)/4;
    frameh = 4*btnh + 5*margin;
    hdls.properties_frame.position = [framex, framey, framew, frameh];
    hdls.properties_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.doecenter_checkbox.position = [framex+margin framey+margin framew-2*margin btnh];
    hdls.badpoint_checkbox.position = [framex+margin framey+2*margin+btnh framew-2*margin btnh];
    hdls.validationpoint_checkbox.position = [framex+margin framey+3*margin+2*btnh framew-2*margin btnh];
    hdls.learningpoint_checkbox.position = [framex+margin framey+4*margin+3*btnh framew-2*margin btnh];

    // Plex numerotation frame
    framex = 2*margin + framew;
    framey = 3*margin + btnh + frameh;
    framew = (figw-5*margin-40)/4;
    frameh = 2*margin + btnh;
    hdls.plexnumerotation_frame.position = [framex, framey, framew, frameh];
    hdls.plexnumerotation_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.doenumerotation_popup.position = [framex+margin framey+margin framew-2*margin btnh];

    // Graph
    selPointsAxes = gca();
    selPointsAxes.axes_bounds = [0.5 0 0.5 0.85];

endfunction
