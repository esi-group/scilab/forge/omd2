// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopModelingGui(gcboTag)

    if argn(2)<1 then
        gcboTag = get(gcbo, "Tag");
    end

    h = findobj("Tag", "sopmodelinggui");

    select gcboTag
    case "reset_btn"
        // Reload data from original file
        sopProj = sopGetCurrentProject(h);
        mdelete(sopProj.workfile)
        sopProj = sopLoadDataFile(h, sopProj.pointedfile);
        sopSetCurrentProject(h, sopProj);
    case "saveclose_btn"
        cbSopModelingGui("save_btn");
        cbSopModelingGui("close_btn");
        return
    case "close_btn"
        btn = messagebox(_("Do you want to quit Modeling module?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            sopProj = sopGetCurrentProject(h);
            mdelete(sopProj.workfile);
            delete(h);
            sopSetGuiEnable(findobj("tag", "sopgui"), "on");
        end
        return // Avoid the execution of updateSopModelingGui();
    case "save_btn"
        // Erase original file by modified one (workfile)
        sopProj = sopGetCurrentProject(h);
        sopSaveProject(sopProj);
    case "startmodeling_menu"
        // Modeling
        sopProj = sopGetCurrentProject(h);
        sopModeling(sopProj);
    case "graphreponse_menu"
        selectedResponse = get(findobj("Tag", "responses_listbox"), "Value");
        sopGraphModel2D(selectedResponse);
    case "graphfactor_menu"
        selectedResponse = get(findobj("Tag", "responses_listbox"), "Value");
        // TODO
    case "graphcontour_menu"
        selectedResponse = get(findobj("Tag", "responses_listbox"), "Value");
        sopGraphReponse2D(selectedResponse);
    case "graphsensibility_menu"
        selectedResponse = get(findobj("Tag", "responses_listbox"), "Value");
        sopGraphSensibility(selectedResponse);
    case "graphcorrelation_menu"
        selectedResponse = get(findobj("Tag", "responses_listbox"), "Value");
        sopGraphCorrelation(selectedResponse);
    case "responses_listbox"
        // Nothing to do
    case "models_listbox"
        // Nothing to do
    case "selectpoints_btn"
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponses = responsesListbox.value;
        sopPointsSelectionGui(selectedResponses);
        return; // Avoid the execution of updateSopModelingGui();
    case "addmodel_btn"
        allModelers = sopLoadWrappers("Modeler");
        allModelersDesc = [];
        for kModeler = 1:size(allModelers, "*")
            [ierr, allModelersDesc($+1)] = sopCallWrapper(allModelers(kModeler), "description");
        end
        selectedItem = x_choose(allModelersDesc, _("Select a modeler"), _("Cancel"));
        if selectedItem==0 then // The user cancels
            return
        end
       
        sopProj = sopGetCurrentProject(h);
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponses = responsesListbox.value;
        
        addedModel = sopInitWrapper(allModelers(selectedItem));
        
        sopProj.responses(selectedResponses).models($+1) = addedModel;

        sopSetCurrentProject(h, sopProj);
        
        // Launch configuration window directly
        modelsListbox = findobj("Tag", "models_listbox");
        modelsListbox.value = size(sopProj.responses(selectedResponses).models)
        cbSopModelingGui("editmodel_btn")
        
    case "delmodel_btn"
        sopProj = sopGetCurrentProject(h);
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponses = responsesListbox.value;
        modelsListbox = findobj("Tag", "models_listbox");
        selectedModel = modelsListbox.value;
        sopProj.responses(selectedResponses).models(selectedModel) = null();
        sopSetCurrentProject(h, sopProj);
    case "editmodel_btn"
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponse = responsesListbox.value;
        modelsListbox = findobj("Tag", "models_listbox");
        selectedModel = modelsListbox.value;
        sopProj = sopGetCurrentProject(h);
        editedModel# = sopProj.responses#(selectedResponse).models#(selectedModel);
        modelName = sopProj.responses(selectedResponse).models(selectedModel).funname;

        [ierr, inparams] = sopCallWrapper(modelName, "configure");
        if ierr<>0 then
            error(msprintf(_("%s: Could not get configuration for ''%s'' wrapper.\n"), "cbSopModelingGui", modelName));
        end
        sopParametersConfigGui(h, "editmodel_btn", inparams, editedModel#);
        return // Avoid the execution of updateSopModelingGui();
    case "quit_menu"
        btn = messagebox(_("Do you want to quit Modeling module?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            sopProj = sopGetCurrentProject(h);
            mdelete(sopProj.workfile);
            delete(h);
            sopSetGuiEnable(findobj("tag", "sopgui"), "on");
        end
        return // Avoid the execution of updateSopModelingGui();
    else
        error("Unknown object with tag: " + gcboTag);
    end

    updateSopModelingGui();

endfunction
