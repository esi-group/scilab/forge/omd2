// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopModelingGui()
    
    sopSetGuiEnable(findobj("tag", "sopgui"), "off");

    h = findobj("Tag", "sopmodelinggui");
    if ~isempty(h) then
        delete(h)
    end

    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");

    figh = 400 + 2*margin + btnh;
    framemaxh = figh - 3*margin - btnh;
    figw = 400 + 3*margin;
    
    sopMainGui = findobj("tag", "sopgui");
    callerProject = sopGetCurrentProject(sopMainGui);
    h = sopFigure(_("Modeling GUI"), [100 100 figw figh], "sopmodelinggui", callerProject, %T, %T, "updateSopModelingGui", "cbSopModelingGui");
    h.closerequestfcn = "cbSopModelingGui(""quit_menu"");";
    h.resizefcn = "resizeSopModelingGui();";

    fileMenu = uimenu("Parent", h, ..
                      "Label","&File", ..
                      "Tag", "file_menu");
           
    uimenu("Parent", fileMenu, ..
           "Label","&Quit", ..
           "Tag", "quit_menu", ..
           "Callback", "cbSopModelingGui");

    modelingMenu = uimenu("Parent", h, ..
                              "Label","&Modeling", ..
                              "Enable", "off", ..
                              "Tag", "modeling_menu");

    uimenu("Parent", modelingMenu, ..
           "Label","&Start", ..
           "Tag", "startmodeling_menu", ..
           "Callback", "cbSopModelingGui");
           
    viewMenu = uimenu("Parent", modelingMenu, ..
                      "Label",_("&View"), ..
                      "Tag", "view_menu");

    uimenu("Parent", viewMenu, ..
           "Label",_("Response / All Factors"), ..
           "Tag", "graphreponse_menu", ..
           "Callback", "cbSopModelingGui");
    //uimenu("Parent", viewMenu, ..
    //       "Label",_("Factor / All Responses"), ..
    //       "Tag", "graphfactor_menu", ..
    //       "Callback", "cbSopModelingGui");
    uimenu("Parent", viewMenu, ..
           "Label",_("&Correlation"), ..
           "Tag", "graphcorrelation_menu", ..
           "Callback", "cbSopModelingGui");
    uimenu("Parent", viewMenu, ..
           "Label",_("Response / Two-factors (Contour)"), ..
           "Tag", "graphcontour_menu", ..
           "Callback", "cbSopModelingGui");
    //uimenu("Parent", viewMenu, ..
    //       "Label",_("&Sensibility Analysis"), ..
    //       "Tag", "graphsensibility_menu", ..
    //       "Callback", "cbSopModelingGui");

    // Window management buttons
    btnw = 90;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Close", ..
              "Position", [figw-margin-btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "close_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Save & Close", ..
              "Position", [figw-2*margin-2*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "saveclose_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Save", ..
              "Position", [figw-3*margin-3*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "save_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Reset", ..
              "Position", [figw-4*margin-4*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "reset_btn");

    frameybasis = 2*margin+btnh;
    // Responses frame
    framex = margin;
    framey = frameybasis;
    framew = 200;
    frameh = framemaxh;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Responses", 60, "responses");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+margin framew-2*margin frameh-2*margin], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Min", 0, ..
              "Max", 0, ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "responses_listbox");

    // Configuration frame
    framex = 2*margin + framew;
    framey = frameybasis;
    framew = 200;
    frameh = framemaxh;
    tmpbtnw = (framew-4*margin)/3
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Configuration", 70, "configuration");
    // Add "Select points" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", _("Select points..."), ..
              "Position", [framex+margin framey+margin framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "selectpoints_btn");
    // Add "Add model" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\plus$", ..
              "Position", [framex+margin framey+2*margin+btnh tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "addmodel_btn");
    // Add "Remove model" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\minus$", ..
              "Position", [framex+2*margin+tmpbtnw framey+2*margin+btnh tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "delmodel_btn");
    // Add "Edit model" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Edit", ..
              "Position", [framex+3*margin+2*tmpbtnw framey+2*margin+btnh tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "editmodel_btn");
    // Add models listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+3*margin+2*btnh framew-2*margin frameh-4*margin-2*btnh], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "String", _("No response selected."), ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopModelingGui();", ..
              "Tag", "models_listbox");

    resizeSopModelingGui();
    updateSopModelingGui();

endfunction
