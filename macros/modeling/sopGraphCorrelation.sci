// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGraphCorrelation(responseIndex)

    h = findobj("Tag", "sopmodelinggui");
    sopProj = sopGetCurrentProject(h);

    if isempty(responseIndex) then
        messagebox("No response selected.", "Error", "error");
        return
    end
    if size(sopProj.responses(responseIndex).models)==0 then
        messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has no model.", "Error", "error");
        return
    end
    for kModel = 1:size(sopProj.responses(responseIndex).models)
        if isempty(sopProj.responses(responseIndex).models(kModel).coeffs) then
            messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has not been modeled (Model #" + string(kModel) + ").", "Error", "error");
            return
        end
    end

    f = sopFigure(_("Correlation - ") + sopProj.responses(responseIndex).name, [100+20*responseIndex 100+20*responseIndex 700 500]);
    scf(f)
    learningPoints = sopGetDoe(sopProj, responseIndex, "learningpoints");
    fullPlex = sopGetDoe(sopProj, responseIndex, "goodpoints");
    maxi = max(fullPlex, "r")
    mini = min(fullPlex, "r");
    learningPoints    = sopDoeNormalization(learningPoints, mini, maxi);

    // Resured response
    // TODO manage transfo
    mesuredResponse = sopProj.responses(responseIndex).values(sopProj.responses(responseIndex).valuestype == 1);

    allAxes = [];
    for kModel = 1:size(sopProj.responses(responseIndex).models) 
        scf(f)
        drawlater();

        a = sopSubplot(1, 1, kModel);
        allAxes($+1) = a;

        [ierr, mname] = sopCallWrapper(sopProj.responses(responseIndex).models(kModel).funname,"description",sopProj.responses#(responseIndex).models#(kModel));

        // RECUPERATION DES OBSERVES
        Ymesures = sopProj.responses(responseIndex).values;

        // RECUPERATION DU VECTEUR DE VAILABILITY
        vailmesures=sopProj.responses(responseIndex).valuestype;

        // VECTEUR DE RUN-ORDER
        runorder=find(vailmesures==1 | vailmesures==3);

        // RECUPERATION DU PLEX
        plexLearn=sopGetDoe(sopProj, responseIndex, "learningpoints");
        plexValid=sopGetDoe(sopProj, responseIndex, "validationpoints");

        // CALCUL DES PREDITS
        allResponses = sopProj.responses#;
        allModels = allResponses(responseIndex).models#;
        
        mdl = sopGetValue(allModels(kModel));
        mdl.points = plexLearn;
        allModels(kModel) = mdl;
        
        sopPrediction(allModels(kModel));
        YpreditsLearn=allModels(kModel).predictions;
        
        mdl = sopGetValue(allModels(kModel));
        mdl.points = plexValid;
        allModels(kModel) = mdl;

        sopPrediction(allModels(kModel));
        YpreditsValid=allModels(kModel).predictions;
        
        plot(Ymesures(vailmesures==1), YpreditsLearn, ".b", "MarkSize", 4)
        
        hLearn = gce();
        
        plot(Ymesures(vailmesures==2), YpreditsValid, ".r", "MarkSize", 4)
        hValid = gce();

        // TRACé DE LA BISSECTRICE
        a = gca();
        a.grid = [1 1];
        xlim=[f.children(1).x_ticks.locations(1);f.children(1).x_ticks.locations($)];
        ylim=[f.children(1).y_ticks.locations(1);f.children(1).y_ticks.locations($)];
        borneinf=min([xlim(1) ylim(1)]);
        bornesup=max([xlim(2) ylim(2)]);

        plot([borneinf bornesup],[borneinf bornesup], "r");
        sopSetAxesLabels(a, _("Observed"), mname + _(" Model (Predicted)"))

        legend([hLearn,hValid], [_("Learning points"),_("Validation points")], 2);
    end
    
    sopPageManager(f, allAxes)

endfunction
