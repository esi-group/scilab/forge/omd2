// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function propertyValue = generic_e_sopmdl(propertyName, modelObj)

    if typeof(modelObj)<>"sopmdl" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_e_sopmdl", 2, "sopmdl"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_e_sopmdl", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_e_sopmdl", 1));
    end

    propertyName = "<" + convstr(propertyName, "l") + ">"

    select propertyName
    case "<coeffs>"
        propertyValue = modelObj(propertyName);
        if typeof(propertyValue) == "dace_model" then
            propertyValue.regr = evstr(get_param(modelObj.params, "regr"))
            propertyValue.corr = evstr(get_param(modelObj.params, "_corr"))
        end
    case "<funname>"
        propertyValue = modelObj(propertyName);
    case "<params>"
        propertyValue = modelObj(propertyName);
    case "<points>"
        propertyValue = modelObj(propertyName);
    case "<predictions>"
        propertyValue = modelObj(propertyName);
    case "<version>"
        propertyValue = modelObj(propertyName);
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end
endfunction

