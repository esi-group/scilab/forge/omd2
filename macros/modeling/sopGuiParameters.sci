// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = sopGuiParameters(varargin)

    varargout = list();
      
    for kParam = 1:size(varargin)
        select convstr(varargin(kParam), "l")
        case "figurebackground"
            varargout($+1) = [229 229 229]/255;
        case "margin"
            varargout($+1) = 10;
        case "listboxbackground"
            varargout($+1) = [1 1 0.9]
        case "buttonbackground"
            varargout($+1) = [200 200 200]/255;
        case "buttonheight"
            varargout($+1) = 25
        else 
            error(msprintf("%s: Unknown Scilab Optimisation GUI parameter name: %s\n", "sopGuiParameters", varargin(kParam)));
        end
    end

endfunction
