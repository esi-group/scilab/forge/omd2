// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function optimizerObj = generic_i_sopoptim(propertyName, propertyValue, optimizerObj)

    if typeof(optimizerObj)<>"sopoptim" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopoptim", 3, "sopoptim"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopoptim", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopoptim", 1));
    end

    propertyName = "<" + convstr(propertyName, "l") + ">";

    select propertyName
    case "<funname>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "FunName"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "FunName"));
        end
        optimizerObj(propertyName) = propertyValue;
    case "<objfunctionoptimalvalue>"
        // TODO type/size check
        optimizerObj(propertyName) = propertyValue;
    case "<optimalpoint>"
        // TODO type/size check
        optimizerObj(propertyName) = propertyValue;
    case "<params>"
        if typeof(propertyValue)<>"plist" then
            error(msprintf("Wrong type for ''%s'' property: A ''plist'' tlist expected.\n", "Params"));
        end
        optimizerObj(propertyName) = propertyValue;
    case "<version>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 3, 1));
        end
        if or(size(propertyValue)<>[1 3]) then
            error(msprintf("Wrong size for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 3, 1));
        end
        optimizerObj(propertyName) = propertyValue;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end

endfunction

