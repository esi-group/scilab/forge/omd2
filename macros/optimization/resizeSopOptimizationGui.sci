// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function resizeSopOptimizationGui()

    fig = findobj("Tag", "sopoptimizationgui");
    hdls = sopGuiHandles(fig);

    [margin, btnh] = sopGuiParameters("Margin", ..
                                      "ButtonHeight");

    figh = fig.position(4);400 + 2*margin + btnh;
    framemaxh = figh - 3*margin - btnh;
    figw = fig.position(3);

    btnw = 90;
    hdls.close_btn.position = [figw-margin-btnw margin btnw btnh];
    hdls.saveclose_btn.position = [figw-2*margin-2*btnw margin btnw btnh];
    hdls.save_btn.position = [figw-3*margin-3*btnw margin btnw btnh];
    hdls.reset_btn.position = [figw-4*margin-4*btnw margin btnw btnh];

    frameybasis = 2*margin+btnh;
    // Responses frame
    framex = margin;
    framey = frameybasis;
    framew = (figw-3*margin)/2;;
    frameh = framemaxh;
    hdls.responses_frame.position = [framex, framey, framew, frameh];
    hdls.responses_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.responses_listbox.position = [framex+margin framey+margin framew-2*margin frameh-2*margin];

    // Optimizer #2 frame
    framex = 2*margin + framew;
    framey = frameybasis;
    framew = (figw-3*margin)/2;
    frameh = (framemaxh-2*margin)/3;
    hdls.optimizer2_frame.position = [framex, framey, framew, frameh];
    hdls.optimizer2_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.configureoptimizer2_btn.position = [framex+margin framey+margin framew-2*margin btnh];
    hdls.selectoptimizer2_btn.position = [framex+margin framey+2*margin+btnh framew-2*margin btnh];
    hdls.optimizername2_txt.position = [framex+margin framey+3*margin+2*btnh framew-2*margin btnh];

    // Optimizer #1 frame
    framex = 2*margin + framew;
    framey = 3*margin + btnh + frameh;
    framew = (figw-3*margin)/2;
    frameh = (framemaxh-2*margin)/3;
    hdls.optimizer1_frame.position = [framex, framey, framew, frameh];
    hdls.optimizer1_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.configureoptimizer1_btn.position = [framex+margin framey+margin framew-2*margin btnh];
    hdls.selectoptimizer1_btn.position = [framex+margin framey+2*margin+btnh framew-2*margin btnh];
    hdls.optimizername1_txt.position = [framex+margin framey+3*margin+2*btnh framew-2*margin btnh];

    // Response coeff
    framex = 2*margin + framew;
    framey = 4*margin + btnh + 2*frameh;
    framew = (figw-3*margin)/2;;
    frameh = (framemaxh-2*margin)/3;
    tmpbtnw = (framew - 3*margin) / 2;
    hdls.responsecoeff_frame.position = [framex, framey, framew, frameh];
    hdls.responsecoeff_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.responsecoeff_edit.position = [framex+margin framey+margin tmpbtnw btnh];
    hdls.responsecoeff_btn.position = [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh];

endfunction
