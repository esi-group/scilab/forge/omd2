// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopOptimizationGui(gcboTag)

    if argn(2) < 1 then
        gcboTag = get(gcbo, "Tag");
    end

    fig = findobj("Tag", "sopoptimizationgui");

    select gcboTag
    case "reset_btn"
        // Reload data from original file
        sopProj = sopGetCurrentProject(fig);
        mdelete(sopProj.workfile)
        sopProj = sopLoadDataFile(fig, sopProj.pointedfile);
        sopSetCurrentProject(fig, sopProj);
    case "saveclose_btn"
        cbSopOptimizationGui("save_btn");
        cbSopOptimizationGui("close_btn");
        return
    case "close_btn"
        btn = messagebox(_("Do you want to quit Optimization module?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            sopProj = sopGetCurrentProject(fig);
            mdelete(sopProj.workfile);
            delete(fig);
            sopSetGuiEnable(findobj("tag", "sopgui"), "on");
        end
        return // Avoid the execution of updateSopModelingGui();
    case "save_btn"
        // Erase original file by modified one (workfile)
        sopProj = sopGetCurrentProject(fig);
        sopSaveProject(sopProj);
    case "configureoptimizer1_btn"
        sopProj = sopGetCurrentProject(fig);
        wrapperFunction = sopProj.optimizers(1).funname;
        [ierr, inparams] = sopCallWrapper(wrapperFunction, "configure");
        if ierr<>0 then
            error(msprintf(_("%s: Could not get configuration for ''%s'' wrapper.\n"), "cbSopOptimizationGui", wrapperFunction));
        end
        sopParametersConfigGui(fig, gcboTag, inparams, sopProj.optimizers#(1));
        return // Avoid the execution of updateSopOptimizationGui();
    case "configureoptimizer2_btn"
        sopProj = sopGetCurrentProject(fig);
        wrapperFunction = sopProj.optimizers(2).funname;
        [ierr, inparams] = sopCallWrapper(wrapperFunction, "configure");
        if ierr<>0 then
            error(msprintf(_("%s: Could not get configuration for ''%s'' wrapper.\n"), "cbSopOptimizationGui", wrapperFunction));
        end
        sopParametersConfigGui(fig, gcboTag, inparams, sopProj.optimizers#(2).params);
        return // Avoid the execution of updateSopOptimizationGui();
    case "quit_menu"
        btn = messagebox(_("Do you want to quit Optimization module?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            sopProj = sopGetCurrentProject(fig);
            mdelete(sopProj.workfile);
            delete(fig);
            sopSetGuiEnable(findobj("tag", "sopgui"), "on");
        end
        return // Avoid the execution of updateSopOptimizationGui();
    case "responsecoeff_btn"
        sopProj = sopGetCurrentProject(fig);
        [newCoeff, ierr] = evstr(get(findobj("Tag", "responsecoeff_edit"), "String"));
        if ierr<>0 then
            set(findobj("Tag", "responsecoeff_edit"), "Background", [1 0 0]);
            set(gcbo, "Enable", "off");
        else
            set(findobj("Tag", "responsecoeff_edit"), "Background", [1 1 1]);
            set(gcbo, "Enable", "on");
            responsesListbox = findobj("Tag", "responses_listbox");
            selectedResponse = responsesListbox.value;
            if ~isempty(selectedResponse) then
                allCoeffs = sopProj.responsescoeffs;
                allCoeffs(selectedResponse) = newCoeff;
                sopProj.responsescoeffs = allCoeffs;
            end
        end
    case "responses_listbox"
        // Nothing to do
    case "startoptimization_menu"
        // Optimization
        sopProj = sopGetCurrentProject(fig);
        sopOptimization(sopProj);
    case "selectoptimizer1_btn"
        optimizerWrappers = sopLoadWrappers("optimizer");
        allOptimizersDesc = [];
        for kOptimizer = 1:size(optimizerWrappers, "*")
            [ierr, allOptimizersDesc($+1)] = sopCallWrapper(allOptimizersDesc($+1), "description");
            if ierr<>0 then
                error(msprintf(_("%s: Could not get description for ''%s'' wrapper.\n"), "cbSopOptimizationGui", allOptimizersDesc($+1)));
            end
        end
        selectedItem = x_choose(allOptimizersDesc, _("Select an optimizer"), _("Cancel"));
        if selectedItem==0 then // The user cancels
            return
        end
       
        sopProj = sopGetCurrentProject(fig);
        sopProj.optimizers(1) = sopInitWrapper(optimizerWrappers);

    case "selectoptimizer2_btn"
        optimizerWrappers = sopLoadWrappers("optimizer");
        allOptimizersDesc = [];
        for kOptimizer = 1:size(optimizerWrappers, "*")
            [ierr, allOptimizersDesc($+1)] = sopCallWrapper(allOptimizersDesc($+1), "description");
            if ierr<>0 then
                error(msprintf(_("%s: Could not get description for ''%s'' wrapper.\n"), "cbSopOptimizationGui", allOptimizersDesc($+1)));
            end
        end
        selectedItem = x_choose(allOptimizersDesc, _("Select an optimizer"), _("Cancel"));
        if selectedItem==0 then // The user cancels
            return
        end
       
        sopProj = sopGetCurrentProject(fig);
        sopProj.optimizers(2) = sopInitWrapper(optimizerWrappers);
    else
        error("Unknown object with tag: " + gcboTag);
    end

    updateSopOptimizationGui();

endfunction
