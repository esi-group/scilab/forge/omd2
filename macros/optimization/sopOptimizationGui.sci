// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopOptimizationGui()
    
    sopSetGuiEnable(findobj("tag", "sopgui"), "off");

    h = findobj("Tag", "sopoptimizationgui");
    if ~isempty(h) then
        delete(h)
    end

    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");

    figh = 400 + 2*margin + btnh;
    framemaxh = figh - 3*margin - btnh;
    figw = 400 + 3*margin;
    
    sopMainGui = findobj("tag", "sopgui");
    callerProject = sopGetCurrentProject(sopMainGui);
    h = sopFigure(_("Optimization GUI"), [100 100 figw figh], "sopoptimizationgui", callerProject, %T, %T, "updateSopOptimizationGui", "cbSopOptimizationGui");
    h.closerequestfcn = "cbSopOptimizationGui(""quit_menu"");";
    h.resizefcn = "resizeSopOptimizationGui();";

    fileMenu = uimenu("Parent", h, ..
                      "Label","&File", ..
                      "Tag", "file_menu");
           
    uimenu("Parent", fileMenu, ..
           "Label","&Quit", ..
           "Tag", "quit_menu", ..
           "Callback", "cbSopOptimizationGui");

    optimizationMenu = uimenu("Parent", h, ..
                              "Label","&Optimization", ..
                              "Enable", "off", ..
                              "Tag", "optimization_menu");

    uimenu("Parent", optimizationMenu, ..
           "Label","&Start", ..
           "Tag", "startoptimization_menu", ..
           "Callback", "cbSopOptimizationGui");

    // Window management buttons
    btnw = 90;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Close", ..
              "Position", [figw-margin-btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "close_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Save & Close", ..
              "Position", [figw-2*margin-2*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "saveclose_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Save", ..
              "Position", [figw-3*margin-3*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "save_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Reset", ..
              "Position", [figw-4*margin-4*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "reset_btn");

    frameybasis = 2*margin+btnh;
    // Responses frame
    framex = margin;
    framey = frameybasis;
    framew = 200;
    frameh = framemaxh;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Responses", 60, "responses");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+margin framew-2*margin frameh-2*margin], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Min", 0, ..
              "Max", 0, ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "responses_listbox");

    // Optimizer #2 frame
    framex = 2*margin + framew;
    framey = frameybasis;
    framew = 200;
    frameh = (framemaxh-2*margin)/3;
    createOptimizerFrame(h, framex, framey, framew, frameh, 2)

    // Optimizer #1 frame
    framex = 2*margin + framew;
    framey = 3*margin + btnh + frameh;
    framew = 200;
    frameh = (framemaxh-2*margin)/3;
    createOptimizerFrame(h, framex, framey, framew, frameh, 1)
    
    // Response coeff
    framex = 2*margin + framew;
    framey = 4*margin + btnh + 2*frameh;
    framew = 200;
    frameh = (framemaxh-2*margin)/3;
    tmpbtnw = (framew - 3*margin) / 2;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], _("Response coefficient"), 120, "responsecoeff");
    // Add "Coeff" edit
    uicontrol("Parent", h, ..
              "Style", "edit", ..
              "String", "", ..
              "Position", [framex+margin framey+margin tmpbtnw btnh], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Tag", "responsecoeff_edit");
    // Add "Coeff" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", _("Ok"), ..
              "Position", [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "responsecoeff_btn");

    resizeSopOptimizationGui();
    updateSopOptimizationGui();

endfunction

function createOptimizerFrame(h, framex, framey, framew, frameh, optimizerNumber)

    tmpbtnw = (framew-4*margin)/3

    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Optimizer #" + string(optimizerNumber), 70, "optimizer" + string(optimizerNumber));

    // Add "Configure optimizer" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", _("Configure"), ..
              "Position", [framex+margin framey+margin framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "configureoptimizer" + string(optimizerNumber) + "_btn");
    // Add "Select simulator" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", _("Select"), ..
              "Position", [framex+margin framey+2*margin+btnh framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopOptimizationGui();", ..
              "Tag", "selectoptimizer" + string(optimizerNumber) + "_btn");
    // Add "Simulator Name" label
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [framex+margin framey+3*margin+2*btnh framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Tag", "optimizername" + string(optimizerNumber) + "_txt");

endfunction

