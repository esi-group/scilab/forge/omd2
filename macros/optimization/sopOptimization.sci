// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopProj = sopOptimization(sopProj)

    rhs = argn(2);
    if rhs<>1 then
        error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"), "sopOptimization", 1))
    end
    if sopObjectType(sopProj)<>"Project" then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A SOP object of type ''%s'' expected.\n"), "sopOptimization", 1, "Project"));
    end

    sopLog("Optimization...");
    sopProj.optimexecstatus = sopStatus("Running");

    sopLog(_("Reset previous optimal point values."));
    sopProj.optimalpoint = [];
    sopProj.objfunctionoptimalvalue = [];

    for kOpt = 1:size(sopProj.optimizers)
        // Call optimizer
        optimFunction = sopProj.optimizers(kOpt).funname;
        sopproj = sopProj; // TODO remove this hack added to make objective function work
        ierr = sopCallWrapper(optimFunction, "optimize", sopProj.optimizers#(kOpt));
        if ierr<>0 then
            sopProj.optimexecstatus = sopStatus("Failed");
            sopError("Optimization: FAILED");
            return
        end

        // Update project with optimizer output arguments values
        sopProj.optimalpoint = sopProj.optimizers(kOpt).optimalpoint;
        sopProj.objfunctionoptimalvalue = sopProj.optimizers(kOpt).objfunctionoptimalvalue;
    end

    sopProj.optimexecstatus = sopStatus("Done");
    sopLog("Optimization: DONE");

endfunction
