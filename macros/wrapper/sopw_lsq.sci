// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_lsq(action, object)

    value = [];

    select action
    case "type"
        value = "modeler";

    case "description"
        value = "lsq wrapper";

    case "version"
        value = [0 0 0];

    case "atomsmodules"

    case "functionsneeded"

    case "defaults"
        value = init_param();
        value = add_param(value, "modeltype", "linear");

    case "configure"
        modeltype = sopParameter("modeltype", "String", list("linear", "purelinear", "quadratic", "purequadratic"), "linear");
        value = list(modeltype);
        
    case "model"
        S = object.learningpoints;
        Y = object.learningresponses;
        modeltype = get_param(object.params, "modeltype");
        Smod = dmat(S,modeltype);
        object.coeffs = lsq(Smod, Y);

        //[n,q] = size(Smod);
        //resid = Y - Smod*object.coeffs;
        //SST = (Y-mean(Y))'*(Y-mean(Y));
        //SSR = sum(resid.^2);
        //SSM = SST-SSR;

        //R2 = 1-SSR/SST;
        //Q2 = 1-SSR/SSM;
        //R2adj = 1-(1-R2)*((n-1)/(n-q));
        //sigma = sqrt(sum(resid.^2)/(n-q));            
    case "estimate"
        plex = object.points
        coeffs = object.coeffs;
        coeffs = matrix(coeffs,length(coeffs),1);
        npoints=plex;
        fullPlex = sopGetDoe(object, 1, "goodpoints");
        maxi = max(fullPlex, "r")
        mini = min(fullPlex, "r");
        //npoints=normplex(points,mini,maxi);
        npoints=(2*plex-repmat(maxi,size(plex,1),1)-repmat(mini,size(plex,1),1)) ./ (repmat(maxi,size(plex,1),1)-repmat(mini,size(plex,1),1));


        modeltype = get_param(object.params, "modeltype");
        fnpoints = dmat(npoints,modeltype);
        object.predictions = fnpoints*coeffs;

    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_lsq", action))
    end
    
endfunction
function X=dmat(D,model)
[n,m]=size(D);

if strcmp(model,"cubic")==0 then
    model="poly3";
elseif strcmp(model,"cubiquepur")==0 then
    model="polyp3";
end

select(model)
case "purelinear"
   X=[ones(n,1) D];
case "linear"
   X=[ones(n,1) D inter(D)];
case "purequadratic"
   X=[ones(n,1) D D.^2];
case "quadratic"
   X=[ones(n,1) D D.^2 inter(D)];
else
   X=[];
   l=length(model);
   test0=strindex(model,"poly");
   if isempty(test0)
       error("L''argument model n''est pas bien défini");
       return;
   end
   
   test=strindex(model,"polyp");
   
   if isempty(test),
       ordre=eval(part(model,5:l));
   else
       ordre=eval(part(model,6:l));
   end
   
   if isempty(ordre)
       error("L''argument model n''est pas bien défini");
       return;       
   elseif ordre<=0
       disp("L''ordre du polynôme doit être strictement positif!");
       return;
   end
   
   for i=1:ordre,
       X=[X D.^i];
   end
   
   if ~isempty(test),
       X=[ones(n,1) X];
   else
       X=[ones(n,1) X inter(D)];
   end
end
endfunction
function subX=inter(X)
subX=[];
s=size(X);
for i=1:(s(2)-1),
    subX=[subX repmat(X(:,i),1,length((i+1):s(2))).*X(:,(i+1):s(2))];
end
endfunction



