// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_alternova(action, object)

    value = [];

    select action
    case "type"
        value = "modeler";

    case "description"
        if ~isempty(object) & ~isempty(get_param(object.params, "modelfilename")) then
            value = fileparts(get_param(object.params, "modelfilename"), "fname") + " - " + getModelName(object);
        else
            value = "Alternova Model";
        end

    case "version"
        value = [0 0 0];

    case "atomsmodules"

    case "functionsneeded"

    case "defaults"
        value = init_param();
        value = add_param(value, "modelfilename", "");

    case "configure"
        modelfilename = sopParameter("modelfilename", "Filename", "", "");
        value = list(modelfilename);

    case "model"
        object.coeffs = getRespIndex(object);

    case "estimate"
        plex = object.points;
        modelfilename = get_param(object.params, "modelfilename");
        index = object.coeffs;

        exec(modelfilename, -1); // Load the function
        execstr("predictions = " + fileparts(modelfilename, "fname") + "(plex'')")
        object.predictions = predictions(index, :);

    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_alternova", action))
    end
    
endfunction

function respIndex = getRespIndex(object)
        // Find variable in file to get its index
        modelFileTxt = mgetl(get_param(object.params, "modelfilename"));
        respName = object.responsename;
        lineIndex = grep(modelFileTxt, respName);
        [nValues, respIndex, modelName] = msscanf(modelFileTxt(lineIndex), "//  Y(%d,:) : %s");
endfunction

function modelName = getModelName(object)
        // Find variable in file to get its index
        modelFileTxt = mgetl(get_param(object.params, "modelfilename"));
        respName = object.responsename;
        lineIndex = grep(modelFileTxt, respName);
        [nValues, respIndex, modelName] = msscanf(modelFileTxt(lineIndex), "//  Y(%d,:) : %s");
        modelName = strsubst(modelName, "_" + respName, "");
endfunction
