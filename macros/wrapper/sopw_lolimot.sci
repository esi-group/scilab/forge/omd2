// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_lolimot(action, object)
    
    value = [];
    
    select action
    case "type"
        value = "modeler";
        
    case "description"
        value = "LOLIMOT wrapper";
        
    case "version"
        value = [0 0 0];

    case "atomsmodules"
        
    case "functionsneeded"
    
    case "defaults"
        value = init_param();
        value = add_param(value, "sigma", 0.33);
        value = add_param(value, "nbpart", 15);
        value = add_param(value, "maximp", 0.05);
        value = add_param(value, "nbcut", 3);
        value = add_param(value, "vec", %T);
        value = add_param(value, "loginfos", %T);
    
    case "configure"
        sigma = sopParameter("sigma", "Constant", [0 1], 0.33);
        nbpart = sopParameter("nbpart", "Constant", [0 +%inf], 15);
        maximp = sopParameter("maximp", "Constant", [0 1], 0.05);
        nbcut = sopParameter("nbcut", "Constant", [0 +%inf], 3);
        vec = sopParameter("vec", "Boolean", %T, %T);
        loginfos = sopParameter("log", "Boolean", [%T %F], %T);
        value = list(sigma, nbpart, maximp, nbcut, vec, loginfos)
    case "model"
        modelingParams = object.params;
        sigma = get_param(modelingParams, "sigma");
        nbpart = get_param(modelingParams, "nbpart")
        maximp = get_param(modelingParams, "maximp")
        nbcut = get_param(modelingParams, "nbcut")
        vec = get_param(modelingParams, "vec")
        loginfos = get_param(modelingParams, "loginfos")
        
        // Without validation points
        if isempty(object.validationpoints) then
            data_learn = [object.learningpoints object.learningresponses];
            modelOut = learn_lolimot(data_learn, sigma, nbpart, maximp, nbcut, vec, loginfos);
        else
            Vec = %T; // TODO BUG IN LOLIMOT
            // If some validation points are selected
            data_learn = [object.learningpoints object.learningresponses];
            data_valid = [object.validationpoints object.validationresponses];
            modelOut = learn_valid_lolimot(data_learn, data_valid, sigma, nbpart, maximp, nbcut, vec, loginfos);
        end
        object.coeffs = modelOut;
        
    case "estimate"
        predictedResponses = []
        lolModel = object.coeffs;
        x = object.points; // Points to predict

        for kPoint = 1:size(x, 1)
            predictedResponses = [predictedResponses ; estim_vec_lolimot(x(kPoint, :), lolModel)];
        end
        object.predictions = predictedResponses;

    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_lolimot", action))
    end
    
endfunction

