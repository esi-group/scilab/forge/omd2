function som_show(sMap)
    // SOM_SHOW Basic SOM visualizations: component planes, u-matrix etc.
    //
    // h = som_show(sMap, ['argID', value, ...])
    //
    //  som_show(sMap);
    //  som_show(sMap,'bar','none');
    //  som_show(sMap,'comp',[1:3],'umat','all');
    //  som_show(sMap,'comp',[1 2],'umat',{[1 2],'1,2 only'},'comp',[3:6]);
    //  som_show(sMap,'size',m,'bar','vert','edge','off');
    //
    // Input and output arguments ([]'s are optional):
    //  sMap        (struct) map struct
    //  [argID,     (string) Additional parameters are given as argID, value
    //    value]    (varies) pairs. See below for list of valid IDs and values.
    //
    //  h           (struct) struct with the following fields:
    //   .plane     (vector) handles to the axes objecets (subplots)
    //   .colorbar  (vector) handles to the colorbars. Colorbars for empty
    //                       grids & RGB color planes do not exist: the
    //                       value for them in the vector is -1.
    //   .label     (vector) handles to the axis labels
    //
    // Here are the valid argument IDs and corresponding values. M is
    // the number of map units
    //  'comp'               Which component planes to draw, title is
    //                       the name of the component (from sMap.comp_names)
    //              (vector) a vector of component indices
    //              (string) 'all' (or '' or []) for all components
    //  'compi'              as 'comp' but uses interpolated shading
    //  'umat'               Show u-matrix calculated using specified
    //                       components
    //              (vector) a vector of component indeces
    //              (string) 'all' (or '' or []) to use all components
    //              (cell)   of form {v, str} uses v as the vector, and put
    //                       str as title instead of the default 'U-matrix'
    //  'umati'              as 'umat' but uses interpolated shading of colors
    //  'empty'     (string) Make an empty plane using given string as title
    //  'color'              Set arbitrary unit colors explicitly
    //              (matrix) size Mx1 or Mx3, Mx1 matrix uses indexed
    //                       coloring;  Mx3 matrix (RGB triples as rows)
    //                       defines fixed unit colors
    //              (cell)   of from {color, str}. 'color' is the Mx1
    //                       or Mx3 RGB triple matrix and 'str' is title
    //                       string
    //  'colori'             as 'color' but uses interpolated shading of colors
    //  'norm'      (string) 'n' or 'd': Whether to show normalized 'n' or
    //                       denormalized 'd' data values on the
    //                       colorbar. By default denormalized values are used.
    //  'bar'       (string) Colorbar direction: 'horiz', 'vert' (default)
    //                       or 'none'
    //  'size'               size of the units
    //              (scalar) same size for each unit, default is 1
    //              (vector) size Mx1, individual size for each unit
    //  'edge'      (string) Unit edges on component planes 'on'
    //                       (default) or 'off'
    //  'footnote'  (string) Footnote string, sMap.name by default
    //  'colormap'  (matrix) user defined colormap
    //  'subplots'  (vector) size 1 x 2, the number of subplots in y- and
    //                       and x-directions (as in SUBPLOT command)
    //
    // If identifiers 'comp', 'compi', 'umat', 'umati', 'color', 'colori'
    // or 'empty' are not specified at all, e.g. som_show(sMap) or
    // som_show(sMap,'bar','none'), the U-matrix and all component planes
    // are shown.
    //
    // For more help, try 'type som_show' or check out online documentation.
    // See also SOM_SHOW_ADD, SOM_SHOW_CLEAR, SOM_UMAT, SOM_CPLANE, SOM_GRID.

    //%%%%%%%%%%% DETAILED DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //
    // som_show
    //
    // PURPOSE
    //
    // Shows basic visualizations of SOM: component planes, unified distance
    // matrices as well as empty planes and fixed color planes.
    //
    // SYNTAX
    //
    //  h = som_show(sMap)
    //  h = som_show(sMap, 'argID', value, ...)
    //
    // DESCRIPTION
    //
    // This function is used for basic visualization of the SOM. Four
    // kinds of SOM planes can be shown:
    //
    //  1. U-matrix (see SOM_UMAT) which shows clustering structure of
    //     the SOM. Either all or just part of the components can
    //     be used in calculating the U-matrix.
    //  2. component planes: each component plane shows the values of
    //     one variable in each map unit
    //  3. an empty plane which may be used as a base for, e.g., hit
    //     histogram visualization or labeling (see SOM_SHOW_ADD)
    //  4. a fixed or indexed color representation for showing color coding or
    //     clustering
    //
    // The component planes and u-matrices may have colorbars showing the
    // scale for the variable. The scale shows by default the values that
    // variables have in the map struct. It may be changed to show the
    // original data values (estimated by SOM_DENORMALIZE). In this case a
    // small 'd' appears below the colorbar. The orientation of these
    // colorbars may be changed, or they can be removed.
    //
    // By default the u-matrix - calculated using all variables - and all
    // component planes are shown. This is achieved by giving command
    // som_show(sMap) without any further arguments
    //
    // REQUIRED INPUT ARGUMENTS
    //
    // sMap  (struct) Map to be shown. If only this argument is
    //                specified, the function draws first the u-matrix
    //                calculated using all the variables followed by all
    //                the component planes.
    //
    // OPTIONAL INPUT ARGUMENTS
    //
    // (M is the number of map units)
    //
    // Optional arguments must be given as 'argID',value -pairs
    //
    // 'comp'      Defines the variabels to be shown as component planes.
    //    (vector) 1xN or Nx1 vector with integer positive numbers ranging
    //             from 1 to the number of variables in the map codebook
    //             (dim). This vector determines the variables to be show
    //             as component planes and their order. Note that the same
    //             component plane (the same variable index) is allowed to
    //             occur several times.
    //    (string) 'all' or '' or []. This uses all variables, that is, it's
    //             the same that using value [1:dim] where dim is the
    //             number of variables in the codebook.
    //
    // 'compi'     Same as 'comp' but uses a Gouraud shaded color plane
    //             (made using SOM_GRID function) instead of the cell-like
    //             visualization of 'comp' (made using SOM_CPLANE). Note
    //             that the color interpolation doesn't work strictly
    //             correctly on 'hexa' grid, as it uses rectangular grid
    //             (see SURF).
    //
    // 'umat'      Show U-matrix: value defines the variables to be used
    //             for calculating a u-matrix.
    //    (vector) as in 'comps'. However, multiple occurences of the
    //             same variable (same variable index) are ignored.
    //    (string) 'all' or '' or []. This uses all variables, that is,
    //             is the same as using value [1:dim] where dim is the
    //             number of variables in the codebook.
    //    (cell)   of form {v, str} where v is a valid index vector for 'umat'
    //             (see above) and str is a string that is used as a title
    //             for the u-matrix instead of the default title
    //             'U-matrix'. This may be useful if several u-matrices
    //             are shown in the same figure.
    //
    // 'umati'     Same as 'umat' but uses shaded color plane (see 'compi').
    //
    // 'empty'     Show an empty plane (patch edges only)
    //    (string) value is used as title
    //
    // 'color'     Define fixed RGB colors for the map units
    //    (matrix) a Mx3 matrix of RGB triples as rows
    //    (vector) a Mx1 vector of any values: sets indexed coloring using
    //             the current colormap (as SURF does)
    //    (matrix) a Mx3xN matrix of RGB triples as rows. This gives N
    //             color planes.
    //    (matrix) a Mx1xN matrix of any values: sets indexed coloring using
    //             the current colormap (as SURF does). This gives N
    //             color planes.
    //    (cell)   of form {rgb, str} where rgb is a Mx3 (xN) matrix of RGB
    //             triples as rows and str is a string that is used as
    //             title(s).
    //    (cell)   of form {v, str} where v is a Mx1(xN) matrix of values
    //             and str is a string that is used as title(s).
    //
    // 'colori'    Same as 'color' but uses shaded color plane (see 'compi').
    //
    // 'norm'      Defines whether to use normalized or denormalized
    //             values in the colorbar. If denormalized values are
    //             used, they are acquired from SOM_DENORMALIZE function
    //             using sMap.comp_norm field.
    //    (string) 'd' (default) for denormalized values and 'n' for
    //             normalized values. The corresponding letter appears
    //             below the colorbar.
    //
    // 'bar'       Define the direction of the colorbars for component planes
    //             and U-matrices or turn them completely off.
    //    (string) 'vert' (default), 'horiz' or 'none'. 'vert' gives
    //             vertical and 'horiz' horizontal colorbars. 'none'
    //             shows no colorbars at all.
    //
    // 'size'      Define sizes of the units.
    //    (scalar) all units have the same size (1 by default)
    //    (vector) size Mx1, each unit gets individual size scaling
    //             (as in SOM_CPLANE)
    //
    // 'edge'      Unit edges on component plane visualizations.
    //    (string) 'on' or 'off' determines whether the unit edges on component
    //             planes ('comp') are shown or not. Default is 'off'. Note that
    //             U-matrix and color planes are _always_ drawn without edges.
    //
    // 'footnote'  Text on the figure
    //    (string) is printed as a movable text object on the figure
    //             where it may be moved using mouse. Default value is the
    //             string in the sMap.name field. Note: value [] gives the
    //             string, but input value '' gives no footnote a all.
    //             See VIS_FOOTNOTE for more information on the text object
    //             and ways to change its font size.
    //
    // 'colormap'  som_show ghages the colormap by default to a gray-level map
    //    (matrix) This argument is used to set some other colormap.
    //
    // 'subplots'  the number of subplots in y- and x-directions, as in
    //    (vector) command SUBPLOT
    //
    // OUTPUT ARGUMENTS
    //
    // h (struct)
    //    .plane         (vector) handles to the axes objects (subplots)
    //    .colorbar      (vector) handles to the colorbars. Colorbars of empty
    //                            & color planes do not exist: the corresponding
    //                            value in the vector is -1
    //    .label         (vector) handles to the axis labels
    //
    // OBJECT TAGS
    //
    // The property field 'Tag' of the axis objects created by this function
    // are set to contain string 'Cplane' if the axis contains component plane
    // ('comp'), color plane ('color') or empty plane ('empty') and string
    // 'Uplane' if it contains a u-matrix ('umat'). The tag is set to
    // 'CplaneI' for planes created using 'compi' and 'colori', and
    // 'UplaneI' for 'umati'.
    //
    // FEATURES
    //
    // Note that when interpolated shading is used in coloring ('compi' and
    // 'colori') the standard built-in bilinear Gouraud interpolation for a
    // SURF object is used. If the lattice is hexagonal - or anything else than
    // rectangular in general - the result is not strictly what is looked
    // for, especially if the map is small.
    //
    // EXAMPLES
    //
    //// Make random data, normalize it, and give component names
    //// Make a map
    //
    //   data=som_data_struct(rand(1000,3),'comp_names',{'One','Two','Three'});
    //   data=som_normalize(data,'var');
    //   map=som_make(data);
    //
    //// Do the basic visualization with som_show: u-matrix and all
    //// component planes
    //
    //   som_show(map);
    //
    //// The values shown in the colorbar are denormalized codebook values
    //// (if denormalization is possible). To view the actual values, use
    //// the ..., 'norm', 'n' argument pair.
    //
    //   som_show(map,'norm','n')
    //
    //// Something more complex:
    //// Show 1-2. Component planes 1 and 2 (variables 'One' and 'Two')
    ////        3. U-matrix that is calculated only using variables
    ////           'One' and 'Two'
    ////           with title '1,2 only'
    ////        4. U-matrix that is calculated using all variables with the
    ////           deafult title 'U-matrix'
    ////        5. The color code (in c) with title 'Color code'
    ////        6. Component plane 3 (variable 'Three')
    //// and  use vertical colorbars and and the values
    //// But first: make a continuous color code (see som_colorcode)
    //
    // c=som_colorcode(map,'rgb1');
    //
    // som_show(map,'comp',[1 2],'umat',{1:2,'1,2 only'},'umat','all', ...
    //  'color',{c,'Color code'},'bar','vert','norm','n','comp',3)
    //
    //  SEE ALSO
    //
    // som_show_add   Show hits, labels and trajectories on SOM_SHOW visualization.
    // som_show_clear Clear hit marks, labels or trajectories from current figure.
    // som_umat       Compute unified distance matrix of self-organizing map.
    // som_grid       Visualization of a SOM grid.
    // som_cplane     Visualization of component, u-matrix and color planes.

    // Copyright (c) 1997-2000 by the SOM toolbox programming team.
    // http://www.cis.hut.fi/projects/somtoolbox/

    // Version 1.0beta johan 100298
    // Version 2.0beta johan 201099 juuso 181199 johan 011299-100200
    //                 juuso 130300 190600

    // Scilab version based on Matlab version and developed in the context of:
    // - OMD2 project (French National Agency for Research)
    // - CSDL project (Systematic competitiveness cluster)

    D=sMap.codebook;
    lattice = sMap.topol.lattice;
    msize = sMap.topol.msize;
    munits = prod(msize);
    comp_norm = sMap.comp_norm;

    [dlem dim]=size(D);

    for i=1:dim
        params = comp_norm(i).params;
        D(:,i) = params(2)*D(:,i)+params(1);
    end

    Dmin = min(D,'r');
    Dmax = max(D,'r');

    select lattice
    case 'hexa'
        patchform = vis_patch('hexa')
    case 'rect'
        patchform = vis_patch('rect')
    end

    unit_coords=som_vis_coords(lattice,msize);

    l=size(patchform,1);
    x=repmat(unit_coords(:,1)',l,1);
    y=repmat(unit_coords(:,2)',l,1);
    nx=repmat(patchform(:,1),1,munits);
    ny=repmat(patchform(:,2),1,munits);

    ncolor = 64;
    ncolor=128;

    Color=zeros(munits,dim);
    for i=1:munits
        for j=1:dim
            Color(i,j)=round((D(i,j)-Dmin(j))/(Dmax(j)-Dmin(j))*ncolor);
        end
    end

    n2 = ceil(sqrt(dim));
    n1 = ceil(dim/n2);

    xmin=min(x+nx);
    xmax=max(x+nx);
    ymin=min(y+ny);
    ymax=max(y+ny);

    fig = scf();
    fig.color_map=jetcolormap(ncolor)
    fig.background=-2;
    drawlater()
    for i=1:dim
        subplot(n1,n2,i)
        a=gca();
        a.data_bounds=[xmin,xmax,ymin,ymax];
        a.isoview="on";
        for k=munits:-1:1
            xfpoly(x(:,k)+nx(:,k),y(:,munits+1-k)+ny(:,munits+1-k))
        end
        c=a.children;
        for k=1:munits
            j = munits + 1 -k;
            c(k).background=Color(k,i);
        end
        c(1:munits).line_mode="off";
        colorbar(Dmin(i),Dmax(i));
        a.title.text=sMap.comp_names(i);
        a.tight_limits="on";
    end
    drawnow()
endfunction

