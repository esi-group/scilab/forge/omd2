function sMap=som_make(sD)
    //SOM_MAKE Create, initialize and train Self-Organizing Map.
    //
    // sMap = som_make(D, [[argID,] value, ...])
    //
    //  sMap = som_make(D);
    //  sMap = som_make(D, 'munits', 20);
    //  sMap = som_make(D, 'munits', 20, 'hexa', 'sheet');
    //  sMap = som_make(D, 'msize', [4 6 7], 'lattice', 'rect');
    //
    //  Input and output arguments ([]'s are optional):
    //   D        (matrix) training data, size dlen x dim
    //            (struct) data struct
    //   [argID,  (string) See below. The values which are unambiguous can
    //    value]  (varies) be given without the preceeding argID.
    //
    //   sMap     (struct) map struct
    //
    // Here are the valid argument IDs and corresponding values. The values
    // which are unambiguous (marked with '*') can be given without the
    // preceeding argID.
    //   'init'       *(string) initialization: 'randinit' or 'lininit' (default)
    //   'algorithm'  *(string) training: 'seq' or 'batch' (default) or 'sompak'
    //   'munits'      (scalar) the preferred number of map units
    //   'msize'       (vector) map grid size
    //   'mapsize'    *(string) do you want a 'small', 'normal' or 'big' map
    //                          Any explicit settings of munits or msize override this.
    //   'lattice'    *(string) map lattice, 'hexa' or 'rect'
    //   'shape'      *(string) map shape, 'sheet', 'cyl' or 'toroid'
    //   'neigh'      *(string) neighborhood function, 'gaussian', 'cutgauss',
    //                          'ep' or 'bubble'
    //   'topol'      *(struct) topology struct
    //   'som_topol','sTopol' = 'topol'
    //   'mask'        (vector) BMU search mask, size dim x 1
    //   'name'        (string) map name
    //   'comp_names'  (string array / cellstr) component names, size dim x 1
    //   'tracking'    (scalar) how much to report, default = 1
    //   'training'    (string) 'short', 'default', 'long'
    //                 (vector) size 1 x 2, first length of rough training in epochs,
    //                          and then length of finetuning in epochs
    //
    // For more help, try 'type som_make' or check out online documentation.
    // See also SOM_MAP_STRUCT, SOM_TOPOL_STRUCT, SOM_TRAIN_STRUCT,
    //          SOM_RANDINIT, SOM_LININIT, SOM_SEQTRAIN, SOM_BATCHTRAIN.

    //%%%%%%%%%%%% DETAILED DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //
    // som_make
    //
    // PURPOSE
    //
    // Creates, initializes and trains a SOM using default parameters.
    //
    // SYNTAX
    //
    //  sMap = som_make(D);
    //  sMap = som_make(...,'argID',value,...);
    //  sMap = som_make(...,value,...);
    //
    // DESCRIPTION
    //
    // Creates, initializes and trains a SOM with default parameters. Uses functions
    // SOM_TOPOL_STRUCT, SOM_TRAIN_STRUCT, SOM_DATA_STRUCT and SOM_MAP_STRUCT to come
    // up with the default values.
    //
    // First, the number of map units is determined. Unless they are
    // explicitly defined, function SOM_TOPOL_STRUCT is used to determine this.
    // It uses a heuristic formula of 'munits = 5*dlen^0.54321'. The 'mapsize'
    // argument influences the final number of map units: a 'big' map has
    // x4 the default number of map units and a 'small' map has x0.25 the
    // default number of map units.
    //
    // After the number of map units has been determined, the map size is
    // determined. Basically, the two biggest eigenvalues of the training
    // data are calculated and the ratio between sidelengths of the map grid
    // is set to this ratio. The actual sidelengths are then set so that
    // their product is as close to the desired number of map units as
    // possible.
    //
    // Then the SOM is initialized. First, linear initialization along two
    // greatest eigenvectors is tried, but if this can't be done (the
    // eigenvectors cannot be calculated), random initialization is used
    // instead.  After initialization, the SOM is trained in two phases:
    // first rough training and then fine-tuning. If the 'tracking'
    // argument is greater than zero, the average quantization error and
    // topographic error of the final map are calculated.
    //
    // REQUIRED INPUT ARGUMENTS
    //
    //  D           The data to use in the training.
    //     (struct) A data struct. If a struct is given, '.comp_names' field as
    //              well as '.comp_norm' field is copied to the map struct.
    //     (matrix) A data matrix, size dlen x dim. The data matrix may
    //              contain unknown values, indicated by NaNs.
    //
    // OPTIONAL INPUT ARGUMENTS
    //
    //  argID (string) Argument identifier string (see below).
    //  value (varies) Value for the argument (see below).
    //
    // Here are the valid argument IDs and corresponding values. The values
    // which are unambiguous (marked with '*') can be given without the
    // preceeding argID.
    //   'init'       *(string) initialization: 'randinit' or 'lininit' (default)
    //   'algorithm'  *(string) training: 'seq' or 'batch' (default) or 'sompak'
    //   'munits'      (scalar) the preferred number of map units
    //   'msize'       (vector) map grid size
    //   'mapsize'    *(string) do you want a 'small', 'normal' or 'big' map
    //                          Any explicit settings of munits or msize override this.
    //   'lattice'    *(string) map lattice, 'hexa' or 'rect'
    //   'shape'      *(string) map shape, 'sheet', 'cyl' or 'toroid'
    //   'neigh'      *(string) neighborhood function, 'gaussian', 'cutgauss',
    //                          'ep' or 'bubble'
    //   'topol'      *(struct) topology struct
    //   'som_topol','sTopol' = 'topol'
    //   'mask'        (vector) BMU search mask, size dim x 1
    //   'name'        (string) map name
    //   'comp_names'  (string array / cellstr) component names, size dim x 1
    //   'tracking'    (scalar) how much to report, default = 1
    //   'training'    (string) 'short', 'default' or 'long'
    //                 (vector) size 1 x 2, first length of rough training in epochs,
    //                          and then length of finetuning in epochs
    //
    // OUTPUT ARGUMENTS
    //
    //  sMap (struct) the trained map struct
    //
    // EXAMPLES
    //
    //  To simply train a map with default parameters:
    //
    //   sMap = som_make(D);
    //
    //  With the optional arguments, the initialization and training can be
    //  influenced. To change map size, use 'msize', 'munits' or 'mapsize'
    //  arguments:
    //
    //   sMap = som_make(D,'mapsize','big'); or sMap=som_make(D,'big');
    //   sMap = som_make(D,'munits', 100);
    //   sMap = som_make(D,'msize', [20 10]);
    //
    //  Argument 'algorithm' can be used to switch between 'seq' and 'batch'
    //  algorithms. 'batch' is the default, so to use 'seq' algorithm:
    //
    //   sMap = som_make(D,'algorithm','seq'); or sMap = som_make(D,'seq');
    //
    //  The 'tracking' argument can be used to control the amout of reporting
    //  during training. The argument is used in this function, and it is
    //  passed to the training functions. To make the function work silently
    //  set it to 0.
    //
    //   sMap = som_make(D,'tracking',0);
    //
    // SEE ALSO
    //
    //  som_map_struct   Create a map struct.
    //  som_topol_struct Default values for SOM topology.
    //  som_train_struct Default values for SOM training parameters.
    //  som_randinint    Random initialization algorithm.
    //  som_lininit      Linear initialization algorithm.
    //  som_seqtrain     Sequential training algorithm.
    //  som_batchtrain   Batch training algorithm.

    // Copyright (c) 1999-2000 by the SOM toolbox programming team.
    // http://www.cis.hut.fi/projects/somtoolbox/

    // Version 2.0beta juuso 111199

    // Scilab version based on Matlab version and developed in the context of:
    // - OMD2 project (French National Agency for Research)
    // - CSDL project (Systematic competitiveness cluster)

    data_name=sD.name;
    comp_names=sD.comp_names;
    comp_norm=sD.comp_norm;
    D=sD.data;
    [dlen dim]=size(D);

    topol = struct('type','som_topol','msize',[],'lattice','hexa','shape','sheet');
    neigh = 'gaussian';
    mask = ones(dim,1);
    name = 'SOM '+date();


    tracking = 1;
    algorithm = 'batch';
    initalg = 'lininit';
    training = 'default';
    mapsize='';

    munits = ceil(5*dlen^0.5);
    topol.msize(1) = round(sqrt(munits));
    topol.msize(2) = round(munits/topol.msize(1));

    munits=prod(topol.msize)

    A=zeros(dim,dim)+%inf;

    for i=1:dim
        D(:,i)=D(:,i)-mean(D(abs(D(:,i))<%inf,i));
    end
    for i=1:dim
        for j=i:dim
            c=D(:,i).*D(:,j);
            c=c(abs(c)<%inf);
            A(i,j)=sum(c)/length(c);
            A(j,i)=A(i,j);
        end
    end
    [V,S]=spec(A);
    eigval=diag(S);
    [y,ind]=gsort(eigval,'r','i');
    eigval=eigval(ind);

    if(eigval($)==0 | eigval($-1)*munits<eigval($))
        ratio = 1;
    else
        ratio = sqrt(eigval($)/eigval($-1));
    end

    if(topol.lattice == 'hexa')
        topol.msize(2)=min(munits,round(sqrt(munits/ratio*sqrt(0.75))));
    else
        topol.msize(2)=min(munits,round(sqrt(munits/ratio)));
    end
    topol.msize(1)=round(munits/topol.msize(2));

    codebook = rand(prod(topol.msize),dim);

    sMap=struct('type','som_map',...
    'codebook',codebook,...
    'topol',topol,...
    'labels',[],...
    'neigh',neigh,...
    'mask',mask,...
    'trainhist',[],...
    'name',name,...
    'comp_names',comp_names,...
    'comp_norm',comp_norm)


    sTrain = struct('type','som_train',...
    'algorithm','lininit',...
    'data_name','',...
    'neigh','',...
    'radius_ini',%nan,...
    'radius_fin',%nan,...
    'alpha_ini',%nan,...
    'alpha_type','',...
    'trainlen',%nan,...
    'time','')

    sMap.trainhist(1)=sTrain;

    sMap=som_lininit(D,sMap);



    sTrain = struct('type','som_train',...
    'algorithm',algorithm,...
    'data_name','',...
    'neigh',neigh,...
    'mask',mask,...
    'radius_ini',2,...
    'radius_fin',1,...
    'alpha_ini',%nan,...
    'alpha_type','inv',...
    'trainlen',5,...
    'time','' )

    msize = sMap.topol.msize;
    ms = max(msize);
    mpd = munits / dlen;

    // rought training
    sTrain.alpha_ini = 0.5;
    sTRain.radius_ini = max(1,ceil(ms/8));
    sTrain.radius_fin = max(1,sTrain.radius_ini/4);
    sTrain.trainlen = ceil(10*mpd);

    sMap.trainhist(2)=sTrain;

    select algorithm
    case 'seq'
        error('algorithm '+algorithm+' not implemented yet ! ')

    case 'batch'
        [sMap,sTrain]=som_batchtrain(sMap,D,sTrain,tracking)

    end



    //Fine Tune
    sTrain.radius_ini = sTrain.radius_fin;
    sTrain.radius_fin = 1;
    sTrain.trainlen = ceil(40*mpd);

    sMap.trainhist(3)=sTrain;


    select algorithm
    case 'seq'
        error('algorithm '+algorithm+' not implemented yet ! ')

    case 'batch'
        [sMap,sTrain]=som_batchtrain(sMap,D,sTrain,tracking)

    end
endfunction
