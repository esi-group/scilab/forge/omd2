function som_cplane(map,msize)
    //SOM_CPLANE Visualize one 2D component plane, U-matrix or color plane.
    //
    // h=som_cplane(lattice, msize, color, [s], [pos])
    // h=som_cplane(topol, color, [s], [pos])
    //
    //  som_cplane('hexa', [10 5], 'none');
    //  som_cplane('rect', [10 5], 'r');
    //  som_cplane(sM.topol, sM.codebook(:,1));
    //  U = som_umat(sM); som_cplane('hexaU',sM.topol.msize,U(:));
    //
    //  Input and output arguments ([]'s are optional):
    //   lattice   (string) 'hexa', 'rect' (component planes)
    //                      'hexaU', 'rectU' (corresponding U-matrices)
    //             (matrix) defines the patch (see function VIS_PATCH).
    //   msize     (vector) 1x2 vector defines grid size (M=prod(msize))
    //             (matrix) Mx2 matrix gives explicit coordinates for each node
    //   topol     (struct) map or topology struct
    //   color              color for the nodes
    //             (matrix) Mx1 matrix gives indexed colors for the units
    //                      Mx3 matrix of RGB triples gives explicit
    //                      color for each unit
    //                      (Note: in case of U-matrix, the number of color
    //                      values is 4*prod(msize)-2*sum(msize)+1, not prod(msize))
    //             (string) ColorSpec gives the same color for each node
    //                      'none' draws black edges only.
    //   [s]       (matrix) size Mx1, gives individual size scaling for each node
    //             (scalar) gives the same size for each node, default=1.
    //                      Additional features: see 'type som_cplane'
    //                      This argument is ignored if the lattice is 'rectU' or 'hexaU'.
    //   [pos]     (vector) a 1x2 vector that determines position of origin,
    //                      default is [1 1].
    //
    //   h         (scalar) the object handle for the PATCH object
    //
    // Axis are set to the 'ij' mode with equal spacing and turned off if
    // 'pos' is not given. If 'lattice' is 'rect', 'hexa', 'rectU' or
    // 'hexaU' the node (a,b) has coordinates (a,b) (+pos), except on the
    // even numbered rows on the 'hexa' and 'hexaU' grids where the
    // coordinates are (a,b+0.5) (+pos).
    //
    // For more help, try 'type som_cplane' or check out online documentation.
    // See also SOM_PIEPLANE, SOM_PLOTPLANE, SOM_BARPLANE, VIS_PATCH,
    //          SOM_VIS_COORDS

    //%%%%%%%%%%%% DETAILED DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //
    // som_cplane
    //
    // PURPOSE
    //
    // Visualizes a 2D component plane or u-matrix
    //
    // SYNTAX
    //
    //  h = som_cplane(topol, color)
    //  h = som_cplane(lattice, msize, color)
    //  h = som_cplane(lattice, msize, color)
    //  h = som_cplane(..., size)
    //  h = som_cplane(..., size, pos)
    //
    // DESCRIPTION
    //
    // Creates some basic visualizations of the SOM grid: the component plane and
    // the unified distance matrix. The routine draws the SOM grid as a patch
    // object according to the specifications given in the input arguments and
    // returns its object handle.
    //
    // Each unit of the map is presented by a polygon whose color, size, shape
    // and location can be specified in various ways. The usual procedure
    // is to choose the lattice and map size used in the map training. Then
    // the function creates the standard sheet shaped topological
    // representation of the map grid with hexagonal or rectangular units.
    // When the values from a map codebook component (or from SOM_UMAT)
    // are given to the function it produces an indexed coloring for the
    // units (as in SURF command). Another possibility is to give a fixed
    // RGB color for each unit explicitly.
    //
    // Special effects (variable unit size, location or shape) can be produced
    // giving different types of input variables.
    //
    // KNOWN BUGS
    //
    // Using 1x3 or 3x1 grids causes problem, as the MATLAB will treat the color
    // information vector 1x3 or 3x1 as a single RGB triple. So, using indexed
    // colors is not possible for this particular map size.
    //
    // It is not possible to specify explicit coordinates for map
    // consistig of just one unit as then the msize is interpreted as
    // map size.
    //
    // REQUIRED INPUT ARGUMENTS
    //
    // Note: M is the number of map units
    //
    // lattice  The basic shape of the map units
    //
    //   (string) 'hexa' or 'rect' creates standard component plane;
    //            'hexaU' or 'rectU' creates standard u-matrix.
    //   (matrix) Lx2 matrix defines the cornes of an arbitary polygon to be used
    //            as the unit marker. (L is the number of patch vertex: L=6 for
    //            'hexa' and L=4 for 'rect')
    //
    // msize    The size of the map grid
    //
    //   (vector) [n1 n2] vector defines the map size (height n1 units, width
    //            n2 units, total M=n1 x n2 units). The units will be placed to their
    //            topological locations to form a uniform hexagonal or rectangular grid.
    //   (matrix) Mx2 matrix defines arbitrary coordinates for the M units
    //            In this case the argument 'lattice' defines the unit form only.
    //
    // topol    Topology of the map grid
    //
    //   (struct) map or topology struct from which the topology is taken
    //
    // color    Unit colors
    //
    //   (string) (ColorSpec) gives the same color for each unit, 'none'
    //            draws black unit edges only.
    //   (vector) Mx1 column vector gives indexed color for each unit using the
    //            current colormap (see help colormap).
    //   (matrix) Mx3 matrix of RGB triples as rows gives each unit a fixed color.
    //
    // OPTIONAL INPUT ARGUMENTS
    //
    // Note: M is the number of map units.
    // Note: if unspecified or given empty values ('' or []) default
    // values are used for optional input arguments.
    //
    // s        The size scaling factors for the units
    //
    //   (scalar) scalar gives each unit the same size scaling:
    //            0   unit disappears (edges can be seen as a dot).
    //            1   by default unit has its normal size (ie. no scaling)
    //            >1  unit overlaps others
    //   (matrix) Mx1 double: each unit gets individual size scaling
    //
    // pos      Position of origin
    //
    //   (vector) This argument exists to be able drawing component planes
    //            in arbitrary locations in a figure. Note the operation:
    //            if this argument is given, the axis limits setting
    //            part in the routine is skipped and the limits setting
    //            will be left to be done by MATLAB's default
    //            operation.
    //
    // OUTPUT ARGUMENTS
    //
    // h (scalar) handle to the created patch object
    //
    // OBJECT TAGS
    //
    // One object handle is returned: field Tag is set to
    //  'planeC'  for component plane
    //  'planeU'  for U-matrix
    //
    // FEATURES
    //
    // There are some extra features in following arguments
    //
    // size
    //  - MxL matrix: radial scaling: the distance between
    //    the center of node m and its kth vertex is scaled by
    //    s(m,k).
    //  - Mx1x2 matrix: the uniform scaling is done separately for
    //    x- and y-directions
    //  - MxLx2 matrix: the scaling is done separately to x- and y-
    //    directions for each vertex.
    //
    // color
    //    Each vertex may be given individual color.
    //    The PATCH object interpolates the colors on the
    //    face if shading is turned to interp.
    //  - 1xMxL matrix: colormap index for each vertex
    //  - LxMx3 matrix: RGB color for each vertex
    //
    // Note: In both cases (size and color) the ordering of the patch
    // vertices in the "built-in" patches is the following
    //
    //          'rect'      'hexa'
    //            1 3          1
    //            2 4         5 2
    //                        6 3
    //                         4
    //
    // The color interpolation result seem to depend on the order
    // in which the patch vertices are defined. Anyway, it gives
    // unfavourable results in our case especially with hexa grid:
    // this is a MATLAB feature.
    //
    // EXAMPLES
    //
    // m=som_make(rand(100,4),'msize',[6 5])         // make a map
    //
    // // show the first variable plane using indexed color coding
    //
    // som_cplane(m.topol.lattice,m.topol.msize,m.codebook(:,1));
    // or som_cplane(m.topol,m.codebook(:,1));
    // or som_cplane(m,m.codebook(:,1));
    //
    // // show the first variable using different sized black units
    //
    // som_cplane(m,'k',m.codebook(:,1));
    //
    // // Show the u-matrix. First we have to calculate it.
    // // Note: som_umat returns a matrix therefore we write u(:) to get
    // // a vector which contains the values in the proper order.
    //
    // u=som_umat(m);
    // som_cplane('hexaU', m.topol.msize, u(:));
    //
    // // Show three first variables coded as RGB colors
    // // and turn the unit edges off
    //
    // h=som_cplane(m, m.codebook(:,1:3),1)
    // set(h,'edgecolor','none');
    //
    // // Try this! (see section FEATURES)
    //
    // som_cplane('rect',[5 5],'none',rand(25,4));
    // som_cplane('rect',[5 5],rand(1,25,4));
    //
    // SEE ALSO
    //
    // som_barplane   Visualize the map prototype vectors as bar diagrams
    // som_plotplane  Visualize the map prototype vectors as line graphs
    // som_pieplane   Visualize the map prototype vectors as pie charts
    // som_umat       Compute unified distance matrix of self-organizing map
    // vis_patch      Define the basic patches used in som_cplane
    // som_vis_coords The default 'hexa' and 'rect' coordinates in visualizations

    // Copyright (c) 1999-2000 by the SOM toolbox programming team.
    // http://www.cis.hut.fi/projects/somtoolbox/

    // Version 2.0beta Johan 061099 juuso 151199 juuso 070600

    // Scilab version based on Matlab version and developed in the context of:
    // - OMD2 project (French National Agency for Research)
    // - CSDL project (Systematic competitiveness cluster)

    [munits dim]=size(map);
    n2=ceil(sqrt(dim));
    n1=ceil(dim/n2);

    s1=min(map,'r');
    s2=max(map,'r');

    //hexagon
    patchform=[[0 0.5774];...
    [0.5 0.2887];...
    [0.5 -0.2887];...
    [0 -0.5774];...
    [-0.5 -0.2887];...
    [-0.5 0.2887]];

    //hexa
    patchform= [[0 0.6667];...
    [0.5 0.3333];...
    [0.5 -0.3333];...
    [0 -0.6667];...
    [-0.5 -0.3333];...
    [-0.5 0.3333]];

    unit_coord(:,1)=matrix(repmat([1:msize(2)],msize(1),1),1,munits)';
    unit_coord(:,2)=repmat([1:msize(1)]',msize(2),1);
    d=rem(unit_coord(:,2),2) == 0
    unit_coord(d,1)=unit_coord(d,1)+.5;

    l=size(patchform,1)
    x=repmat(unit_coord(:,1)',l,1);

    y=repmat(unit_coord(:,2)',l,1);
    nx=repmat(patchform(:,1),1,munits);
    ny=repmat(patchform(:,2),1,munits);

    xmin=min(x+nx);xmax=max(x+nx);
    ymin=min(y+ny);ymax=max(y+ny);
    // subplot

    f=gcf();
    f.color_map=jetcolormap(64);
    drawlater()
    for i=1:dim
        subplot(n1,n2,i)
        for j=1:munits
            xfpoly(x(:,j)+nx(:,j),y(:,j)+ny(:,j))
        end
        a=gca();
        a.data_bounds=[xmin,xmax,ymin,ymax];
        a.isoview='on';
        c=a.children;
        for j=1:munits
            id = munits +1 - j;
            c(id).background = round((map(id,i)-s1(i))/(s2(i)-s1(i))*64);
        end
        colorbar(s1(i),s2(i))
    end
    drawnow()
endfunction
