function [sMap,sTrain]=som_batchtrain(sMap,D,sTRain,tracking)
    //SOM_BATCHTRAIN  Use batch algorithm to train the Self-Organizing Map.
    //
    // [sM,sT] = som_batchtrain(sM, D, [argID, value, ...])
    //
    //  sM     = som_batchtrain(sM,D);
    //  sM     = som_batchtrain(sM,sD,'radius',[10 3 2 1 0.1],'tracking',3);
    //  [M,sT] = som_batchtrain(M,D,'ep','msize',[10 3],'hexa');
    //
    //  Input and output arguments ([]'s are optional):
    //   sM      (struct) map struct, the trained and updated map is returned
    //           (matrix) codebook matrix of a self-organizing map
    //                    size munits x dim or  msize(1) x ... x msize(k) x dim
    //                    The trained map codebook is returned.
    //   D       (struct) training data; data struct
    //           (matrix) training data, size dlen x dim
    //   [argID, (string) See below. The values which are unambiguous can
    //    value] (varies) be given without the preceeding argID.
    //
    //   sT      (struct) learning parameters used during the training
    //
    // Here are the valid argument IDs and corresponding values. The values which
    // are unambiguous (marked with '*') can be given without the preceeding argID.
    //   'mask'       (vector) BMU search mask, size dim x 1
    //   'msize'      (vector) map size
    //   'radius'     (vector) neighborhood radiuses, length 1, 2 or trainlen
    //   'radius_ini' (scalar) initial training radius
    //   'radius_fin' (scalar) final training radius
    //   'tracking'   (scalar) tracking level, 0-3
    //   'trainlen'   (scalar) training length in epochs
    //   'train'     *(struct) train struct, parameters for training
    //   'sTrain','som_train'  = 'train'
    //   'neigh'     *(string) neighborhood function, 'gaussian', 'cutgauss',
    //                         'ep' or 'bubble'
    //   'topol'     *(struct) topology struct
    //   'som_topol','sTopol'  = 'topol'
    //   'lattice'   *(string) map lattice, 'hexa' or 'rect'
    //   'shape'     *(string) map shape, 'sheet', 'cyl' or 'toroid'
    //   'weights'    (vector) sample weights: each sample is weighted
    //
    // For more help, try 'type som_batchtrain' or check out online documentation.
    // See also  SOM_MAKE, SOM_SEQTRAIN, SOM_TRAIN_STRUCT.

    //%%%%%%%%%%%% DETAILED DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //
    // som_batchtrain
    //
    // PURPOSE
    //
    // Trains a Self-Organizing Map using the batch algorithm.
    //
    // SYNTAX
    //
    //  sM = som_batchtrain(sM,D);
    //  sM = som_batchtrain(sM,sD);
    //  sM = som_batchtrain(...,'argID',value,...);
    //  sM = som_batchtrain(...,value,...);
    //  [sM,sT] = som_batchtrain(M,D,...);
    //
    // DESCRIPTION
    //
    // Trains the given SOM (sM or M above) with the given training data
    // (sD or D) using batch training algorithm.  If no optional arguments
    // (argID, value) are given, a default training is done. Using optional
    // arguments the training parameters can be specified. Returns the
    // trained and updated SOM and a train struct which contains
    // information on the training.
    //
    // REFERENCES
    //
    // Kohonen, T., "Self-Organizing Map", 2nd ed., Springer-Verlag,
    //    Berlin, 1995, pp. 127-128.
    // Kohonen, T., "Things you haven't heard about the Self-Organizing
    //    Map", In proceedings of International Conference
    //    on Neural Networks (ICNN), San Francisco, 1993, pp. 1147-1156.
    //
    // KNOWN BUGS
    //
    // Batchtrain does not work correctly for a map with a single unit.
    // This is because of the way 'min'-function works.
    //
    // REQUIRED INPUT ARGUMENTS
    //
    //  sM          The map to be trained.
    //     (struct) map struct
    //     (matrix) codebook matrix (field .data of map struct)
    //              Size is either [munits dim], in which case the map grid
    //              dimensions (msize) should be specified with optional arguments,
    //              or [msize(1) ... msize(k) dim] in which case the map
    //              grid dimensions are taken from the size of the matrix.
    //              Lattice, by default, is 'rect' and shape 'sheet'.
    //  D           Training data.
    //     (struct) data struct
    //     (matrix) data matrix, size [dlen dim]
    //
    // OPTIONAL INPUT ARGUMENTS
    //
    //  argID (string) Argument identifier string (see below).
    //  value (varies) Value for the argument (see below).
    //
    //  The optional arguments can be given as 'argID',value -pairs. If an
    //  argument is given value multiple times, the last one is
    //  used. The valid IDs and corresponding values are listed below. The values
    //  which are unambiguous (marked with '*') can be given without the
    //  preceeding argID.
    //
    //  Below is the list of valid arguments:
    //   'mask'       (vector) BMU search mask, size dim x 1. Default is
    //                         the one in sM (field '.mask') or a vector of
    //                         ones if only a codebook matrix was given.
    //   'msize'      (vector) map grid dimensions. Default is the one
    //                         in sM (field sM.topol.msize) or
    //                         'si = size(sM); msize = si(1:end-1);'
    //                         if only a codebook matrix was given.
    //   'radius'     (vector) neighborhood radius
    //                         length = 1: radius_ini = radius
    //                         length = 2: [radius_ini radius_fin] = radius
    //                         length > 2: the vector given neighborhood
    //                                     radius for each step separately
    //                                     trainlen = length(radius)
    //   'radius_ini' (scalar) initial training radius
    //   'radius_fin' (scalar) final training radius
    //   'tracking'   (scalar) tracking level: 0, 1 (default), 2 or 3
    //                         0 - estimate time
    //                         1 - track time and quantization error
    //                         2 - plot quantization error
    //                         3 - plot quantization error and two first
    //                             components
    //   'trainlen'   (scalar) training length in epochs
    //   'train'     *(struct) train struct, parameters for training.
    //                         Default parameters, unless specified,
    //                         are acquired using SOM_TRAIN_STRUCT (this
    //                         also applies for 'trainlen', 'radius_ini'
    //                         and 'radius_fin').
    //   'sTrain', 'som_topol' (struct) = 'train'
    //   'neigh'     *(string) The used neighborhood function. Default is
    //                         the one in sM (field '.neigh') or 'gaussian'
    //                         if only a codebook matrix was given. Other
    //                         possible values is 'cutgauss', 'ep' and 'bubble'.
    //   'topol'     *(struct) topology of the map. Default is the one
    //                         in sM (field '.topol').
    //   'sTopol', 'som_topol' (struct) = 'topol'
    //   'lattice'   *(string) map lattice. Default is the one in sM
    //                         (field sM.topol.lattice) or 'rect'
    //                         if only a codebook matrix was given.
    //   'shape'     *(string) map shape. Default is the one in sM
    //                         (field sM.topol.shape) or 'sheet'
    //                         if only a codebook matrix was given.
    //   'weights'    (vector) weight for each data vector: during training,
    //                         each data sample is weighted with the corresponding
    //                         value, for example giving weights = [1 1 2 1]
    //                         would have the same result as having third sample
    //                         appear 2 times in the data
    //
    // OUTPUT ARGUMENTS
    //
    //  sM          the trained map
    //     (struct) if a map struct was given as input argument, a
    //              map struct is also returned. The current training
    //              is added to the training history (sM.trainhist).
    //              The 'neigh' and 'mask' fields of the map struct
    //              are updated to match those of the training.
    //     (matrix) if a matrix was given as input argument, a matrix
    //              is also returned with the same size as the input
    //              argument.
    //  sT (struct) train struct; information of the accomplished training
    //
    // EXAMPLES
    //
    // Simplest case:
    //  sM = som_batchtrain(sM,D);
    //  sM = som_batchtrain(sM,sD);
    //
    // To change the tracking level, 'tracking' argument is specified:
    //  sM = som_batchtrain(sM,D,'tracking',3);
    //
    // The change training parameters, the optional arguments 'train','neigh',
    // 'mask','trainlen','radius','radius_ini' and 'radius_fin' are used.
    //  sM = som_batchtrain(sM,D,'neigh','cutgauss','trainlen',10,'radius_fin',0);
    //
    // Another way to specify training parameters is to create a train struct:
    //  sTrain = som_train_struct(sM,'dlen',size(D,1));
    //  sTrain = som_set(sTrain,'neigh','cutgauss');
    //  sM = som_batchtrain(sM,D,sTrain);
    //
    // By default the neighborhood radius goes linearly from radius_ini to
    // radius_fin. If you want to change this, you can use the 'radius' argument
    // to specify the neighborhood radius for each step separately:
    //  sM = som_batchtrain(sM,D,'radius',[5 3 1 1 1 1 0.5 0.5 0.5]);
    //
    // You don't necessarily have to use the map struct, but you can operate
    // directly with codebook matrices. However, in this case you have to
    // specify the topology of the map in the optional arguments. The
    // following commads are identical (M is originally a 200 x dim sized matrix):
    //  M = som_batchtrain(M,D,'msize',[20 10],'lattice','hexa','shape','cyl');
    //   or
    //  M = som_batchtrain(M,D,'msize',[20 10],'hexa','cyl');
    //   or
    //  sT= som_set('som_topol','msize',[20 10],'lattice','hexa','shape','cyl');
    //  M = som_batchtrain(M,D,sT);
    //   or
    //  M = reshape(M,[20 10 dim]);
    //  M = som_batchtrain(M,D,'hexa','cyl');
    //
    // The som_batchtrain also returns a train struct with information on the
    // accomplished training. This struct is also added to the end of the
    // trainhist field of map struct, in case a map struct was given.
    //  [M,sTrain] = som_batchtrain(M,D,'msize',[20 10]);
    //  [sM,sTrain] = som_batchtrain(sM,D); // sM.trainhist{end}==sTrain
    //
    // SEE ALSO
    //
    //  som_make         Initialize and train a SOM using default parameters.
    //  som_seqtrain     Train SOM with sequential algorithm.
    //  som_train_struct Determine default training parameters.

    // Copyright (c) 1997-2000 by the SOM toolbox programming team.
    // http://www.cis.hut.fi/projects/somtoolbox/

    // Version 1.0beta juuso 071197 041297
    // Version 2.0beta juuso 101199

    // Scilab version based on Matlab version and developed in the context of:
    // - OMD2 project (French National Agency for Research)
    // - CSDL project (Systematic competitiveness cluster)

    topol = sMap.topol;
    [munits dim]=size(sMap.codebook);
    nonempty = find(sum(isnan(D),'c') < dim);
    D = D(nonempty,:);
    [dlen ddim] = size(D);
    if (dim ~= ddim )
        error('Map and data input space dimension disagree ! ');
    end
    weights = 1;
    msize = topol.msize;
    lattice = topol.lattice;
    neigh = sTrain.neigh;
    radius_ini = sTrain.radius_ini;
    radius_fin = sTrain.radius_fin;

    M = sMap.codebook;
    mask = sTrain.mask;
    trainlen = sTrain.trainlen;

    if(trainlen ==1)
        radius = radius_ini;
    else
        r0 = radius_ini;
        r1 = radius_fin;
        rtemp = r1+(0:(trainlen-1))/(trainlen-1)*(r0-r1);
        radius = rtemp($:-1:1);
    end

    Ud = zeros(munits,munits);

    if(topol.shape ~='sheet')
        error('shape '+topol.shape+' not implemented yet')
    else

        //        mdim = length(msize);
        //        Coords = zeros(munits,mdim);
        //        k = [1 cumprod(msize(1:$-1))];
        //        inds = [0:(munits-1)]';
        //        for i=mdim:-1:1
        //            Coords(:,i)=floor(inds(k(i)));
        //            inds = rem(inds,k(i));
        //        end
        //        Coords=Coords(:,$:-1:1);

        Coords = som_unit_coords(msize,topol.lattice,topol.shape);

        dx = max(Coords(:,1))-min(Coords(:,1));
        if(msize(1) > 1)
            dx = dx*msize(1)/(msize(1)-1);
        else
            dx = dx + 1;
        end
        dy = max(Coords(:,2))-min(Coords(:,2));
        if(msize(2)> 1)
            dy = dy*msize(2)/(msize(2)-1);
        else
            dy=dy + 1;
        end
        for i=1:(munits - 1)
            inds = [(i+1):munits];
            Dco = (Coords(inds,:)-Coords(ones(1,munits-i)*i,:))';
            Ud(i,inds)=sqrt(sum(Dco.^2,'r'));
        end
    end
    Ud = Ud + Ud';
    Ud = Ud.^2;

    radius = radius.^2;
    radius(find(radius==0))=%eps;

    Known = ~isnan(D);
    W1 = (mask*ones(1,dlen)).* Known';
    D(find(~Known))=0;

    WD = 2*diag(mask)*D';
    dconst = ((D.^2)*mask)';

    qe = zeros(trainlen,1);

    blen = min(munits,dlen);
    bmus = zeros(1,dlen);
    ddists = zeros(1,dlen);

    // Training

    for t=1:trainlen
        i0 = 0
        while i0+1 < dlen
            inds = [(i0+1):min(dlen,i0+blen)];
            i0 = i0 + blen;
            Dist = (M.^2)*W1(:,inds) - M*WD(:,inds);
            [ddists(inds),bmus(inds)] = min(Dist,'m');
        end
        if(tracking >0)
            ddists = ddists + dconst;
            ddists(ddists <0) = 0;
            qe(t) = mean(sqrt(ddists));
        end
        select neigh
        case 'bubble',   H = (Ud<=radius(t));
        case 'gaussian', H = exp(-Ud/(2*radius(t)));
        case 'curgauss', H = exp(-Ud/(2*radius(t))) .* (Ud<=radius(t));
        case 'ep',       H = (1-Ud/radius(t)) .* (Ud<=radius(t));
        end


        P = sparse([bmus' (1:dlen)'],repmat(weights,dlen,1),[munits dlen]);

        S = H * (P*D);

        A = H * (P*ones(Known));

        nonzero = find(A>0);
        M(nonzero) = S(nonzero) ./ A(nonzero);
    end
    sMap.codebook = M;
endfunction
