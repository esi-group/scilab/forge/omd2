// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [f,con]=cout_test(x)
    global neval
    neval = neval + 1;
    nx=size(x,1);
    pb = "CONSTR";
    // pb = 'BNH';
    // pb = 'SRN';
    // pb = 'KITA';

    select pb
    case "BNH"
        // BNH
        f(:,1)=4*x(:,1).^2+4*x(:,2).^2;
        f(:,2)=(x(:,1)-5).^2+(x(:,2)-5).^2;

        con(:,1)=1/25*((x(:,1)-5).^2+x(:,2).^2)-1;
        con(:,2)=1-1/7.7*((x(:,1)-8).^2+(x(:,2)+3).^2);

    case "CONSTR"
        // CONSTR

        f(:,1)=x(:,1);
        f(:,2)=(1+x(:,2))./x(:,1);

        con(:,1) = 6-(9*x(:,1)+x(:,2));
        con(:,2) = 1 -(9*x(:,1) - x(:,2));

        //         con(:,1)=-ones(nx,1);
        //         con(:,2)=-ones(nx,1);

    case "KITA"
        // KITA
        f(:,1)=-(-x(:,1).^2 + x(:,2));
        f(:,2)=-(0.5*x(:,1)+x(:,2)+1);

        con(:,1)=x(:,1)/6+x(:,2)-13/2;
        con(:,2)=.5*x(:,1)+x(:,2)-15/2;
        con(:,3)=5*x(:,1)+x(:,2)-30;


    case "SRN"

        //SRN

        f(:,1)=(x(:,1)-2).^2+(x(:,2)-1).^2+2;
        f(:,2)=9*x(:,1)-(x(:,2)-1).^2;

        con(:,1)=1/225*(x(:,1).^2+x(:,2).^2)-1;
        con(:,2)=1/10*(x(:,1)-3*x(:,2))+1;

    case "PB2"
        //PB2

        f(:,1)=x(:,1);
        f(:,2)=x(:,2);

        con(:,1)=1+.1*cos(16*atan(x(:,1)./x(:,2)))-x(:,1).^2-x(:,2).^2;
        con(:,2)=(x(:,1)-.5).^2+(x(:,2)-.5).^2-.5;


    end
endfunction