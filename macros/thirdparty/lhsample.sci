// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = lhsample(n, d)
// Latin hypercube sample
// n: number of points
// d: space dimension
  X = (grand(d, 'prm', (0 : (n - 1))') + grand(n, d, 'def')) / n
endfunction
