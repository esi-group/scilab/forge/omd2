// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function doeGeneratorObj = generic_i_sopdoege(propertyName, propertyValue, doeGeneratorObj)

    if typeof(doeGeneratorObj)<>"sopdoege" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopdoege", 3, "sopdoege"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopdoege", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopdoege", 1));
    end

    propertyName = "<" + convstr(propertyName, "l") + ">"

    select propertyName
    case "<funname>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "FunName"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "FunName"));
        end
        doeGeneratorObj(propertyName) = propertyValue;
    case "<params>"
        if typeof(propertyValue)<>"plist" then
            error(msprintf("Wrong type for ''%s'' property: A ''plist'' tlist expected.\n", "InParams"));
        end
        doeGeneratorObj(propertyName) = propertyValue;
    case "<values>"
        // TODO type and size check
        doeGeneratorObj(propertyName) = propertyValue;
    case "<version>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 3, 1));
        end
        if or(size(propertyValue)<>[1 3]) then
            error(msprintf("Wrong size for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 3, 1));
        end
        doeGeneratorObj(propertyName) = propertyValue;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end

endfunction

