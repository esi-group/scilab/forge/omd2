// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGraphResponses()

    datagui = findobj("Tag", "sopdatagui");

    sopProj = sopGetCurrentProject(datagui);

    if size(sopProj.responses)==0 then
        messagebox("Project has no reponses.", "Error", "error");
        return
    end

    // Visualization
    fig = sopFigure(_("Responses"));
    drawlater(); // drawnow will be called in sopPageManager
    allAxes = [];
    for kResp = 1:size(sopProj.responses)
        legStrings = [];
        legHandles = []
        
        a = sopSubplot(1, 1, kResp);
        allAxes($+1) = a;
        
        lPoints = sopProj.responses(kResp).values(sopProj.responses(kResp).valuestype==1);
        plot(lPoints, ".b", "MarkerSize", 4);
        legHandles($+1) = get(gce(), "children");
        legString($+1) = _("Learning Points");
        
        vPoints = sopProj.responses(kResp).values(sopProj.responses(kResp).valuestype==2);
        plot(vPoints, ".r", "MarkerSize", 4);
        legHandles($+1) = get(gce(), "children");
        legString($+1) = _("Validation Points");
        
        hLeg = legend(legHandles, legString, -5);
        hLeg.background = -2;
        sopSetAxesLabels(a, "", sopProj.responses(kResp).name);
        a.tight_limits = "on";
    end
    sopPageManager(fig, allAxes)
endfunction

