// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function updateSopDataGui()

    sopdatagui = findobj("Tag", "sopdatagui");

    sopproject = sopGetCurrentProject(sopdatagui);

    if isempty(sopproject) then // At application start
        return
    end

    // Menus
    fileMenu = findobj("Tag", "file_menu");
    fileMenu.enable = "on";
    loaddatafileMenu = findobj("Tag", "loaddatafile_menu");
    loaddatafileMenu.enable = "on";
    quitMenu = findobj("Tag", "quit_menu");
    quitMenu.enable = "on";
    factorsMenu = findobj("Tag", "factors_menu");
    factorsMenu.enable = "on";
    generateFactorsMenu = findobj("Tag", "generatefactors_menu");
    if isempty(sopproject.doegenerators) then
        generateFactorsMenu.enable = "off";
    else
        generateFactorsMenu.enable = "on";
    end
    viewFactorsMenu = findobj("Tag", "viewfactors_menu");
    enabled = "on";
    for kFact = 1:size(sopproject.factors)
        if isempty(sopproject.factors(kFact).values) then
            enabled = "off";
            break
        end
    end
    viewFactorsMenu.enable = enabled;
    responsesMenu = findobj("Tag", "responses_menu");
    responsesMenu.enable = "on";
    simulateResponsesMenu = findobj("Tag", "simulateresponses_menu");
    if isempty(sopproject.simulator) then
        simulateResponsesMenu.enable = "off";
    else
        enabled = "on";
        for kFact = 1:size(sopproject.factors)
            if isempty(sopproject.factors(kFact).values) then
                enabled = "off";
                break
            end
        end
        simulateResponsesMenu.enable = enabled;
    end
    viewResponsesMenu = findobj("Tag", "viewresponses_menu");
    enabled = "on";
    for kResp = 1:size(sopproject.responses)
        if isempty(sopproject.responses(kResp).values) then
            enabled = "off";
            break
        end
    end
    viewResponsesMenu.enable = enabled;

    // Window management buttons
    hdls = sopGuiHandles(sopdatagui);
    if isempty(sopproject.pointedfile) then
        hdls.reset_btn.enable = "off";
    else
        hdls.reset_btn.enable = "on";
    end
    hdls.save_btn.enable = "on";
    hdls.saveclose_btn.enable = "on";
    hdls.close_btn.enable = "on";

    // Buttons
    var2respbtn = findobj("Tag", "variablestoresponses_btn");
    resp2varbtn = findobj("Tag", "responsestovariables_btn");
    var2factbtn = findobj("Tag", "variablestofactors_btn");
    fact2varbtn = findobj("Tag", "factorstovariables_btn");

    // Variables listbox
    variablesListbox = findobj("Tag", "variables_listbox");
    if sopproject.datasource=="doe" then
        variablesListbox.enable = "off";
        variablesListbox.string = "";
        var2respbtn.enable = "off";
        var2factbtn.enable = "off";
    else
        projectVariables = sopproject.variables;
        if ~isempty(projectVariables) then
            varNames = [];
            for kVar = 1:size(projectVariables)
                varNames($+1) = projectVariables(kVar).name;
            end
            if or(varNames<>variablesListbox.string') then
                variablesListbox.string = varNames;
            end
            variablesListbox.enable = "on";

            if ~isempty(variablesListbox.value) then
                var2respbtn.enable = "on";
                var2factbtn.enable = "on";
            else
                var2respbtn.enable = "off";
                var2factbtn.enable = "off";
            end
        else
            variablesListbox.enable = "off";
            variablesListbox.string = "";
            var2respbtn.enable = "off";
            var2factbtn.enable = "off";
        end
    end

    // Responses frame
    responsesListbox = findobj("Tag", "responses_listbox");
    addResponseBtn = findobj("Tag", "addresponse_btn");
    delResponseBtn = findobj("Tag", "delresponse_btn");
    editResponseBtn = findobj("Tag", "editresponse_btn");
    projectResponses = sopproject.responses;
    if sopproject.datasource=="file" then
        addResponseBtn.enable = "off";
        delResponseBtn.enable = "off";
        editResponseBtn.enable = "off";
    else
        addResponseBtn.enable = "on";
    end
    if size(projectResponses)<>0 then
        respNames = [];
        for kResp = 1:size(projectResponses)
            respNames($+1) = projectResponses(kResp).name;
        end
        if or(respNames<>responsesListbox.string') then
            responsesListbox.string = respNames;
        end
        responsesListbox.enable = "on";

        if ~isempty(responsesListbox.value) then
            if sopproject.datasource=="file" then
                resp2varbtn.enable = "on";
            else
                delResponseBtn.enable = "on";
                editResponseBtn.enable = "on";
            end
        else
            delResponseBtn.enable = "off";
            editResponseBtn.enable = "off";
            resp2varbtn.enable = "off";
        end
    else
        responsesListbox.enable = "off";
        responsesListbox.string = "";
        resp2varbtn.enable = "off";
        delResponseBtn.enable = "off";
        editResponseBtn.enable = "off";
    end

    // Factors frame
    factorsListbox = findobj("Tag", "factors_listbox");
    addFactorBtn = findobj("Tag", "addfactor_btn");
    delFactorBtn = findobj("Tag", "delfactor_btn");
    editFactorBtn = findobj("Tag", "editfactor_btn");
    projectFactors = sopproject.factors;
    if sopproject.datasource=="file" then
        addFactorBtn.enable = "off";
        delFactorBtn.enable = "off";
        editFactorBtn.enable = "off";
    else
        addFactorBtn.enable = "on";
    end
    if ~isempty(projectFactors) then
        factNames = [];
        for kFact = 1:size(projectFactors)
            factNames($+1) = projectFactors(kFact).name;
        end
        if or(factNames<>factorsListbox.string') then
            factorsListbox.string = factNames;
        end
        factorsListbox.enable = "on";

        if ~isempty(factorsListbox.value) then
            if sopproject.datasource=="file" then
                fact2varbtn.enable = "on";
            else
                delFactorBtn.enable = "on";
                editFactorBtn.enable = "on";
            end
        else
            delFactorBtn.enable = "off";
            editFactorBtn.enable = "off";
            fact2varbtn.enable = "off";
        end
    else
        factorsListbox.enable = "off";
        factorsListbox.string = "";
        fact2varbtn.enable = "off";
        delFactorBtn.enable = "off";
        editFactorBtn.enable = "off";
    end

    // Factors configuration
    generatorsListbox = findobj("Tag", "generators_listbox");
    addGeneratorBtn = findobj("Tag", "addgenerator_btn");
    delGeneratorBtn = findobj("Tag", "delgenerator_btn");
    editGeneratorBtn = findobj("Tag", "editgenerator_btn");
    projectGenerators = sopproject.doegenerators;
    if sopproject.datasource=="file" then
        addGeneratorBtn.enable = "off";
        delGeneratorBtn.enable = "off";
        editGeneratorBtn.enable = "off";
    else
        addGeneratorBtn.enable = "on";
    end
    if size(projectGenerators)<>0 then
        generatorsNames = [];
        for kGen = 1:size(projectGenerators)
            generatorsNames($+1) = projectGenerators(kGen).funname // TODo Get description here
        end
        if or(size(projectGenerators)<>size(generatorsListbox.string')) | or(generatorsNames<>generatorsListbox.string') then
            if isempty(projectGenerators) then // Manage []
                generatorsListbox.string = "";
            else
                generatorsListbox.string = generatorsNames;
            end
        end
        generatorsListbox.enable = "on";

        if ~isempty(generatorsListbox.value) then
            if sopproject.datasource=="doe" then
                delGeneratorBtn.enable = "on";
                editGeneratorBtn.enable = "on";
            end
        else
            delGeneratorBtn.enable = "off";
            editGeneratorBtn.enable = "off";
        end
    else
        generatorsListbox.string = "";
        generatorsListbox.enable = "off";
        delGeneratorBtn.enable = "off";
        editGeneratorrBtn.enable = "off";
    end
    
    // Responses configuration
    simulatornameTxt = findobj("Tag", "simulatorname_txt");
    selectsimulatorBtn = findobj("Tag", "selectsimulator_btn");
    configuresimulatorBtn = findobj("Tag", "configuresimulator_btn");
    if sopproject.datasource=="file" then
        simulatornameTxt.enable = "off";
        selectsimulatorBtn.enable = "off";
        configuresimulatorBtn.enable = "off";
    else
        selectsimulatorBtn.enable = "on";
        simulatornameTxt.enable = "on";
        if ~isempty(sopproject.simulator) then
            configuresimulatorBtn.enable = "on";
            simulatornameTxt.string = sopproject.simulator.funname; // TODo Get description here
        else
            configuresimulatorBtn.enable = "off";
            simulatornameTxt.string = _("No simulator selected.");
        end
    end
    
endfunction

