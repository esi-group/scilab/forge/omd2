// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function variableObj = sopVariable(variableName, variableMinBound, variableMaxBound, variableNominalValue, isFixed)

    // Create the tlist
    variableObj = tlist(["sopvar", "<name>", "<units>", "<indexinfile>", "<values>", "<valuestype>", "<models>", "<minbound>", "<maxbound>", "<nominalvalue>", "<isfixed>"])

    // Set default values
    variableObj.name = "";
    variableObj.units = "";
    variableObj.indexinfile = 0;
    variableObj.values = [];
    variableObj.valuestype = [];
    variableObj.models = list();
    variableObj("<minbound>") = %nan; // Insert directly because tests in generic_i_sopvar are based on a not yet initialized field
    variableObj("<maxbound>") = %nan; // Insert directly because tests in generic_i_sopvar are based on a not yet initialized field
    variableObj.nominalvalue = %nan;
    variableObj.isfixed = %F;
    
    rhs = argn(2);
    if rhs >=1 then
        variableObj.name = variableName;
    end
    if rhs >=2 then
        variableObj.minbound = variableMinBound;
    end
    if rhs >=3 then
        variableObj.maxbound = variableMaxBound;
    end
    if rhs >=4 then
        variableObj.nominalvalue = variableNominalValue;
    end
    if rhs >=5 then
        variableObj.isfixed = isFixed;
    end
endfunction

