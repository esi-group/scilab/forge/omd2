// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopDataGui()
    
    sopSetGuiEnable(findobj("tag", "sopgui"), "off");

    h = findobj("Tag", "sopdatagui");
    if ~isempty(h) then
        delete(h)
    end

    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");

    figh = 400 + btnh + 2*margin;
    framemaxh = figh - btnh - 3*margin;
    figw = 600 + 5*margin + 40;
    
    sopMainGui = findobj("tag", "sopgui");
    callerProject = sopGetCurrentProject(sopMainGui);
    h = sopFigure(_("Data GUI"), [100 100 figw figh], "sopdatagui", callerProject, %T, %T, "updateSopDataGui", "cbSopDataGui");
    h.closerequestfcn = "cbSopDataGui(""quit_menu"")"
    h.resizefcn = "resizeSopDataGui();";

    fileMenu = uimenu("Parent", h, ..
                      "Label","&File", ..
                      "Tag", "file_menu");
           
    uimenu("Parent", fileMenu, ..
           "Label","&Load data file...", ..
           "Tag", "loaddatafile_menu", ..
           "Callback", "cbSopDataGui");
    uimenu("Parent", fileMenu, ..
           "Label","&Quit", ..
           "Tag", "quit_menu", ..
           "Callback", "cbSopDataGui");

    factorsMenu = uimenu("Parent", h, ..
                         "Label","&Factors", ..
                         "Tag", "factors_menu");

    uimenu("Parent", factorsMenu, ..
           "Label","&Generate", ..
           "Tag", "generatefactors_menu", ..
           "Callback", "cbSopDataGui");
           
    uimenu("Parent", factorsMenu, ..
           "Label","&View", ..
           "Tag", "viewfactors_menu", ..
           "Callback", "cbSopDataGui");

    responsesMenu = uimenu("Parent", h, ..
                           "Label","&Responses", ..
                           "Tag", "responses_menu");

    uimenu("Parent", responsesMenu, ..
           "Label","&Simulate", ..
           "Tag", "simulateresponses_menu", ..
           "Callback", "cbSopDataGui");
           
    uimenu("Parent", responsesMenu, ..
           "Label","&View", ..
           "Tag", "viewresponses_menu", ..
           "Callback", "cbSopDataGui");

    // Window management buttons
    btnw = 90;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Close", ..
              "Position", [figw-margin-btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "close_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Save & Close", ..
              "Position", [figw-2*margin-2*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "saveclose_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Save", ..
              "Position", [figw-3*margin-3*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "save_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Reset", ..
              "Position", [figw-4*margin-4*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "reset_btn");
              
    frameybasis = 2*margin+btnh;
    // Variables frame
    framex = margin;
    framey = frameybasis;
    framew = 200;
    frameh = framemaxh;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Variables", 60, "variables");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+margin framew-2*margin frameh-2*margin], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "variables_listbox");

    // Buttons
    miniframeh = (figh-3*margin)/2;
    btnw = 40;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\Rightarrow$", ..
              "Position", [2*margin+framew margin+miniframeh/2+margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "variablestoresponses_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\Leftarrow$", ..
              "Position", [2*margin+framew margin+miniframeh/2-margin-btnh btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "responsestovariables_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\Rightarrow$", ..
              "Position", [2*margin+framew margin+miniframeh*3/2+2*margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "variablestofactors_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\Leftarrow$", ..
              "Position", [2*margin+framew margin+miniframeh*3/2-btnh btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "factorstovariables_btn");

    // Responses frame
    framex = 3*margin + framew + btnw;
    framey = frameybasis;
    framew = 200;
    frameh = (framemaxh-margin)/2;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Responses", 60, "responses");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+2*margin+btnh framew-2*margin frameh-3*margin-btnh], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Min", 0, ..
              "Max", 0, ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "responses_listbox");
    tmpbtnw = (framew-4*margin)/3;
    // Add "Add response" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\plus$", ..
              "Position", [framex+margin framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "addresponse_btn");
    // Add "Remove response" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\minus$", ..
              "Position", [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "delresponse_btn");
    // Add "Edit response" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Edit", ..
              "Position", [framex+3*margin+2*tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "editresponse_btn");

    // Factors frame
    framex = 3*margin + framew + btnw;
    framey = frameh + 3*margin + btnh;
    framew = 200;
    frameh = (framemaxh-margin)/2;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Factors", 50, "factors");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+2*margin+btnh framew-2*margin frameh-3*margin-btnh], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "factors_listbox");
    tmpbtnw = (framew-4*margin)/3;
    // Add "Add factor" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\plus$", ..
              "Position", [framex+margin framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "addfactor_btn");
    // Add "Remove factor" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\minus$", ..
              "Position", [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "delfactor_btn");
    // Add "Edit factor" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Edit", ..
              "Position", [framex+3*margin+2*tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "editfactor_btn");

    // Responses configuration frame
    framex = 4*margin + 2*framew + btnw;
    framey = frameybasis;
    framew = 200;
    frameh = (framemaxh-margin)/2;
    tmpbtnw = (framew-4*margin)/3
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Responses configuration", 130, "respconfig");
    // Add "Configure simulator" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", _("Configure"), ..
              "Position", [framex+margin framey+margin framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "configuresimulator_btn");
    // Add "Select simulator" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", _("Select"), ..
              "Position", [framex+margin framey+2*margin+btnh framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "selectsimulator_btn");
    // Add "Simulator Name" label
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [framex+margin framey+3*margin+2*btnh framew-2*margin btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "simulatorname_txt");

    // Factors configuration frame
    framex = 4*margin + 2*framew + btnw;
    framey = frameh + 3*margin + btnh;
    framew = 200;
    frameh = (framemaxh-margin)/2;
    tmpbtnw = (framew-4*margin)/3
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Factors configuration", 130, "factconfig");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+2*margin+btnh framew-2*margin frameh-3*margin-btnh], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "generators_listbox");
    tmpbtnw = (framew-4*margin)/3;
    // Add "Add generator" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\plus$", ..
              "Position", [framex+margin framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "addgenerator_btn");
    // Add "Remove generator" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "$\minus$", ..
              "Position", [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "delgenerator_btn");
    // Add "Edit generator" button
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Edit", ..
              "Position", [framex+3*margin+2*tmpbtnw framey+margin tmpbtnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "off", ..
              "Callback", "cbSopDataGui();", ..
              "Tag", "editgenerator_btn");

    resizeSopDataGui();
    updateSopDataGui();

endfunction
