// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopObj = sopLoadProject(projectFileName)

    sopproj = [];

    if fileinfo(projectFileName) then
        sopproject = [];
        errStatus = %F;
        errMsg = msprintf(gettext("File ''%'' could not be found."), projectFileName);
        return
    end

    clear sopproj // Clear this variable to be sure returned value has been read in the file
    load(projectFileName);
    
    if ~exists("sopproj") then
        errMsg = msprintf(gettext("File ''%'' does not contain project data."), projectFileName);
        return
    end
    
    sopObj = sopCreateObject(projectFileName);

endfunction
