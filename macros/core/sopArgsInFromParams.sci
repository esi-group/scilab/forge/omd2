// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function listArgIn = sopArgsInFromParams(sopProj, wrapperInParams, instanceInParams, varargin)

    listArgIn = list();
    // TODO Test if not the same number of params after a generator update
    for kParam = 1:size(wrapperInParams)
        select wrapperInParams(kParam).type
        case "FactorsValues" // Factors
            normalizedSelectedPoints = varargin(1);
            listArgIn($+1) = normalizedSelectedPoints;
        case "ResponsesValues" // Responses
            selectedResponseValues = varargin(2);
            listArgIn($+1) = selectedResponseValues;
        case "FactorsResponsesValues" // Factors & Responses
            normalizedSelectedPoints = varargin(1);
            selectedResponseValues = varargin(2);
            listArgIn($+1) = [normalizedSelectedPoints selectedResponseValues];
        case "ValidationFactorsResponsesValues" // Factors & Responses (validation points)
            validationPoints = varargin(3);
            validationResponseValues = varargin(4);
            listArgIn($+1) = [validationPoints validationResponseValues];
        case "Function"
            listArgIn($+1) = evstr(get_param(instanceInParams, wrapperInParams(kParam).name));
        case "Constant"
            listArgIn($+1) = get_param(instanceInParams, wrapperInParams(kParam).name);
        case "Boolean"
            listArgIn($+1) = get_param(instanceInParams, wrapperInParams(kParam).name);
        case "NumberOfPoints" // User-defined value
            listArgIn($+1) = get_param(instanceInParams, wrapperInParams(kParam).name);
        case "NumberOfDimensions" // Number of factors
            listArgIn($+1) = size(sopProj.factors); // TODO there could be constant factors
        case "InitialPoint" // Nominal values of factors
            if isempty(sopProj.optimalpoint) then // First optimizer called
                listArgIn($+1) = [];
                for kFact = 1:size(sopProj.factors)
                    listArgIn($) = [listArgIn($) sopProj.factors(kFact).nominalvalue];
                end
            else
                listArgIn($+1) = sopProj.optimalpoint; // Second optimizer called
            end
        case "LowerBounds" // Lower Bounds
            listArgIn($+1) = [];
            for kFact = 1:size(sopProj.factors)
                listArgIn($) = [listArgIn($) sopProj.factors(kFact).minbound];
            end
        case "UpperBounds" // Lower Bounds
            listArgIn($+1) = [];
            for kFact = 1:size(sopProj.factors)
                listArgIn($) = [listArgIn($) sopProj.factors(kFact).maxbound];
            end
        case "ObjectiveFunction" // Ojective function
            listArgIn($+1) = evstr("sopObjectiveFunction");
        case "NumberOfResponses"
            listArgIn($+1) = size(sopProj.responses)
        case "DesignOfExperiment" // Points to simulate
            doe = [];
            for kFact = 1:size(sopProj.factors)
                doe = [doe, sopProj.factors(kFact).values];
            end
            listArgIn($+1) = doe;
        case "FunctionName" // A function as parameter
            listArgIn($+1) = get_param(instanceInParams, wrapperInParams(kParam).name);
        case "String" // A String as parameter
            listArgIn($+1) = get_param(instanceInParams, wrapperInParams(kParam).name);
        else
            error("Unknown parameter type: " + string(wrapperInParams(kParam).type)); // TODO
        end
    end
    
endfunction