// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function wrappersFunctionName = sopLoadWrappers(wrapperType)

    wrappersFunctionName = [];

    wrappersFile = SCIHOME + filesep() + "sopwrappers.xml";
    
    if isfile(wrappersFile) then
        doc = xmlRead(wrappersFile);
    else
        error(_("Wrappers file not found."));
    end
    
    for kChild = 1:prod(size(doc.root.children))
        funcName = doc.root.children(kChild).attributes.functionname;
        if isdef(funcName) then
            [ierr, currentWrapperType] = sopCallWrapper(funcName, "type");
            if ierr<>0 then
                error(msprintf(_("%s: Could not get type for ''%s'' wrapper.\n"), "sopLoadWrappers", funcName));
            end
            if currentWrapperType==convstr(wrapperType, "l") then
                wrappersFunctionName($+1) = funcName;
            end
        else
            disp("Unknown function: " + funcName);
        end
     end
endfunction
