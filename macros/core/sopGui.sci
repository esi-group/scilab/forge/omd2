// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGui(projectFile)

    h = findobj("Tag", "sopgui");
    if ~isempty(h) then
        scf(h)
        return
    end

    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");

    txtWidth = 150;
    txtHeight = 40;
    btnWidth = 150;
    btnHeight = 100;
    figh = 2 * txtHeight + btnHeight;
    figw = 3 * btnWidth;
    h = sopFigure(_("Scilab Optimization Platform"), [100 100 figw figh], "sopgui", [], %T, %T, "updateSopGui");
    h.closerequestfcn = "cbSopGui(""quitmenu"")";
    h.resizefcn = "resizeSopGui();";
    
    fileMenu = uimenu("Parent", h, ..
                      "Label","&File", ..
                      "Tag", "sopguifilemenu");

    uimenu("Parent", fileMenu, ..
           "Label","&New project...", ..
           "Tag", "newprojectmenu", ..
           "Callback", "cbSopGui");

    uimenu("Parent", fileMenu, ..
           "Label","&Load project...", ..
           "Tag", "loadprojectmenu", ..
           "Callback", "cbSopGui");

    uimenu("Parent", fileMenu, ..
           "Label","&Save project...", ..
           "Tag", "saveprojectmenu", ..
           "Callback", "cbSopGui");

    uimenu("Parent", fileMenu, ..
           "Label","&Quit", ..
           "Tag", "quitmenu", ..
           "Callback", "cbSopGui");

    paMenu = uimenu("Parent", h, ..
                    "Label","&ProActive", ..
                    "Enable", "off", ..
                    "Tag", "sopguipamenu");

    uimenu("Parent", paMenu, ..
           "Label","&Configure...", ..
           "Tag", "sopguipaconfigmenu", ..
           "Callback", "cbSopGui");

    helpMenu = uimenu("Parent", h, ..
                      "Label","&?", ..
                      "Tag", "sopguihelpmenu");

    uimenu("Parent", helpMenu, ..
           "Label","&Help for SOP main GUI...", ..
           "Tag", "sopguihelpsopguimenu", ..
           "Callback", "cbSopGui");

    uimenu("Parent", helpMenu, ..
           "Label","&Help for Scilab Optimization Platform...", ..
           "Tag", "sopguihelpsopmenu", ..
           "Callback", "cbSopGui");

    uimenu("Parent", helpMenu, ..
           "Label","&About Scilab Optimization Platform...", ..
           "Tag", "sopguiaboutmenu", ..
           "Callback", "cbSopGui");

    // Data module
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Data", ..
              "Position", [0 2*txtHeight btnWidth btnHeight], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 20, ..
              "Enable", "off", ..
              "Callback", "cbSopGui();", ..
              "Tag", "databtn");
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [0 txtHeight txtWidth txtHeight], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "FontWeight", "bold", ..
              "HorizontalAlignment", "center", ..
              "Enable", "on", ..
              "Callback", "cbSopGui();", ..
              "Tag", "dataconfigstatustxt");
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [0 0 txtWidth txtHeight], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "FontWeight", "bold", ..
              "HorizontalAlignment", "center", ..
              "Enable", "on", ..
              "Callback", "cbSopGui();", ..
              "Tag", "dataexecstatustxt");

    // Modeling module
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Modeling", ..
              "Position", [btnWidth 2*txtHeight btnWidth btnHeight], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 20, ..
              "Enable", "off", ..
              "Callback", "cbSopGui();", ..
              "Tag", "modelingbtn");
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [txtWidth txtHeight txtWidth txtHeight], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "FontWeight", "bold", ..
              "HorizontalAlignment", "center", ..
              "Enable", "on", ..
              "Callback", "cbSopGui();", ..
              "Tag", "modelingconfigstatustxt");
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [txtWidth 0 txtWidth txtHeight], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "FontWeight", "bold", ..
              "HorizontalAlignment", "center", ..
              "Enable", "on", ..
              "Callback", "cbSopGui();", ..
              "Tag", "modelingexecstatustxt");

    // Optimization module
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "String", "Optimization", ..
              "Position", [2*btnWidth 2*txtHeight btnWidth btnHeight], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 20, ..
              "Enable", "off", ..
              "Callback", "cbSopGui();", ..
              "Tag", "optimbtn");
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [2*txtWidth txtHeight txtWidth txtHeight], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "FontWeight", "bold", ..
              "HorizontalAlignment", "center", ..
              "Enable", "on", ..
              "Callback", "cbSopGui();", ..
              "Tag", "optimconfigstatustxt");
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [2*txtWidth 0 txtWidth txtHeight], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "FontWeight", "bold", ..
              "HorizontalAlignment", "center", ..
              "Enable", "on", ..
              "Callback", "cbSopGui();", ..
              "Tag", "optimexecstatustxt");

    resizeSopGui();
    updateSopGui();

    rhs = argn(2);
    if rhs==1 then
        if typeof(projectFile)=="string" then
            sopProj = sopLoadDataFile([], projectFile);
            sopSetCurrentProject(h, sopProj);
        elseif sopObjectType(projectFile)=="Project" then
            sopSetCurrentProject(h, projectFile);
        end
    end
endfunction
