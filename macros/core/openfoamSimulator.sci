// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [responsesValues] = openfoamSimulator(pointsToSimulate, pathToBashrc, openFOAMTempatePath, controlDictFileFun, numberOfIterations, blockMeshDictFileFun, density, postprocessingFun, numberOfResponses)

    openFOAMOutputPath = TMPDIR + filesep();
    resultDirectory = TMPDIR + filesep();
    nObs = size(pointsToSimulate, 1);
    
    responsesValues = zeros(nObs, numberOfResponses);

    for kPoint=1:nObs
        sopLog(msprintf("Simulate point %04d/%04d...\n", kPoint, nObs))
        // PRE-PROCESSING
        dirToCreate = openFOAMOutputPath + sprintf("%04d", kPoint);
        if isdir(dirToCreate) then
            rmdir(dirToCreate);
        end
        status = mkdir(dirToCreate);
        if status<>1 then
            sopError(msprintf("Could not create directory ''%s''.\n", dirToCreate))
        end
        [status, msg] = copyfile(openFOAMTempatePath, dirToCreate);
        if status<>1 then
            sopError(msprintf("Could not copy template from %s to %s\n", openFOAMTempatePath, dirToCreate))
        end

        controlDictFile = dirToCreate + filesep() + "system" + filesep() + "controlDict";
        ierr = execstr(controlDictFileFun + "(controlDictFile, numberOfIterations)", "errcatch");
        if ierr<>0 then
            sopError(msprintf(gettext("Could not create ''%s'' file.\n"), controlDictFile))
        end

        blockMeshDictFile = dirToCreate + filesep() + "constant" + filesep() + "polyMesh" + filesep() + "blockMeshDict";
        ierr = execstr(blockMeshDictFileFun + "(pointsToSimulate(kPoint, :), density, blockMeshDictFile)", "errcatch");
        if ierr<>0 then
            sopError(msprintf(gettext("Could not create ''%s'' file.\n"), blockMeshDictFile))
        end

        // SIMULATION
        openFOAMCommand = "-case " + openFOAMOutputPath + sprintf("%04d", kPoint);

        blockMeshCmd = "source " + pathToBashrc + "; " + fullfile("$FOAM_APPBIN","blockMesh") + " " + openFOAMCommand + " > /dev/null";
        stat = host(blockMeshCmd)
        if stat<>0 then
            sopWarning(msprintf(gettext("Simulation failed for point #%d (blockMesh).\n"), kPoint));
            responsesValues(kPoint, :) = %nan;
            continue
        end

        simpleFoamCmd = "source " + pathToBashrc + "; " + fullfile("$FOAM_APPBIN","simpleFoam") + " " +openFOAMCommand + " > /dev/null";
        stat = host(simpleFoamCmd);
        if stat<>0 then
            sopWarning(msprintf(gettext("Simulation failed for point #%d (simpleFoam).\n"), kPoint));
            responsesValues(kPoint, :) = %nan;
            continue
        end

        // POST-PROCESSING
        resultPath = resultDirectory + sprintf("%04d", kPoint);
        ierr = execstr("responsesValues(kPoint, :) = " + postprocessingFun + "(resultPath, numberOfIterations, density)", "errcatch");
        if ierr<>0 then
            sopError(msprintf(gettext("Post processing function (''%s'') failed for point ''%d''.\n"), postprocessingFun, kPoint))
        end
    end
endfunction
