// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSetGuiEnable(figHandle, state)
    // TODO inputs type and size check
    hdls = sopGuiHandles(figHandle);
    fieldNames = fieldnames(hdls);
    for kHdl = 1:size(fieldNames, "*")
        if hdls(fieldNames(kHdl)).type=="uicontrol" | hdls(fieldNames(kHdl)).type=="uimenu" then
            set(hdls(fieldNames(kHdl)), "enable", state)
        end
    end
    if state=="on" then
        execstr(figHandle.userdata.sopupdatefunction);
    end
endfunction