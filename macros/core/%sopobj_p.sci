// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function %sopobj_p(sopObj)

    // Load corresponding data file
    load(sopObj.workfile);

    // Display the object type
    objectType = msprintf("SOP object of type ""%s"" with properties:", sopObjectType(sopObj))
    mprintf("%s\n", objectType);
    mprintf("%s\n", ascii(61*ones(1, length(objectType))));
    
    // Display the object data
    if part(sopObjectType(sopObj), 1:4)=="List" then
        for kElt=1:size(evstr("sopproj" + sopObj.editeddata))
            mprintf(" \n");
            %sopobj_p(sopObj(kElt))
        end
    else
        mprintf("%s\n", string(evstr("sopproj" + sopObj.editeddata)))
    end

    // Show debug information
    mprintf("\n");

    mprintf("Original file: %s\n", strsubst(sopObj.origfile, TMPDIR, "TMPDIR"));
    mprintf("Pointed file: %s\n", strsubst(sopObj.pointedfile, TMPDIR, "TMPDIR"));
    mprintf("Work file: %s\n", strsubst(sopObj.workfile, TMPDIR, "TMPDIR"));

    mprintf("Edited data: sopproj%s\n", sopObj.editeddata);

    [Y, M, D, H, MI, S] = datevec(sopObj.creationdate);
    mprintf("Creation date: %4d/%2d/%2d-%2d:%2d:%2d\n", Y, M, D, H, MI, S);
    [Y, M, D, H, MI, S] = datevec(sopObj.modificationdate);
    mprintf("Modification date: %4d/%2d/%2d-%2d:%2d:%2d\n", Y, M, D, H, MI, S);

endfunction
