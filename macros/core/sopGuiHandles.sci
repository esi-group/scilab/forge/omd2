// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function stHandles = sopGuiHandles(handle, stHandles)
    rhs = argn(2);
    if rhs < 2 then
        stHandles = struct();
    end
    children = handle.children;
    for kChild = 1:size(children, "*")
        ierr = execstr("tag = children(kChild).tag;", "errcatch");
        if ierr==0 & tag <> "" then
            // Test tag validity
            ierr = execstr(tag + " = 1;", "errcatch");
            if ierr==0 then
                // Test tag does not already exist
                if isfield(stHandles, tag) then
                    error("Duplicate tag:" + tag)
                end
                stHandles(tag) = children(kChild);
            end
        end
        stHandles = sopGuiHandles(children(kChild), stHandles)
    end
endfunction
