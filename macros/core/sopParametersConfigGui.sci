// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopParametersConfigGui(parentFigHandle, callingGcboTag, wrapperConfigParams, editedObject, paramsField)
    
    if isempty(wrapperConfigParams) then
        messagebox(_("No configuration available."), _("Parameters configuration"), "info");
        return
    end
    
    rhs = argn(2);
    if rhs < 5 then
        paramsField = "params";
    end
    
    // Deactivate calling GUI
    sopSetGuiEnable(parentFigHandle, "off");
    
    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");

    // Add information on calling GUI
    parentFigHandle.info_message = "Waiting for configuration GUI...";

    // Check parameters
    inputParamsList = list();
    for kParam = 1:size(wrapperConfigParams)
        select wrapperConfigParams(kParam).type
        case "Filename"
            inputParamsList($+1) = wrapperConfigParams(kParam);
        case "Function"
            inputParamsList($+1) = wrapperConfigParams(kParam);
        case "FunctionName"
            inputParamsList($+1) = wrapperConfigParams(kParam);
        case "Constant"
            inputParamsList($+1) = wrapperConfigParams(kParam);
        case "String"
            inputParamsList($+1) = wrapperConfigParams(kParam);
        case "Boolean"
            inputParamsList($+1) = wrapperConfigParams(kParam);
        else
            error("Unknown parameter type: " + string(wrapperConfigParams(kParam).type)); // TODO
        end
    end

    nbInputParams = size(inputParamsList);

    defaultEntryWidth = 200;
    figw = 100 + defaultEntryWidth + 5*margin;
    figh = 4*margin + nbInputParams*(btnh+margin) + btnh;
    figx = parentFigHandle.position(1) + parentFigHandle.position(3)/2 - figw/2;
    figy = parentFigHandle.position(2) + parentFigHandle.position(4)/2 - figh/2;
    h = sopFigure("Parameters configuration", [figx figy figw figh], "sopparametersconfiggui", [], %T, %T)
    ud = h.userdata;
    ud.sopparametersconfig = list();
    ud.sopparametersconfig(1) = wrapperConfigParams;
    ud.sopparametersconfig(2) = editedObject;
    ud.sopparametersconfig(3) = parentFigHandle;
    ud.sopparametersconfig(4) = paramsField;
    h.userdata = ud;

    // Get the parameters values
    paramsValues = editedObject(paramsField);

    // Add validation button
    btnw = 60;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "Position", [figw-margin-btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Ok"), ..
              "Callback", "cbSopParametersConfigGui();", ..
              "Tag", "ok_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "Position", [figw-2*margin-2*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Cancel"), ..
              "Callback", "cbSopParametersConfigGui();", ..
              "Tag", "cancel_btn");
              
    // Configuration frame
    framex = margin;
    framey = 2*margin + btnh;
    framew = 100 + defaultEntryWidth + 3*margin;
    frameh = nbInputParams*(btnh+margin) + margin;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Parameters values", 100, "parametersvalues");
    
    // For each parameter, add an entry
    for kParam = nbInputParams:-1:1
        uicontrol("Parent", h, ..
                  "Style", "text", ..
                  "Position", [framex+margin framey+margin+(kParam-1)*(btnh+margin) 100 btnh], ..
                  "BackgroundColor", figbg, ..
                  "FontWeight", "bold", ..
                  "String", inputParamsList(kParam).name, ..
                  "FontName", "Arial", ..
                  "FontSize", 12, ..
                  "Enable", "on", ..
                  "Tag", inputParamsList(kParam).name + "_label");
        // Choose between different parameters types
        select inputParamsList(kParam).type
        case "Function"
            // Popup with possible values
            possibleFunctions = [];
            for kPoss = 1: size(inputParamsList(kParam).valuesconstraints)
                possibleFunctions($+1) = inputParamsList(kParam).valuesconstraints(kPoss);
            end
            uicontrol("Parent", h, ..
                      "Style", "popupmenu", ..
                      "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                      "BackgroundColor", figbg, ..
                      "String", possibleFunctions, ..
                      "Value", find(possibleFunctions==get_param(paramsValues, inputParamsList(kParam).name)), ..
                      "FontName", "Arial", ..
                      "FontSize", 12, ..
                      "Enable", "on", ..
                      "Callback", "cbSopParametersConfigGui();", ..
                      "Tag", inputParamsList(kParam).name);
        case "Filename"
            // Button
            uicontrol("Parent", h, ..
                      "Style", "pushbutton", ..
                      "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                      "BackgroundColor", [1 1 1], ..
                      "String", fileparts(get_param(paramsValues, inputParamsList(kParam).name), "fname"), ..
                      "Userdata", get_param(paramsValues, inputParamsList(kParam).name), ..
                      "FontName", "Arial", ..
                      "FontSize", 12, ..
                      "Enable", "on", ..
                      "Callback", "cbSopParametersConfigGui();", ..
                      "Tag", inputParamsList(kParam).name);
        case "FunctionName"
            // Edit
            uicontrol("Parent", h, ..
                      "Style", "edit", ..
                      "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                      "BackgroundColor", [1 1 1], ..
                      "String", get_param(paramsValues, inputParamsList(kParam).name), ..
                      "FontName", "Arial", ..
                      "FontSize", 12, ..
                      "Enable", "on", ..
                      "Callback", "cbSopParametersConfigGui();", ..
                      "Tag", inputParamsList(kParam).name);
        case "Constant"
            // Edit
            uicontrol("Parent", h, ..
                      "Style", "edit", ..
                      "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                      "BackgroundColor", [1 1 1], ..
                      "String", string(get_param(paramsValues, inputParamsList(kParam).name)), ..
                      "FontName", "Arial", ..
                      "FontSize", 12, ..
                      "Enable", "on", ..
                      "Callback", "cbSopParametersConfigGui();", ..
                      "Tag", inputParamsList(kParam).name);
        case "String"
            // Edit
            if typeof(inputParamsList(kParam).valuesconstraints)=="string" & size(inputParamsList(kParam).valuesconstraints, "*") == 1 then // Only one possible value
                enabled = "off";
            else
                enabled = "on";
            end
            if typeof(inputParamsList(kParam).valuesconstraints)=="string" then
                uicontrol("Parent", h, ..
                          "Style", "edit", ..
                          "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                          "BackgroundColor", [1 1 1], ..
                          "String", get_param(paramsValues, inputParamsList(kParam).name), ..
                          "FontName", "Arial", ..
                          "FontSize", 12, ..
                          "Enable", enabled, ..
                          "Callback", "cbSopParametersConfigGui();", ..
                          "Tag", inputParamsList(kParam).name);
            else
                // Popup with possible values
                possibleValues = [];
                for kPoss = 1: size(inputParamsList(kParam).valuesconstraints)
                    possibleValues($+1) = inputParamsList(kParam).valuesconstraints(kPoss);
                end
                uicontrol("Parent", h, ..
                          "Style", "popupmenu", ..
                          "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                          "BackgroundColor", [1 1 1], ..
                          "String", possibleValues, ..
                          "Value", find(possibleValues==get_param(paramsValues, inputParamsList(kParam).name)), ..
                          "FontName", "Arial", ..
                          "FontSize", 12, ..
                          "Enable", enabled, ..
                          "Callback", "cbSopParametersConfigGui();", ..
                          "Tag", inputParamsList(kParam).name);
            end
        case "Boolean"
            // Popup containing %T/%F
            if size(inputParamsList(kParam).valuesconstraints, "*") == 1 then // Only one possible value
                enabled = "off";
            else
                enabled = "on";
            end
            uicontrol("Parent", h, ..
                      "Style", "popupmenu", ..
                      "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) defaultEntryWidth btnh], ..
                      "BackgroundColor", figbg, ..
                      "String", ["%F", "%T"], ..
                      "Value", bool2s(get_param(paramsValues, inputParamsList(kParam).name)) + 1, ..
                      "FontName", "Arial", ..
                      "FontSize", 12, ..
                      "Enable", enabled, ..
                      "Callback", "cbSopParametersConfigGui();", ..
                      "Tag", inputParamsList(kParam).name);
        else
            error("Unknown parameter type: " + string(inputParamsList(kParam).type)); // TODO
        end
        
    end

endfunction
