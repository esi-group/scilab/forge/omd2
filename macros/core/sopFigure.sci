// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function fig = sopFigure(figTitle, figPosition, figTag, callerProject, removeMenus, haveUicontrols, updateFunctionName, cbFunctionName)

    rhs = argn(2);

    if rhs<1 then
        figTitle = "";
    end
    if rhs<2 then
        figPosition = [];
    end
    if rhs<3 then
        figTag = "";
    end
    if rhs<4 then
        callerProject = [];
    end
    if rhs<5 then
        removeMenus = %F;
    end
    if rhs<6 then
        haveUicontrols = %F;
    end

    figbg = sopGuiParameters("FigureBackground");

    fig = scf();
    set(fig, "background", addcolor(figbg));

    if ~isempty(figTitle) then
        set(fig, "figure_name", figTitle);
    end
    if ~isempty(figTag) then
        set(fig, "tag", figTag);
    end
    if ~isempty(callerProject) then
        currentProject = sopCreateObject(callerProject)
        sopSetCurrentProject(fig, currentProject);
    end

    if removeMenus then
        // Remove old menus
        toolbar(get(fig, "figure_id"), "off");
        delmenu(get(fig, "figure_id"), gettext("File"));
        delmenu(get(fig, "figure_id"), gettext("Tools"));
        delmenu(get(fig, "figure_id"), gettext("Edit"));
        delmenu(get(fig, "figure_id"), gettext("?"));
    end

    // Set position only once toolbar has been removed
    if ~isempty(figPosition) then
        if getos()<>"Darwin" & removeMenus then
            figPosition(4) =  figPosition(4)+25; // TODO Scilab bug?
        end
        set(fig, "Position", figPosition);
    end

    if haveUicontrols then // Ugly hack to be sure the figure has been resized
        a = gca();
        set(a, "background", addcolor(figbg));
        xpause(3000000);
    end

    if rhs>=7 then
        ud = fig.userdata;

        if typeof(ud)<>"st" then
            ud = struct();
        end

        ud.sopupdatefunction = updateFunctionName;

        fig.userdata = ud;
    end

    if rhs==8 then
        ud = fig.userdata;

        ud.sopcallbackfunction = cbFunctionName;

        fig.userdata = ud;
    end

    // Event handler to manage figure closure using cross
    //fig.event_handler = "sopFigureEventHandler";
    //fig.event_handler_enable = "on";

endfunction
