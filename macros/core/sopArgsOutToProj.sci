// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopProj = sopArgsOutToProj(sopProj, wrapperOutParams, varargin)

    for kParam = 1:size(wrapperOutParams)
        select wrapperOutParams(kParam).type
        case "ModelCoefficients"
            kResp = varargin($-1);
            kModel = varargin($);
            currentModel = sopProj.responses(kResp).models(kModel);
            // Call generic insertion function directly because the model coeffs can be of an "exotic" type
            // Ex: coeffs are a "dace_model" tlist for DACE
            currentModel = generic_i_sopmdl("coeffs", evstr("argOut" + string(kParam)), currentModel);
            sopProj.responses(kResp).models(kModel) = currentModel;
        case "GeneratedDoe"
            // TODO add values to factors
            generatedDoe = varargin(kParam);
            for kFact = 1:size(sopProj.factors)
                ub = sopProj.factors(kFact).maxbound;
                lb = sopProj.factors(kFact).minbound;
                sopProj.factors(kFact).values = lb + (ub-lb) * generatedDoe(:, kFact);
                sopProj.factors(kFact).valuestype = ones(size(generatedDoe, 1), 1);
            end
        case "OptimalPoint"
            sopProj.optimalpoint = varargin(kParam);
        case "ObjectiveFunctionOptimalValue"
            sopProj.objfunctionoptimalvalue = varargin(kParam);
        case "SimulatedResponses"
            simulatedResponses = varargin(kParam);
            for kResp = 1:size(sopProj.responses)
                sopProj.responses(kResp).values = [sopProj.responses(kResp).values ; simulatedResponses(:, kResp)];
                valuesType = ones(size(simulatedResponses(:, kResp), 1), 1); // Learning point
                failed = isnan(sopProj.responses(kResp).values);
                valuesType(failed) = 0; // Bad point
                sopProj.responses(kResp).valuestype = [sopProj.responses(kResp).valuestype ; valuesType];
            end
        case "FakeParameter"
            // Nothing to do?
        else
            error("Unknown parameter type: " + string(wrapperOutParams(kParam).type)); // TODO
        end
    end

endfunction
