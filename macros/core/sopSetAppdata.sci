// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSetAppdata(figHandle, dataName, data)

    // Check that figHandle is a valid figure
    ud = get(figHandle, "userdata");
    if ~isfield(ud, "sopappdata") then
        error("This figure does seems to be a Scilab Optimization Platform figure.");
    end

    sopappdata = ud.sopappdata;

    rhs = argn(2);
    // Whole appdata initialization
    if rhs==2 then
    elseif rhs<>3 then
        error("Wrong number of input argument.");
    end

    // A single field setting
    select dataName
    case "pointedfile"
        sopappdata.pointedfile = "";
    else
        error("Wrong sopdata field: " + dataName);
    end

    figHandle.userdata.sopappdata = sopappdata;

endfunction
