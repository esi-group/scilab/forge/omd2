// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopPageManager(figHandle, figAxes)

    // Add page management informations in figure userdata
    ud = figHandle.userdata;
    if typeof(ud) <> "st" then
        ud = struct();
    end
    
    if ~isfield(ud, "soppagemanager") then // First use of sopPageManager for this figure ==> INITIALIZATION
        ud.soppagemanager = struct();
        ud.soppagemanager.nbrows = min(2, size(figAxes, "*"));
        ud.soppagemanager.nbcols = min(2, size(figAxes, "*"));
        ud.soppagemanager.axes = figAxes;
        ud.soppagemanager.currentpage = 1;

        // Add page management buttons
        [btnbg] = sopGuiParameters("ButtonBackground");

        txtWidth = 150;
        txtHeight = 40;
        btnWidth = 20;
        btnHeight = btnWidth;
        figHeight = figHandle.axes_size(2);

        cb = "f = get(gcbo, ""parent""); f.userdata.soppagemanager.currentpage = f.userdata.soppagemanager.currentpage - 1; sopPageManager(f);";
        ud.soppagemanager.prevpagebtn = uicontrol("Parent", figHandle, ..
                                                  "Style", "pushbutton", ..
                                                  "String", "<", ..
                                                  "Position", [0 figHeight-btnHeight btnWidth btnHeight], ..
                                                  "BackgroundColor", btnbg, ..
                                                  "FontName", "Arial", ..
                                                  "FontWeight", "bold", ..
                                                  "FontSize", 12, ..
                                                  "Enable", "on", ..
                                                  "Callback", cb,..
                                                  "Tag", "prevpagebtn");
        
        cb = "f = get(gcbo, ""parent"");";
        cb = cb + "entryLabels = [""Number of rows"";""Number of columns""];";
        cb = cb + "pageConfig = x_mdialog(""Page configuration"", entryLabels, [string(f.userdata.soppagemanager.nbrows);string(f.userdata.soppagemanager.nbcols)]);";
        cb = cb + "if ~isempty(pageConfig) then";
        cb = cb + "    f.userdata.soppagemanager.currentpage = 1;"
        cb = cb + "    f.userdata.soppagemanager.nbrows = max(evstr(pageConfig(1)), 1);"
        cb = cb + "    f.userdata.soppagemanager.nbcols = max(evstr(pageConfig(2)), 1);"
        cb = cb + "end;";
        cb = cb + "sopPageManager(f);;"
        ud.soppagemanager.pageindexbtn = uicontrol("Parent", figHandle, ..
                                                  "Style", "pushbutton", ..
                                                  "String", "1/6", ..
                                                  "Position", [btnWidth figHeight-btnHeight 2*btnWidth btnHeight], ..
                                                  "BackgroundColor", btnbg, ..
                                                  "FontName", "Arial", ..
                                                  "FontWeight", "bold", ..
                                                  "FontSize", 12, ..
                                                  "Enable", "on", ..
                                                  "Callback", cb, ..
                                                  "Tag", "pagesetupbtn");

        cb = "f = get(gcbo, ""parent""); f.userdata.soppagemanager.currentpage = f.userdata.soppagemanager.currentpage + 1; sopPageManager(f);";
        ud.soppagemanager.nextpagebtn = uicontrol("Parent", figHandle, ..
                                                  "Style", "pushbutton", ..
                                                  "String", ">", ..
                                                  "Position", [3*btnWidth figHeight-btnHeight btnWidth btnHeight], ..
                                                  "BackgroundColor", btnbg, ..
                                                  "FontName", "Arial", ..
                                                  "FontWeight", "bold", ..
                                                  "FontSize", 12, ..
                                                  "Enable", "on", ..
                                                  "Callback", cb, ..
                                                  "Tag", "nextpagebtn");

        figHandle.userdata = ud;
    end // End of init

    currentpage = ud.soppagemanager.currentpage;
    nbAxesPerPage = ud.soppagemanager.nbrows * ud.soppagemanager.nbcols;

    // Update buttons status + string
    if currentpage <> 1 then
        ud.soppagemanager.prevpagebtn.enable = "on";
    else
        ud.soppagemanager.prevpagebtn.enable = "off";
    end
    nbPages = ceil(size(ud.soppagemanager.axes, "*") / nbAxesPerPage);
    if currentpage <> nbPages then
        ud.soppagemanager.nextpagebtn.enable = "on";
    else
        ud.soppagemanager.nextpagebtn.enable = "off";
    end
    if size(ud.soppagemanager.axes) == 1 then
        ud.soppagemanager.pageindexbtn.enable = "off";
    else
        ud.soppagemanager.pageindexbtn.enable = "on";
    end
    ud.soppagemanager.pageindexbtn.string = string(currentpage) + "/" + string(nbPages);

    // Axes to be displayed
    drawlater
    firstAxes = (currentpage-1) * nbAxesPerPage + 1;
    for kAxes = 1:size(ud.soppagemanager.axes, "*")
        ud.soppagemanager.axes(kAxes).visible = "off";
    end
    for kAxes = 1:size(ud.soppagemanager.axes, "*")
        if kAxes>=firstAxes & kAxes<firstAxes+nbAxesPerPage then
            sopSubplot(ud.soppagemanager.axes(kAxes), ud.soppagemanager.nbrows, ud.soppagemanager.nbcols, kAxes - firstAxes + 1);
            ud.soppagemanager.axes(kAxes).visible = "on";
        end
    end
    drawnow
endfunction
