// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function status = sopAddWrapper(wrapperFunctionName)

    status = %T;

    // Check input arguments number
    rhs = argn(2);
    if rhs<>1  then
        error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"), "sopAddWrapper", 1));
    end

    if typeof(wrapperFunctionName)<>"string" then
        error(msprintf(_("%s: Wrong type for input argument #%d: A string expected.\n"), "sopAddWrapper", 1));
    end
    if size(wrapperFunctionName, "*")<>1 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A string expected.\n"), "sopAddWrapper", 1));
    end

    // If the function is already loaded then test it
    if isdef(wrapperFunctionName) then
        // Try to get the wrapper type
        [ierr, wrapperType] = sopCallWrapper(wrapperFunctionName, "type");
        if ierr<>0 then
            status = %F;
            error(msprintf(_("%s: Could not get type for ''%s'' wrapper.\n"), "sopAddWrapper", wrapperFunctionName));
        end
    end

    wrappersFile = SCIHOME + filesep() + "sopwrappers.xml";
    
    if isfile(wrappersFile) then
        doc = xmlRead(wrappersFile);
    else
        // Create the file
        doc = xmlDocument(wrappersFile);
        doc.root = xmlElement(doc, "sopwrapperslist");
    end
    
    // Does the wrapper already exist
    for kChild = 1:prod(size(doc.root.children))
        if doc.root.children(kChild).attributes.functionname == wrapperFunctionName then
            status = %F;
            return
        end
    end
    
    // Add the wrapper
    wElement = xmlElement(doc, "sopwrapper")
    wElement = xmlSetAttributes(wElement, ["functionname"  wrapperFunctionName])
    doc.root.children(prod(size(doc.root.children)) + 1) = wElement;

    xmlWrite(doc);
    
endfunction

