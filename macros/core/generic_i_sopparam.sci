// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function parameterObj = generic_i_sopparam(propertyName, propertyValue, parameterObj)

    if typeof(parameterObj)<>"sopparam" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopparam", 3, "sopparam"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopparam", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopparam", 1));
    end

    select convstr(propertyName, "l")
    case "defaultvalue"
        // TODO check value according to type and to contraints
        parameterObj._defaultvalue = propertyValue;
    case "name"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%'' property: A string expected.\n", "Name"));
        end
        parameterObj._name = propertyValue;
    case "type"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%'' property: A string expected.\n", "Type"));
        end
        // TODO check string contents, must be a valid type
        parameterObj._type = propertyValue;
    case "uservalue"
        // TODO check value according to type and to contraints
        parameterObj._uservalue = propertyValue;
    case "valuescontraints"
        // TODO check value according to type
        // - can be a list of possible values
        // - can be a 1-by-2 vector of bounds
        parameterObj._valuescontraints = propertyValue;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end
endfunction

