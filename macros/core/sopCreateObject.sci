// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopObj = sopCreateObject(sopObjOrFilename, varargin)

    rhs = argn(2);

    sopObj = mlist(["sopobj", "<origfile>", "<pointedfile>", "<workfile>", "<editeddata>", "<creationdate>", "<modificationdate>"], "", "", "", "", [], []);
    // origfile = the project file (loaded/saved in sopGui)
    // pointedfile = the file this pointer is currently editing (OLD = filename)
    // workfile = the file used to store unsaved modification (OLD = identifier)
    // editeddata = "path" to data pointed by this pointer in the work file (OLD = path)
    // creationdate = date of creation of this pointer
    // modificationdate = date of last modifcation of data in the file (used to update the GUI with a "modified status" and to warn the user about usaved modifications)

    if rhs==0 then // Init object with a new empty project
        tempFilename = tempname("sop");
        mdelete(tempFilename);
        tempFilename = tempFilename + ".sop";

        sopObj.origfile = "";
        sopObj.pointedfile = "";
        sopObj.workfile = tempFilename;
        sopObj.editeddata = "";
        sopObj.creationdate = datenum();
        sopObj.modificationdate = sopObj.creationdate;

        sopproj = sopCreateEmptyProject();
        save(tempFilename, "sopproj")
    elseif rhs == 1 then
        if typeof(sopObjOrFilename)=="string" then  // Pointer to existing project file
            origFileName = sopObjOrFilename;
            projectFileName = sopObjOrFilename;
            workFile = %F;
        else // sopObj of type 'project'
            origFileName = sopObjOrFilename.origfile;
            projectFileName = sopObjOrFilename.workfile;
            workFile = %T;
        end

        tempFilename = tempname("sop");
        mdelete(tempFilename);
        tempFilename = tempFilename + ".sop";

        sopObj.origfile = fullpath(origFileName);
        sopObj.pointedfile = projectFileName;
        sopObj.workfile = tempFilename;
        sopObj.editeddata = "";
        sopObj.creationdate = datenum();
        sopObj.modificationdate = sopObj.creationdate;

        copyfile(projectFileName, tempFilename);

        // Update paths in models to be absolute
        // Filenames are stored as absoluted during the session to avoid problems with current path when simulating
        if ~workFile then
            for kResp = 1:size(sopObj.responses)
                for kModel = 1:size(sopObj.responses(kResp).models)
                    params = sopObj.responses(kResp).models(kModel).params;
                    [ierr, wrapperParams] = sopCallWrapper(sopObj.responses(kResp).models(kModel).funname, "configure");
                    for kParam=1:size(wrapperParams)
                        if wrapperParams(kParam).type=="Filename" then
                            fileParam = get_param(params, wrapperParams(kParam).name);
                            if ~isfile(fileParam) then
                                absoluteFileParam = fileparts(sopObj.origfile, "path") + fileParam
                                params = set_param(params, wrapperParams(kParam).name, absoluteFileParam);
                                sopObj.responses(kResp).models(kModel).params = params;
                            end
                        end
                    end
                end
            end
        end
    else // Update path to edited data
        generatedPath = "";
        for kPath = 1:size(varargin)
            if typeof(varargin(kPath)) == "string" & ~isempty(varargin(kPath)) then
                if part(varargin(kPath), 1)<>"." then
                    generatedPath = generatedPath + "." + varargin(kPath);
                else
                    generatedPath = generatedPath + varargin(kPath);
                end
            elseif typeof(varargin(kPath)) == "constant" then
                generatedPath = generatedPath + "(" + sci2exp(varargin(kPath)) + ")";
            elseif typeof(varargin(kPath)) == "polynomial" then
                generatedPath = generatedPath + "(" + sci2exp(varargin(kPath)) + ")";
            end
        end
        if execstr("sopObjOrFilename;sopObjOrFilename.editeddata = generatedPath;", "errcatch")<>0 then
            pause
        end
        sopObj = sopObjOrFilename;
    end

endfunction

function projectObj = sopCreateEmptyProject(projectDescription)

    // Create the tlist
    projectObj = tlist(["sopproj", "<version>", "<description>", "<filedata>", "<variables>", "<factors>", "<responses>", "<doenumerotation>", "<doecenterpointindex>", ..
                        "<datasource>", "<doegenerators>", ..
                        "<optimizers>", "<responsescoeffs>", ..
                        "<optimalpoint>", "<objfunctionoptimalvalue>", ..
                        "<dataconfigstatus>", "<modelconfigstatus>", "<optimconfigstatus>", ..
                        "<dataexecstatus>", "<modelexecstatus>", "<optimexecstatus>", ..
                        "<simulator>", ..
                        "<paparams>"])

    // Set default values
    projectObj.version = sopVersion();
    projectObj.description = "";
    projectObj.filedata = list(); // A filedata is a 'variable' tlist (save data)
    projectObj.variables = list(); // A variable is a 'variable' tlist
    projectObj.factors = list(); // A parameter is a 'variable' tlist
    projectObj.responses = list(); // A response is a 'variable' tlist
    projectObj.doenumerotation = []; // A 'variable' tlist
    projectObj.doecenterpointindex = 0;
    projectObj.datasource = "file"; // Can be "file" or "doe"
    projectObj.doegenerators = list();
    projectObj.optimizers = list();
    projectObj.responsescoeffs = [];
    projectObj.optimalpoint = [];
    projectObj.objfunctionoptimalvalue = [];
    projectObj.dataconfigstatus = sopStatus("Done"); // Data to be defined // TODO
    projectObj.modelconfigstatus = sopStatus("Done"); // Modelization to be defined // TODO
    projectObj.optimconfigstatus = sopStatus("Done"); // Optimization to be defined // TODO
    projectObj.dataexecstatus = sopStatus("ToBeDone"); // Data to be defined
    projectObj.modelexecstatus = sopStatus("ToBeDone"); // Modelization to be defined
    projectObj.optimexecstatus = sopStatus("ToBeDone"); // Optimization to be defined
    projectObj("<simulator>") = []; // To avoid the test, require a 'sopsimul' tlist
    [ierr, paparams] = sopCallWrapper("sopw_proactive", "defaults");
    if ierr<>0 then
        error()
    end
    projectObj.paparams = paparams;

    rhs = argn(2);
    if rhs >=1 then
        projectObj.description = projectDescription;
    end
endfunction
