// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopLog(strInfos)
    dt = getdate();
    dateString = sprintf("[%04d/%02d/%02d-%02d:%02d:%02d]", dt(1), dt(2), dt(6), dt(7), dt(8), dt(9))
    strInfos = strsubst(strInfos, TMPDIR, "TMPDIR");
    mprintf("%s %s\n", dateString, strInfos);
endfunction