// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function propertyValue = generic_e_sopparam(propertyName, parameterObj)

    if typeof(parameterObj)<>"sopparam" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_e_sopparam", 2, "sopparam"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_e_sopparam", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_e_sopparam", 1));
    end

    select convstr(propertyName, "l")
    case "defaultvalue"
        propertyValue = parameterObj._defaultvalue;
    case "name"
        propertyValue = parameterObj._name;
    case "type"
        propertyValue = parameterObj._type;
    case "uservalue"
        propertyValue = parameterObj._uservalue;
    case "valuesconstraints"
        propertyValue = parameterObj._valuescontraints;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end

endfunction

