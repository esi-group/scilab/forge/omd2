// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function updateSopGui()

    sopgui = findobj("Tag", "sopgui"); // Should never be empty

    sopproj = sopGetCurrentProject(sopgui);

    if isempty(sopproj) then
        return
    end
    
    fileMenu = findobj("Tag", "sopguifilemenu");
    fileMenu.enable = "on";
    fileMenu = findobj("Tag", "newprojectmenu");
    fileMenu.enable = "on";
    fileMenu = findobj("Tag", "loadprojectmenu");
    fileMenu.enable = "on";
    fileMenu = findobj("Tag", "quitmenu");
    fileMenu.enable = "on";
    
    paMenu = findobj("Tag", "sopguipamenu");
    [ierr, paEnabled] = sopCallWrapper("sopw_proactive", "enabled")
    if ierr<>0 | ~paEnabled then
        paMenu.enable = "off";
    else
        paMenu.enable = "on";
    end
    paMenu = findobj("Tag", "sopguipaconfigmenu");
    paMenu.enable = "on";
    
    helpMenu = findobj("Tag", "sopguihelpmenu");
    helpMenu.enable = "on";
    helpMenu = findobj("Tag", "sopguihelpsopguimenu");
    helpMenu.enable = "on";
    helpMenu = findobj("Tag", "sopguihelpsopmenu");
    helpMenu.enable = "on";
    helpMenu = findobj("Tag", "sopguiaboutmenu");
    helpMenu.enable = "on";
    
    dataBtn = findobj("Tag", "databtn");
    dataConfigStatusTxt = findobj("Tag", "dataconfigstatustxt");
    dataExecStatusTxt = findobj("Tag", "dataexecstatustxt");

    modelBtn = findobj("Tag", "modelingbtn");
    modelConfigStatusTxt = findobj("Tag", "modelingconfigstatustxt");
    modelExecStatusTxt = findobj("Tag", "modelingexecstatustxt");

    optimBtn = findobj("Tag", "optimbtn");
    optimConfigStatusTxt = findobj("Tag", "optimconfigstatustxt");
    optimExecStatusTxt = findobj("Tag", "optimexecstatustxt");

    // /!\ DO NOT UPDATE IF NOT ACTIVATED (sopproj can be corrupted) 

    // Activate data button
    dataBtn.enable = "on";
    // Activate & Update data status
    dataConfigStatusTxt.enable = "on";
    dataConfigStatusTxt.string = _("Config: ") + string(sopproj.dataconfigstatus);
    dataConfigStatusTxt.backgroundcolor = getBackgroundColor(sopproj.dataconfigstatus)
    dataExecStatusTxt.enable = "on";
    dataExecStatusTxt.string = _("Exec: ") + string(sopproj.dataexecstatus);
    dataExecStatusTxt.backgroundcolor = getBackgroundColor(sopproj.dataexecstatus)

    if sopproj.dataexecstatus <> sopStatus("Done") then
        // Deactivate modeling button & status
        modelBtn.enable = "off";
        modelConfigStatusTxt.enable = "off";
        modelExecStatusTxt.enable = "off";
        // Deactivate optimization button & status
        optimBtn.enable = "off";
        optimConfigStatusTxt.enable = "off";
        optimExecStatusTxt.enable = "off";
    else
        // Activate modeling button
        modelBtn.enable = "on";
        // Activate & Update modeling status
        modelConfigStatusTxt.enable = "on";
        modelConfigStatusTxt.string = _("Config: ") + string(sopproj.modelconfigstatus);
        modelConfigStatusTxt.backgroundcolor = getBackgroundColor(sopproj.modelconfigstatus)
        modelExecStatusTxt.enable = "on";
        modelExecStatusTxt.string = _("Exec: ") + string(sopproj.modelexecstatus);
        modelExecStatusTxt.backgroundcolor = getBackgroundColor(sopproj.modelexecstatus)

        if sopproj.modelexecstatus <> sopStatus("Done") then
            // Deactivate optimization button & status
            optimBtn.enable = "off";
            optimConfigStatusTxt.enable = "off";
            optimExecStatusTxt.enable = "off";
        else
            // Activate optimization button
            optimBtn.enable = "on";
            // Activate & Update optimization status
            optimConfigStatusTxt.enable = "on";
            optimConfigStatusTxt.string = _("Config: ") + string(sopproj.optimconfigstatus);
            optimConfigStatusTxt.backgroundcolor = getBackgroundColor(sopproj.optimconfigstatus)
            optimExecStatusTxt.enable = "on";
            optimExecStatusTxt.string = _("Exec: ") + string(sopproj.optimexecstatus);
            optimExecStatusTxt.backgroundcolor = getBackgroundColor(sopproj.optimexecstatus)
        end
    end
endfunction

function bgColor = getBackgroundColor(status)
    select string(status)
    case string(sopStatus("ToBeDone"))
        bgColor = sopGuiParameters("FigureBackground");
    case string(sopStatus("Running"))
        bgColor = [1 0.5 1];
    case string(sopStatus("Failed"))
        bgColor = [1 0 0];
    case string(sopStatus("Done"))
        bgColor = [0 1 0];
    end
endfunction
