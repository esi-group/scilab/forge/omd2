// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function t = %sopproj_string(projectObj)
    t      = "Version: " + %sopversion_string(projectObj.version);
    t($+1) = "Description: " + projectObj.description;
    t($+1) = "FileData: [" + string(size(projectObj.filedata)) + " variables]";
    t($+1) = "Variables: [" + string(size(projectObj.variables)) + " variables]";
    t($+1) = "Factors: [" + string(size(projectObj.factors)) + " variables]";
    t($+1) = "Responses: [" + string(size(projectObj.responses)) + " variables]";
    t($+1) = "DoeNumerotation: " + string(projectObj.doenumerotation);
    t($+1) = "DoeCenterPointIndex: " + string(projectObj.doecenterpointindex);
endfunction
