// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function propertyValue = generic_e_sopobj(propertyName, sopObj)

    if typeof(sopObj)<>"sopobj" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_e_sopobj", 2, "sopobj"));
    end
    if typeof(propertyName)<>"string" & typeof(propertyName)<>"constant" & typeof(propertyName)<>"polynomial" then
        error(msprintf("%s: Wrong type for input argument #%d: A string or an integer value expected.\n", "generic_e_sopobj", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string or an integer value expected.\n", "generic_e_sopobj", 1));
    end

    valueExtraction = %T;

    if typeof(propertyName)=="string" then

        shortPropertyName = propertyName;
        propertyName = "<" + convstr(propertyName, "l") + ">";

        select propertyName
        case "<pointedfile>"
            propertyValue = sopObj(propertyName);
            valueExtraction = %F;
        case "<origfile>"
            propertyValue = sopObj(propertyName);
            valueExtraction = %F;
        case "<creationdate>"
            propertyValue = sopObj(propertyName);
            valueExtraction = %F;
        case "<modificationdate>"
            propertyValue = sopObj(propertyName);
            valueExtraction = %F;
        case "<workfile>"
            propertyValue = sopObj(propertyName);
            valueExtraction = %F;
        case "<editeddata>"
            propertyValue = sopObj(propertyName);
            valueExtraction = %F;
        case "<validationpoints>"
            // Which response?
            editeddata = sopObj.editeddata;
            [nValues, respIndex, modelIndex] = msscanf(editeddata, ".responses(%d).models(%d)");
            propertyValue = sopGetDoe(sopCreateObject(sopObj.workfile, sopObj.pointedfile), respIndex, "validationpoints");
            // Normalization
            sopProj = sopCreateObject(sopObj.workfile, sopObj.pointedfile);
            lb = [];
            ub = [];
            for kFact=1:size(sopProj.factors)
                lb = [lb,sopProj.factors(kFact).minbound];
                ub = [ub,sopProj.factors(kFact).maxbound];
            end
            propertyValue = sopDoeNormalization(propertyValue, lb, ub);
            valueExtraction = %F;
        case "<validationresponses>"
            // Which response?
            editeddata = sopObj.editeddata;
            [nValues, respIndex, modelIndex] = msscanf(editeddata, ".responses(%d).models(%d)");
            values = sopObj.responses(respIndex).values;
            valuesType = sopObj.responses(respIndex).valuestype;
            propertyValue = values(valuesType==2);
            valueExtraction = %F;
        case "<learningpoints>"
            // Which response?
            editeddata = sopObj.editeddata;
            [nValues, respIndex, modelIndex] = msscanf(editeddata, ".responses(%d).models(%d)");
            propertyValue = sopGetDoe(sopCreateObject(sopObj.workfile, sopObj.pointedfile), respIndex, "learningpoints");
            // Normalization
            sopProj = sopCreateObject(sopObj.workfile, sopObj.pointedfile);
            lb = [];
            ub = [];
            for kFact=1:size(sopProj.factors)
                lb = [lb,sopProj.factors(kFact).minbound];
                ub = [ub,sopProj.factors(kFact).maxbound];
            end
            propertyValue = sopDoeNormalization(propertyValue, lb, ub);
            valueExtraction = %F;
        case "<learningresponses>"
            // Which response?
            editeddata = sopObj.editeddata;
            [nValues, respIndex, modelIndex] = msscanf(editeddata, ".responses(%d).models(%d)");
            values = sopObj.responses(respIndex).values;
            valuesType = sopObj.responses(respIndex).valuestype;
            propertyValue = values(valuesType==1);
            valueExtraction = %F;
        case "<responsename>"
            // Which response?
            editeddata = sopObj.editeddata;
            [nValues, respIndex, modelIndex] = msscanf(editeddata, ".responses(%d).models(%d)");
            dataPtr = sopCreateObject(sopObj, "");
            propertyValue = dataPtr.responses(respIndex).name;
            valueExtraction = %F;
        case "<doevariablesnumber>"
            parentProj = sopCreateObject(sopObj.workfile, sopObj.pointedfile, "");
            allFactors = parentProj.factors;
            propertyValue = 0;
            for k=1:size(allFactors)
                if ~(allFactors(k).isfixed) then
                    propertyValue = propertyValue + 1;
                end
            end
            valueExtraction = %F;
        else
            if part(shortPropertyName, length(shortPropertyName)) == "#" then
                // Path extraction
                valueExtraction = %F;
                shortPropertyName = part(shortPropertyName, 1:(length(shortPropertyName)-1))
                propertyName = "<" + shortPropertyName + ">";
            end

            // Try to find the field in the pointed data
            pointedData = sopGetValue(sopObj);
            fields = getfield(1, pointedData);
            if or(fields == propertyName) then // The field exists
                propertyValue = sopCreateObject(sopObj, sopObj.editeddata, shortPropertyName);
            elseif typeof(pointedData) == "sopdoege" | typeof(pointedData) == "sopsimul" then
                if is_param(pointedData.params, shortPropertyName) then
                    propertyValue = sopCreateObject(sopObj, sopObj.editeddata, "params", shortPropertyName);
                else // A project field
                    fields = getfield(1, sopGetValue(sopObj));
                    if or(fields == propertyName) then // The field exists
                        propertyValue = sopCreateObject(sopObj, shortPropertyName);
                    else
                        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
                    end
                end
            else // A project field
                fields = getfield(1, sopGetValue(sopObj));
                if or(fields == propertyName) then // The field exists
                    propertyValue = sopCreateObject(sopObj, "", shortPropertyName);
                else
                    error(msprintf("''%s'' is not a valid property name.\n", propertyName));
                end
            end
        end
    elseif typeof(propertyName)=="polynomial" | typeof(propertyName)=="constant" then
        propertyValue = sopCreateObject(sopObj, sopObj.editeddata , propertyName);
        valueExtraction = %F;
    end

    if valueExtraction then
        propertyValue = sopGetValue(propertyValue);
    end
endfunction
