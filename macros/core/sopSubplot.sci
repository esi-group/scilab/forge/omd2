// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function currentAxes = sopSubplot(varargin)

    if typeof(varargin(1)) == "handle" then // sopSubplot called to resize/reorganize the axes (See sopPageManager)
        currentAxes = varargin(1);
        nbRows = varargin(2);
        nbCols = varargin(3);
        graphIndex = varargin(4);
        if size(varargin)==4 then
            margin = 0.05
        else
            margin = varargin(5);
        end
    else // sopSubplot called to create an axes
        nbRows = varargin(1);
        nbCols = varargin(2);
        graphIndex = varargin(3);
        if size(varargin)==3 then
            margin = 0.05
        else
            margin = varargin(4);
        end
        subplot(nbRows, nbCols, graphIndex);
        currentAxes = gca();
    end

    leftMargin = margin;
    rightMargin = margin;
    topMargin = margin;
    bottomMargin = margin;

    interX = margin;
    interY = margin;

    currentAxesRow = int((graphIndex-1)/nbCols) + 1;
    currentAxesCol = graphIndex-1-nbCols*(currentAxesRow-1) + 1;

    width = (1 - leftMargin - rightMargin - (nbCols - 1) * interX) / nbCols;
    height = (1 - topMargin - bottomMargin - (nbRows - 1) * interY) / nbRows;

    x = leftMargin + (currentAxesCol-1)*(width + interX);
    y = topMargin + (currentAxesRow-1)*(height + interY);

    set(currentAxes, "axes_bounds", [x, y, width, height]);
    set(currentAxes, "background", -2);
endfunction

