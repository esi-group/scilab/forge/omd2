// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function data = sopGetAppdata(figHandle, dataName)
    ud = get(figHandle, "userdata");
    if ~isfield(ud, "sopappdata") then
        error("This figure does seems to be a Scilab Optimization Platform figure.");
    end
    
endfunction
