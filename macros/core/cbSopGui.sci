// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopGui(gcboTag)

    sopgui = findobj("Tag", "sopgui"); // Should never be empty

    if argn(2)<1 then
        gcboTag = get(gcbo, "Tag");
    end

    select gcboTag
    case "loadprojectmenu"
        [filename, pathname] = uigetfile("*.sop");
        if ~isempty(filename) & ~isempty(pathname) then
            sopProj = sopLoadDataFile(sopgui, pathname + filesep() + filename);
            sopSetCurrentProject(sopgui, sopProj);
        end
    case "newprojectmenu"
        sopProj = sopCreateObject();
        sopSetCurrentProject(sopgui, sopProj);
    case "saveprojectmenu"
        sopProj = sopGetCurrentProject(sopgui)
        if isempty(sopProj.pointedfile) then
            [filename, pathname] = uiputfile("*.sop");
            if ~isempty(filename) & ~isempty(pathname) then
                sopSaveProject(sopProj, pathname + filesep() + filename)
            else
                return
            end
        else
            sopSaveProject(sopProj)
        end
    case "quitmenu"
        btn = messagebox(_("Do you want to quit Scilab Optimization Platform?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            delete(sopgui);
            return // Do not update since the GUI no more exists
        end
    case "databtn"
        sopDataGui();
        return // Do not update since this GUI must be disabled while data module is running
    case "modelingbtn"
        sopModelingGui();
        return // Do not update since this GUI must be disabled while modeling module is running
    case "optimbtn"
        sopOptimizationGui();
        return // Do not update since this GUI must be disabled while optimization module is running
    case "sopguipaconfigmenu"
        sopProj = sopGetCurrentProject(sopgui);
        [ierr, params] = sopCallWrapper("sopw_proactive", "configure");
        if ierr<>0 then
            error(msprintf(_("%s: Could not get configuration for ''%s'' wrapper.\n"), "cbSopModelingGui", "sopw_proactive"));
        end
        sopParametersConfigGui(sopgui, "sopguipaconfigmenu", params, sopProj, "paparams");
        return // Avoid the execution of updateSopGui();
    case "sopguihelpsopguimenu"
        help sopgui
    case "sopguihelpsopmenu"
        help sop
    case "sopguiaboutsopmenu"
        sopAbout();
    end

    updateSopGui();

endfunction

