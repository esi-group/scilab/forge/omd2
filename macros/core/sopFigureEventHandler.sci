// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopFigureEventHandler(win, x, y, ibut)
    if ibut==-1000 then // The figure has been close

        fig = scf(win);

        hdls = sopGuiHandles(fig);

        if isempty(fig.userdata) | ~isfield(fig.userdata, "sopcallbackfunction") then
            return
        end
        
        cbFunname = fig.userdata.sopcallbackfunction;
        
        if ~isempty(cbFunname) then
            if isfield(hdls, "cancel_btn") then
                execstr(cbFunname + "(""cancel_btn"")");
                return
            end

            if isfield(hdls, "quit_menu") then
                execstr(cbFunname + "(""quit_menu"")");
                return
            end
        end
    end
endfunction