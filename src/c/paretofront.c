/*
 * PARETOFRONT returns the logical Pareto Front of a set of points.
 *      synopsis:   front = paretofront(M)
 * INPUT ARGUMENT
 *      - M         n x m array, of which (i,j) element is the j-th objective
 *                  value of the i-th point;
 *  OUTPUT ARGUMENT
 *      - front     n x 1 logical vector to indicate if the corresponding
 *                  points are belong to the front (true) or not (false).
 * Matlab version: By Yi Cao at Cranfield University, 31 October 2007
 */

/*
 * Scilab version based on Matlab version and developed in the context of:
 * - OMD2 project (French National Agency for Research)
 * - CSDL project (Systematic competitiveness cluster)
 */
#include <stdlib.h>
#include "paretofront.h"

void paretofront(int* front, double* M, unsigned int row, unsigned int col)
{
    unsigned int t = 0, s = 0, i = 0, j = 0, j1 = 0, j2 = 0;
    int *checklist = NULL;
    int coldominatedflag = 0;

    checklist = (int *)malloc(row*sizeof(int));
    for(t = 0; t<row; t++)
    {
        checklist[t] = 1;
    }

    for(s = 0; s<row; s++)
    {
        t=s;
        if (!checklist[t])
        {
            continue;
        }

        checklist[t] = 0;
        coldominatedflag = 1;
        for(i=t+1;i<row;i++)
        {
            if (!checklist[i])
            {
                continue;
            }
            checklist[i] = 0;
            for (j=0,j1=i,j2=t;j<col;j++,j1+=row,j2+=row)
            {
                if (M[j1] < M[j2])
                {
                    checklist[i] = 1;
                    break;
                }
            }
            if (!checklist[i])
            {
                continue;
            }
            coldominatedflag = 0;
            for (j=0,j1=i,j2=t;j<col;j++,j1+=row,j2+=row)
            {
                if (M[j1] > M[j2])
                {
                    coldominatedflag = 1;
                    break;
                }
            }
            if (!coldominatedflag) //swap active index continue checking
            {
                front[t] = 0;
                checklist[i] = 0;
                coldominatedflag = 1;
                t = i;
            }
        }
        front[t]=coldominatedflag;
        if (t>s)
        {
            for (i=s+1; i<t; i++)
            {
                if (!checklist[i])
                {
                    continue;
                }
                checklist[i] = 0;
                for (j=0,j1=i,j2=t;j<col;j++,j1+=row,j2+=row)
                {
                    if (M[j1] < M[j2])
                    {
                        checklist[i] = 1;
                        break;
                    }
                }
            }
        }
    }
    free(checklist);
}

