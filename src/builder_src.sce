// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function builder_src()
  langage_src = "c";
  path_src = get_absolute_file_path("builder_src.sce");
  tbx_builder_src_lang(langage_src, path_src);
endfunction

builder_src();
clear builder_src; // remove builder_src on stack
