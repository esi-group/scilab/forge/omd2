mode(-1);

// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

testPath = get_absolute_file_path("proactive_simulation_scifun.tst");

p = sopCreateObject();

p.description = "OMD2 - Simulate responses using ProActive and a Scilab function";

p.factors($+1) = sopVariable("var1", 0, 10, 5);
p.factors($+1) = sopVariable("var2", 10, 20, 15);

p.responses($+1) = sopVariable("resp1");
p.responses($+1) = sopVariable("resp2");

p.datasource = "doe";

p.filename = TMPDIR + filesep() + "proactive_simulation_scifun.sop";

sopSaveProject(p);

// Factors value generation
function value = fakegenerator(job, obj)
    value = [];
    select job
    case "version"
        value = [0 0 0]
    case "type"
        value = "doegenerator"
    case "defaults"
        value = init_param();
    case "generate"
        nbfactors = size(obj.factors);
        for k = 1:nbfactors
            value = [value (obj.factors(k).minbound:obj.factors(k).maxbound)'];
        end
        obj.values = value
    end
endfunction

p.doegenerators($+1) = sopInitWrapper("fakegenerator");

sopDoeGeneration(p);

// Responses values simulation using ProActive
function value = fakesimulator(job, obj)
    value = [];
    select job
    case "version"
        value = [0 0 0]
    case "type"
        value = "simulator"
    case "defaults"
        value = init_param();
    case "simulate"
        PAconnect("pamr://1");
        PAoptions("Debug",%t);
        t = PATask(1,3);
        pointsToSimulate = (1:3)';
        for k=1:size(pointsToSimulate, 1)
            t(1,k).Func = "cosh";
            t(1,k).Params = list(pointsToSimulate(k))
        end
        r = PAsolve(t);
        values  = PAwaitFor(r)
        simulatedResponses = list2vec(values);
        simulatedResponses = [simulatedResponses simulatedResponses/2]
        obj.values = simulatedResponses;
    end
endfunction

p.simulator = sopInitWrapper("fakesimulator");


sopSimulation(p);


