// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = rosen(x)
    y = (1-x(1)).^2 + 105*(x(2)-x(1).^2).^2;
endfunction

// <<Fully unconstrained problem>>
sol = optimize(rosen, [3 3])
assert_checkalmostequal(sol, [1 1], 1000*%eps);

// <<lower bound constrained>>
sol = optimize(rosen,[3 3],[2 2],[])
assert_checkalmostequal(sol, [2 4], 1e8*%eps);

// <<x(2) fixed at 3>>
//sol = optimize(rosen,[3 3],[-%inf 3],[%inf 3])
//assert_checkalmostequal(sol, [1.7314 3.0000], 1e8*%eps);

// <<simple linear inequality: x(1) + x(2) <= 1>>
//sol = optimize(rosen,[0 0],[],[],[1 1], 1)
//assert_checkalmostequal(sol, [0.6187 0.3813], 1e8*%eps);

// <<nonlinear inequality: sqrt(x(1)^2 + x(2)^2) <= 1>>
// <<nonlinear equality  : x(1)^2 + x(2)^3 = 0.5>>
function [c, ceq] = nonlcon(x)
    c = norm(x) - 1;
    ceq = x(1)^2 + x(2)^3 - 0.5;
endfunction
options = optimset('TolFun', 1e-8, 'TolX', 1e-8);
sol = optimize(rosen, [3 3], [],[],[],[],[],[], nonlcon, [], options)
assert_checkalmostequal(sol, [0.6513 0.4233], 1e12*%eps);

