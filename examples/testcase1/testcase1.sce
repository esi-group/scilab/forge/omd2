mode(-1);

// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

testCasePath = get_absolute_file_path("testcase1.sce");

//exec(testCasePath + "initTestCase1.sce", -1);

doInit = %T;
doDoe = %T;
doSimul = %T;
doModel = %T;
doOptim = %T;

//-----------------------
// Project initialization
//-----------------------
if doInit then
    sopproj = sopCreateObject();

    sopproj.description = "OMD2 - Test Case #1";

    sopproj.factors($+1) = sopVariable("x1", 4, 11, 7.637, %T);
    sopproj.factors($+1) = sopVariable("x2", 15, 45, 26.635, %T);
    sopproj.factors($+1) = sopVariable("x3", 5, 20, 12.683, %T);
    sopproj.factors($+1) = sopVariable("x4", 5, 11, 7.604, %T);
    sopproj.factors($+1) = sopVariable("x5", 20, 60, 54.5, %T);
    sopproj.factors($+1) = sopVariable("a1", 0.6, 4, 2);
    sopproj.factors($+1) = sopVariable("b1", 0.6, 4, 2);
    sopproj.factors($+1) = sopVariable("a2", 9, 60, 30);
    sopproj.factors($+1) = sopVariable("b2", 9, 60, 30);
    sopproj.factors($+1) = sopVariable("a3", 9, 60, 30);
    sopproj.factors($+1) = sopVariable("b3", 9, 60, 30);
    sopproj.factors($+1) = sopVariable("a4", 1.5, 10, 5);
    sopproj.factors($+1) = sopVariable("b4", 1.5, 10, 5);

    sopproj.responses($+1) = sopVariable("pressureLoss");
    sopproj.responses($+1) = sopVariable("flowSD");

    sopproj.datasource = "doe";

    sopproj.filename = sopModulePath() + "examples/testcase1/testcase1_init.sop";
    sopSaveProject(sopproj);
else
    sopproj = sopLoadProject(sopModulePath() + "examples/testcase1/testcase1_init.sop");
end

//------------------
// Points generation
//------------------
if doDoe then
    sopproj.doegenerators($+1) = sopInitWrapper("sopw_lhc");
    sopproj.doegenerators($).params.m = 80;

    sopDoeGeneration(sopproj);

    sopproj.filename = sopModulePath() + "examples/testcase1/testcase1_doe.sop";
    sopSaveProject(sopproj);
else
    sopproj = sopLoadProject(sopModulePath() + "examples/testcase1/testcase1_doe.sop");
end

//-------------
// Simulation
//-------------
if doSimul then
    sopproj.simulator = sopInitWrapper("sopw_openfoam");
    sopSimulation(sopproj)

    // Random selection of validation points
    validIndex = 1:5:sopproj.doegenerators($).params.m;
    allResponses = sopproj.responses#;
    for kResp = 1:size(allResponses)
        valuesType = allResponses(kResp).valuestype;
        valuesType(validIndex) = 2;
        allResponses(kResp).valuestype = valuesType;
    end
    // Bad points
    for kResp = 1:size(allResponses)
        values = allResponses(kResp).values;
        valuesType = allResponses(kResp).valuestype;
        valuesType(values<0) = 0;
        allResponses(kResp).valuestype = valuesType;
    end
    sopproj.filename = sopModulePath() + "examples/testcase1/testcase1_simul.sop";
    sopSaveProject(sopproj);
else
    sopproj = sopLoadProject(sopModulePath() + "examples/testcase1/testcase1_simul.sop");
end

//-------------
// Modelization
//-------------
if doModel then
    allResponses = sopproj.responses#;
    for kResp = 1:size(allResponses)
        allResponses(kResp).models($+1) = sopInitWrapper("sopw_dace");
    end
    sopModeling(sopproj);
    sopproj.filename = sopModulePath() + "examples/testcase1/testcase1_model.sop";
    sopSaveProject(sopproj);
else
    sopproj = sopLoadProject(sopModulePath() + "examples/testcase1/testcase1_model.sop");
end

//-------------
// Optimization
//-------------
if doOptim then
    sopproj.optimizers($+1) = sopInitWrapper("sopw_optim_ga");
    sopproj.responsescoeffs = [-1 -1];
    sopOptimization(sopproj)

    sopproj.filename = sopModulePath() + "examples/testcase1/testcase1_optim.sop";
    sopSaveProject(sopproj);
else
    sopproj = sopLoadProject(sopModulePath() + "examples/testcase1/testcase1_optim.sop");
end


//------------------
// Full project file
//------------------
sopproj.filename = sopModulePath() + "examples/testcase1/testcase1.sop";
sopproj.filename = sopModulePath() + "examples/testcase1/testcase1_" + string(sopproj.doegenerators(1).params.m) + ".sop";
sopSaveProject(sopproj);

