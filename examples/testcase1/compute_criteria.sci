function [pressureLoss,flowSD] = compute_criteria(path,density,Ux,Uy,P,nbound,boundStartFace)
//--------------------------------------------------------------------------
// Computes the pressure loss and flow standard deviation. For pressure
// loss, the input surface is between P1 and P2, and the output between P11
// and P12. The flow SD is computed at the section between P9 and P10.
//
// Value of the 'density' parameter is needed.
//
// The outlet pressure is always 0 so pressure loss is equal to inlet pressure.
//
// Inputs:
//   - P: pressure field (ncells x 1)
//   - Ux, Uy: velocity fields (ncells x 1)
//
// Output:
//   - pressureLoss (scalar)
//   - flowSD: standard deviation of the (normal) flow between P9 and P10
//   (scalar)
//--------------------------------------------------------------------------
// author: V. Picheny, ECP - date: feb. 2010
//--------------------------------------------------------------------------
//
// Scilab author
// SIREHNA
// G. Jacquenot
// 2010 - 04 - 30

// Number of arguments in the input function
[%nargout,%nargin] = argn(0);
if %nargin<=5 then
  // Get inlet elements indices
  // Open the file "./owner" in "fid" for reading
  filename=path+'/boundary';
  fid = mopen(filename,'r');
  // Read header lines
  mgetl(fid,22);
  [Dummy,text,nbound]         = mfscanf(1,fid,'%s %d');
  mgetl(fid,1);
  [Dummy,text,boundStartFace] = mfscanf(1,fid,'%s %d');
  mclose(fid);
end;


//--- Flow SD --------------------------------------------------------------
ncells  = size(P,2);

// Indices of the d elements between P9 and P10
// The last 2*d² elements correspond to the last rectangular block.
//outIndices = ncells-((2*density^2+density):-1:(2*density^2+1));
outIndices = (ncells-density+1):ncells;

// Flow between P9 and P10
UxOutlet = Ux(outIndices);
UyOutlet = Uy(outIndices);

// Old computation
// Un = sqrt(UxOutlet.^2 + UyOutlet.^2);
// flowSD = std(Un);

// Projected flow
// Tangent vector
Vt = [(51.282 - 79.193) (10.149 - 147.339)];
Vtnorm = Vt./norm(Vt);
Up     = 0.5*( 1.0 + (UxOutlet.^2 + UyOutlet.^2) - ((Vtnorm(1) - UxOutlet).^2 + (Vtnorm(2) - UyOutlet).^2) );
flowSD = nanstdev(Up);

//--- Pressure loss --------------------------------------------------------
// Get inlet elements indices
// Open the file "./owner" in "fid" for reading
filename=path+'/owner';
fid = mopen(filename,'r');
// Read header lines
mgetl(fid,boundStartFace+21);
// Read "boundCells" values in file "fid"
boundCells = mfscanf(nbound,fid,'%d');
// Close the file
mclose(fid);

pInlet       = P(boundCells+1);
pressureLoss = mean(pInlet);
endfunction
