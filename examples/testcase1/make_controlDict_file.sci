function make_controlDict_file(filename,OF_nb_iteration)
//function make_controlDict_file(OF_nb_iteration)
//--------------------------------------------------------------------------
//
// This function generates the controlDict file that contains the final number
// of iterations of OpenFoam
//
// Scilab author
// SIREHNA
// G. Jacquenot
// 2010 - 10 - 18

// Nombre d'arguments dans l'appel de la fonction
[%nargout,%nargin] = argn(0)

if %nargin<1 then
  OF_nb_iteration = 1000;
end;

fid     = mopen(filename,"wt");
//--------------------------------------------------------------------------
// En-tête

A = "/*--------------------------------*- C++ -*----------------------------------*\\ \n"+...
    "| =========                 |                                                 | \n"+...
    "| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           | \n"+...
    "|  \\\\    /   O peration     | Version:  1.3                                   | \n"+...
    "|   \\\\  /    A nd           | Web:      http://www.OpenFOAM.org               | \n"+...
    "|    \\\\/     M anipulation  |                                                 | \n"+...
    "\\*---------------------------------------------------------------------------*/ \n"+...
    "\n"+...
    "FoamFile\n"+...
    "{ \n"+...
    "    version     2.0;\n"+...
    "    format      ascii;\n\n"+...
    "    root        """";\n"+...
    "    case        """";\n"+...
    "    instance    """";\n"+...
    "    local       """";\n\n"+...
    "    class       dictionary;\n"+...
    "    object      controlDict;\n"+...
    "}\n\n"+...
    "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n";

fprintf(fid,A);

fprintf(fid,"application simpleFoam;\n");
fprintf(fid,"\n");
fprintf(fid,"startFrom       startTime; //latestTime;\n");
fprintf(fid,"\n");
fprintf(fid,"startTime       0;\n");
fprintf(fid,"\n");
fprintf(fid,"stopAt          endTime;\n");
fprintf(fid,"\n");
fprintf(fid,"endTime         %d;\n",OF_nb_iteration);
fprintf(fid,"\n");
fprintf(fid,"deltaT          1;\n");
fprintf(fid,"\n");
fprintf(fid,"writeControl    timeStep;\n");
fprintf(fid,"\n");
fprintf(fid,"writeInterval   100;\n");
fprintf(fid,"\n");
fprintf(fid,"purgeWrite      1;\n");
fprintf(fid,"\n");
fprintf(fid,"writeFormat     ascii;\n");
fprintf(fid,"\n");
fprintf(fid,"writePrecision  12;\n");
fprintf(fid,"\n");
fprintf(fid,"writeCompression uncompressed;\n");
fprintf(fid,"\n");
fprintf(fid,"timeFormat      general;\n");
fprintf(fid,"\n");
fprintf(fid,"timePrecision   12;\n");
fprintf(fid,"\n");
fprintf(fid,"runTimeModifiable yes;\n");
fprintf(fid,"\n");
fprintf(fid,"\n");
fprintf(fid,"// ************************************************************************* //\n");
fprintf(fid,"\n");
mclose(fid);
endfunction
