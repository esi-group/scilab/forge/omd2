function [vertices,blocks,inlet,outlet,fixedWalls,frontAndBack] = make_index(C)
// function [vertices,blocks,inlet,outlet,fixedWalls,frontAndBack] = make_index(C)
//--------------------------------------------------------------------------
// Genere les matrices d'indices pour la la fonction blockMeshDict
// Fonction appelee par le script make_mesh_file.
//
// entree C : matrice [4xn] contenant les coordonnees des profils
// superieurs et inferieurs.
// C(1,:): abcisses de la partie superieure
// C(2,:): ordonnees de la partie superieure
// C(3,:): abcisses de la partie inferieure
// C(4,:): ordonnees de la partie inferieure
//
// Sortie : - vertices : coordonnees des noeuds
//          - blocks : blocs elementaires
//          - inlet, outlet : entree / sortie
//          - fixedWalls : frontieres fixes
//          - frontAndBack : pour definir la 2D
//--------------------------------------------------------------------------
// author: V. Picheny, ECP - date: feb. 2010
//--------------------------------------------------------------------------
//
// Scilab author
// SIREHNA
// G. Jacquenot
// 2010 - 04 - 30

npts    = size(C,2);
nblocks = npts-1;
nnodes  = 4*npts;

//--- Make vertices --------------------------------------------------------
vertices = zeros(nnodes,3);

vblocks = 0:nblocks;

vertices(4*vblocks+1,1:2) = C(3:4,vblocks+1)';
vertices(4*vblocks+2,1:2) = C(1:2,vblocks+1)';
vertices(4*vblocks+3,1:2) = vertices(4*vblocks+1,1:2);
vertices(4*vblocks+4,1:2) = vertices(4*vblocks+2,1:2);
vertices(4*vblocks+3,3)   = 0.1;
vertices(4*vblocks+4,3)   = 0.1;


vblocks = 1:nblocks;

//--- Make blocks ----------------------------------------------------------
blocks = 4*ones(1,8).*.vblocks' + ones(nblocks,1) .*. [0,1,-3,-4,2,3,-1,-2];

//--- Make fixed walls -----------------------------------------------------
fixedWalls                 = zeros(2*nblocks,4);
fixedWalls(2*vblocks-1,:)  = 4*ones(1,4).*.vblocks' + ones(nblocks,1) .*. [0,2,-2,-4];
fixedWalls(2*vblocks  ,:)  = 4*ones(1,4).*.vblocks' + ones(nblocks,1) .*. [1,-3,-1,3];


//--- Make front and back --------------------------------------------------
nfrontAndBack                = 2*nblocks;
frontAndBack                 = zeros(nfrontAndBack,4);
frontAndBack(2*vblocks-1,:)  = 4*ones(1,4).*.vblocks' + ones(nblocks,1) .*. [0,-4,-3,1];
frontAndBack(2*vblocks  ,:)  = 4*ones(1,4).*.vblocks' + ones(nblocks,1) .*. [2,3,-1,-2];

inlet  = [2,0,1,3];
outlet = nnodes-[4,3,1,2];
endfunction
