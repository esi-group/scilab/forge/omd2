// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function responsesValues = testcase1PostProcessing(resultPath, numberOfIterations, density)
    path = resultPath  + "/" + string(numberOfIterations);
    [Ux_final,Uy_final,P_final] = get_U_and_p(path);

    path = resultPath + "/constant/polyMesh"
    [pressureLoss flowSD] = compute_criteria(path, density, Ux_final(1, :), Uy_final(1, :), P_final(1, :));
    responsesValues = [pressureLoss flowSD];
endfunction
