function [Ux_final,Uy_final,P_final,ncells] = get_U_and_p(path_output)
//--------------------------------------------------------------------------
// Get data from an existing DOE and concatenate p fields into text files.
//
//
// Input files location:
//  - path_output: Directory containing OpenFoam results
//
// Output files:
//   - Ux_final, Uy_final: contain the final U fields of all the
//   experiments. [nobs x ncells]
//   - P_final: contain the final p fields of all the
//   experiments. [nobs x ncells]
//--------------------------------------------------------------------------
// author: V. Picheny, ECP - date: feb. 2010
//--------------------------------------------------------------------------
//
// Scilab author
// SIREHNA
// G. Jacquenot
// 2010 - 04 - 30
//

// Open the file "./p" in ""fid"" for reading
filename=path_output+'/p';
fid = mopen(filename,'r');
// Read header lines
mgetl(fid,20);
ncells = mfscanf(1,fid,'%d');
mgetl(fid,2);

// Initialization
P_final  = zeros(1,ncells);
Ux_final = zeros(1,ncells);
Uy_final = zeros(1,ncells);

// Read "ncells" value in file "fid"
p = mfscanf(ncells,fid,'%e');
// Close the file
mclose(fid);
P_final = p';

// Get Ux and Uy
// Open the file "./U" in ""fid"" for reading
filename=path_output+'/U';
fid = mopen(filename,'r');
// Read header lines
mgetl(fid,22);
// Read "ncells" value in file "fid"
res = mfscanf(ncells,fid,'(%lg %lg %lg)\n');
// Close the file
mclose(fid);

Ux_final = res(:,1)';
Uy_final = res(:,2)';

endfunction
