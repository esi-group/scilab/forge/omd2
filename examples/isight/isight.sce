// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Test file used to create a project file

// Create empty project
sopProj = sopProject();

// Load data file
sopLoadDataFile(sopProj, sopModulePath() + "examples/isight/plex_reduit_v3.db");

// Sort responses/factors
sopAddFactor(sopProj, [3 4 5 6 11 12 15 16]);
sopAddResponse(sopProj, [11 13]);

// Save the project
sopSaveProject(sopProj, sopModulePath() + "examples/isight/isight-generated.sop");
